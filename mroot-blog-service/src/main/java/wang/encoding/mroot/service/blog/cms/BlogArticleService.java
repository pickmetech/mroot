/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.cms;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.blog.entity.cms.article.BlogArticleGetVO;

import java.math.BigInteger;
import java.util.List;

/**
 * 博客 文章 Service 接口
 *
 * @author ErYang
 */
public interface BlogArticleService {


    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return BlogArticleGetVO
     */
    BlogArticleGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return BlogArticleGetVO
     */
    BlogArticleGetVO getByIdNoCache(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章
     *
     * @param articleGetVO BlogArticleGetVO
     * @return boolean
     */
    boolean update(@NotNull final BlogArticleGetVO articleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<BlogArticleGetVO>
     * @param articleGetVO BlogArticleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<BlogArticleGetVO>
     */
    IPage<BlogArticleGetVO> list2page(@NotNull final Page<BlogArticleGetVO> page,
            @NotNull final BlogArticleGetVO articleGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<BlogArticleGetVO>
     * @param articleGetVO BlogArticleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     * @param categoryIds  BigInteger 文章分类
     *
     * @return IPage<BlogArticleGetVO>
     */
    IPage<BlogArticleGetVO> list2page(@NotNull final Page<BlogArticleGetVO> page,
            @NotNull final BlogArticleGetVO articleGetVO, @Nullable String orderByField, boolean isAsc,
            @NotNull final List<BigInteger> categoryIds);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  BlogArticleGetVO 列表
     * @param articleGetVO BlogArticleGetVO
     * @return List
     */
    List<BlogArticleGetVO> list(@NotNull final BlogArticleGetVO articleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  BlogArticleGetVO 集合
     * @param articleGetVO  BlogArticleGetVO 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    List<BlogArticleGetVO> list(final BlogArticleGetVO articleGetVO, final int count, @NotNull final String column,
            final boolean isAsc);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogArticleService interface

/* End of file BlogArticleService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/cms/BlogArticleService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
