/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.system.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.MailLogDO;
import wang.encoding.mroot.service.blog.system.BlogMailLogService;
import wang.encoding.mroot.service.system.MailLogService;
import wang.encoding.mroot.vo.blog.entity.system.maillog.BlogMailLogGetVO;

import java.math.BigInteger;

/**
 * 博客 电子邮箱记录 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class BlogMailLogServiceImpl implements BlogMailLogService {


    private final MailLogService mailLogService;

    @Autowired
    @Lazy
    public BlogMailLogServiceImpl(MailLogService mailLogService) {
        this.mailLogService = mailLogService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 电子邮箱记录
     *
     * @param mailLogGetVO BlogMailLogGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final BlogMailLogGetVO mailLogGetVO) {
        MailLogDO mailLogDO = BeanMapperComponent.map(mailLogGetVO, MailLogDO.class);
        return mailLogService.saveByT(mailLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 电子邮箱记录
     *
     * @param mailLogGetVO BlogMailLogGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final BlogMailLogGetVO mailLogGetVO) {
        MailLogDO mailLogDO = BeanMapperComponent.map(mailLogGetVO, MailLogDO.class);
        return mailLogService.updateById(mailLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * MailLogDO 转为 BlogMailLogGetVO
     *
     * @param mailLogDO MailLogDO
     * @return BlogMailLogGetVO
     */
    private BlogMailLogGetVO mailLogDO2BlogMailLogGetVO(@NotNull final MailLogDO mailLogDO) {
        BlogMailLogGetVO mailLogGetVO = BeanMapperComponent.map(mailLogDO, BlogMailLogGetVO.class);
        if (null != mailLogGetVO.getState()) {
            mailLogGetVO.setStatus(StateEnum.getValueByKey(mailLogGetVO.getState()));
        }
        if (null != mailLogGetVO.getGmtCreateIp()) {
            mailLogGetVO.setIp(IpUtils.intToIpv4String(mailLogGetVO.getGmtCreateIp()));
        }
        return mailLogGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogMailLogServiceImpl class

/* End of file BlogMailLogServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/system/impl/BlogMailLogServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
