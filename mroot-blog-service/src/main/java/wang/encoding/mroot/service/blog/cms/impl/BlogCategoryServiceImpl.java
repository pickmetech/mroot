/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.cms.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.AesManageComponent;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.domain.entity.cms.CategoryDO;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;
import wang.encoding.mroot.service.cms.CategoryService;
import wang.encoding.mroot.vo.blog.entity.cms.category.BlogCategoryGetVO;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * 博客 文章分类 Service 接口实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.BLOG_CATEGORY_CACHE)
public class BlogCategoryServiceImpl implements BlogCategoryService {


    private final CategoryService categoryService;

    @Autowired
    @Lazy
    public BlogCategoryServiceImpl(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return BlogCategoryGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public BlogCategoryGetVO getById(@NotNull final BigInteger id) {
        CategoryDO categoryDO = categoryService.getTById(id);
        if (null != categoryDO) {
            return this.categoryDO2BlogCategoryGetVO(categoryDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 类型小于 3 文章分类集合
     *
     * @return List
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public List<BlogCategoryGetVO> listTypeLt3() {
        List<CategoryDO> list = categoryService.listTypeLt3();
        if (ListUtils.isNotEmpty(list)) {
            List<CategoryDO> categoryDOList = new ArrayList<>();
            for (CategoryDO categoryDO : list) {
                if (BooleanEnum.YES.getKey() == categoryDO.getDisplay()) {
                    categoryDOList.add(categoryDO);
                }
            }
            if (ListUtils.isNotEmpty(categoryDOList)) {
                List<BlogCategoryGetVO> sortList = BeanMapperComponent.mapList(categoryDOList, BlogCategoryGetVO.class);
                sortList = BlogCategoryGetVO.list(sortList);
                // 排序
                Collections.sort(sortList);
                for (BlogCategoryGetVO categoryGetVO : sortList) {
                    // ID 加密
                    categoryGetVO.setAesId(AesManageComponent.getEncrypt(String.valueOf(categoryGetVO.getId())));
                }
                return sortList;
            }
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据父类 id 查询子类 id
     *
     * @param  id BigInteger
     *
     * @return List
     */
    @Override
    public List<BigInteger> listByPId(@NotNull final BigInteger id) {
        if (null != id && BigIntegerUtils.gt(id, BigInteger.ZERO)) {
            return categoryService.listByPId(id);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * CategoryDO 转为 BlogCategoryGetVO
     *
     * @param categoryDO CategoryDO
     * @return BlogCategoryGetVO
     */
    private BlogCategoryGetVO categoryDO2BlogCategoryGetVO(@NotNull final CategoryDO categoryDO) {
        BlogCategoryGetVO categoryGetVO = BeanMapperComponent.map(categoryDO, BlogCategoryGetVO.class);
        if (null != categoryGetVO.getState()) {
            categoryGetVO.setStatus(StateEnum.getValueByKey(categoryGetVO.getState()));
        }
        if (null != categoryGetVO.getGmtCreateIp()) {
            categoryGetVO.setIp(IpUtils.intToIpv4String(categoryGetVO.getGmtCreateIp()));
        }

        if (null != categoryGetVO.getPid() && BigIntegerUtils.gt(categoryGetVO.getPid(), BigInteger.ZERO)) {
            BlogCategoryGetVO parentCategory = this.getById(categoryGetVO.getPid());
            if (null != parentCategory) {
                categoryGetVO.setParentCategory(parentCategory);
            }
        }
        // ID 加密
        categoryGetVO.setAesId(AesManageComponent.getEncrypt(String.valueOf(categoryGetVO.getId())));
        return categoryGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogCategoryServiceImpl class

/* End of file BlogCategoryServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/cms/impl/BlogCategoryServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
