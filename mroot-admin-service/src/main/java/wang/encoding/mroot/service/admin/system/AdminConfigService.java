/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;


/**
 * 配置后台 Service
 *
 * @author ErYang
 */
public interface AdminConfigService {


    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminConfigGetVO
     */
    AdminConfigGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminConfigGetVO
     *
     * @param title String 名称
     * @return AdminConfigGetVO
     */
    AdminConfigGetVO getByTitle(@NotNull final String title);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminConfigGetVO
     *
     * @param sole String 标识
     * @return AdminConfigGetVO
     */
    AdminConfigGetVO getBySole(@NotNull final String sole);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminConfigGetVO
     *
     * @param sole String 标识
     * @return AdminConfigGetVO
     */
    AdminConfigGetVO getBySole2Init(@NotNull final String sole);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增配置
     *
     * @param configGetVO AdminConfigGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminConfigGetVO configGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新配置
     *
     * @param configGetVO AdminConfigGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminConfigGetVO configGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除配置(更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminConfigGetVO>
     * @param adminConfigGetVO AdminConfigGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminConfigGetVO>
     */
    IPage<AdminConfigGetVO> list2page(@NotNull final Page<AdminConfigGetVO> pageAdmin,
            @NotNull final AdminConfigGetVO adminConfigGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param configGetVO AdminConfigGetVO
     *
     * @return String
     */
    String validationConfig(@NotNull final AdminConfigGetVO configGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminConfigGetVO 缓存
     *
     * @param id BigInteger
     */
    void removeCacheById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    int getMax2Sort();

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 AdminConfigGetVO  得到 列表
     *
     * @param adminConfigGetVO AdminConfigGetVO
     * @return List
     */
    List<AdminConfigGetVO> list(@NotNull final AdminConfigGetVO adminConfigGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 AdminConfigGetVO  得到 列表
     *
     * @param adminConfigGetVO AdminConfigGetVO
     * @return List
     */
    List<AdminConfigGetVO> list2Init(@NotNull final AdminConfigGetVO adminConfigGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 用于分页 代码生成器专用
     *
     * @param page Page
     * @param tableArray List<String>
     * @return IPage
     */
    IPage<Map<String, String>> listTableByMap(@NotNull final Page<Map<String, String>> page,
            @Nullable final List<String> tableArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 代码生成器专用
     *
     * @param tableName String 表名
     * @return List
     */
    List<Map<String, String>> getTableByTableName(@NotNull final String tableName);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表详情 代码生成器专用
     *
     * @param tableName String
     * @return List
     */
    List<Map<String, String>> getTableDetailByTableName(@NotNull final String tableName);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminConfigService class

/* End of file AdminConfigService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/AdminConfigService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
