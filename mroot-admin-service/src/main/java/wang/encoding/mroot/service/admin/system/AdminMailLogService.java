/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.maillog.AdminMailLogGetVO;

import java.math.BigInteger;
import java.util.List;

/**
 * 后台 电子邮箱记录 Service 接口
 *
 * @author ErYang
 */
public interface AdminMailLogService {


    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminMailLogGetVO
     */
    AdminMailLogGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 发送邮箱 查询 AdminMailLogGetVO
     *
     * @param fromMail String 发送邮箱
     * @return AdminMailLogGetVO
     */
    AdminMailLogGetVO getByFromMail(@NotNull final String fromMail);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 接收邮箱 查询 AdminMailLogGetVO
     *
     * @param toMail String 接收邮箱
     * @return AdminMailLogGetVO
     */
    AdminMailLogGetVO getByToMail(@NotNull final String toMail);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 电子邮箱记录
     *
     * @param mailLogGetVO AdminMailLogGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminMailLogGetVO mailLogGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 电子邮箱记录
     *
     * @param mailLogGetVO AdminMailLogGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminMailLogGetVO mailLogGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 电子邮箱记录 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminMailLogGetVO>
     * @param adminMailLogGetVO AdminMailLogGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminMailLogGetVO>
     */
    IPage<AdminMailLogGetVO> list2page(@NotNull final Page<AdminMailLogGetVO> pageAdmin,
            @NotNull final AdminMailLogGetVO adminMailLogGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminMailLogGetVO AdminMailLogGetVO
     *
     * @return String
     */
    String validationMailLog(@NotNull final AdminMailLogGetVO adminMailLogGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminMailLogGetVO 列表
     * @param adminMailLogGetVO AdminMailLogGetVO
     * @return List
     */
    List<AdminMailLogGetVO> list(@NotNull final AdminMailLogGetVO adminMailLogGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  AdminMailLogGetVO 集合
     * @param adminMailLogGetVO  AdminMailLogGetVO 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    List<AdminMailLogGetVO> list(final AdminMailLogGetVO adminMailLogGetVO, final int count,
            @NotNull final String column, final boolean isAsc);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminMailLogService interface

/* End of file AdminMailLogService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/AdminMailLog.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
