/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.config.AdminConfigBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.enums.ConfigTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.CacheKeyGeneratorUtils;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.ConfigDO;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.service.admin.common.task.AdminBusinessAsyncTask;
import wang.encoding.mroot.service.admin.system.AdminConfigService;
import wang.encoding.mroot.service.system.ConfigService;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 配置后台 Service 实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.ADMIN_CONFIG_CACHE)
public class AdminConfigServiceImpl implements AdminConfigService {

    private final ConfigService configService;

    private final AdminBusinessAsyncTask adminBusinessAsyncTask;

    @Autowired
    @Lazy
    public AdminConfigServiceImpl(ConfigService configService, AdminBusinessAsyncTask adminBusinessAsyncTask) {
        this.configService = configService;
        this.adminBusinessAsyncTask = adminBusinessAsyncTask;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminConfigGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminConfigGetVO getById(@NotNull final BigInteger id) {
        ConfigDO configDO = configService.getTById(id);
        if (null != configDO) {
            return this.configDO2AdminConfigGetVO(configDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminConfigGetVO
     *
     * @param title String 名称
     * @return AdminConfigGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminConfigGetVO getByTitle(@NotNull final String title) {
        ConfigDO configDO = new ConfigDO();
        configDO.setTitle(title);
        ConfigDO configDOInfo = configService.getByModel(configDO);
        if (null != configDOInfo) {
            return this.configDO2AdminConfigGetVO(configDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminConfigGetVO
     *
     * @param sole String 标识
     * @return AdminConfigGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminConfigGetVO getBySole(@NotNull final String sole) {
        ConfigDO configDO = new ConfigDO();
        configDO.setSole(sole);
        ConfigDO configDOInfo = configService.getByModel(configDO);
        if (null != configDOInfo) {
            return this.configDO2AdminConfigGetVO(configDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminConfigGetVO
     *
     * @param sole String 标识
     * @return AdminConfigGetVO
     */
    @Override
    public AdminConfigGetVO getBySole2Init(@NotNull final String sole) {
        ConfigDO configDO = new ConfigDO();
        configDO.setSole(sole);
        ConfigDO configDOInfo = configService.getByModel(configDO);
        if (null != configDOInfo) {
            AdminConfigGetVO adminConfigGetVO1 = new AdminConfigGetVO();
            BeanUtils.copyProperties(configDOInfo, adminConfigGetVO1);
            return adminConfigGetVO1;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增配置
     *
     * @param configGetVO AdminConfigGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminConfigGetVO configGetVO) {
        ConfigDO configDO = BeanMapperComponent.map(configGetVO, ConfigDO.class);
        return configService.saveByT(configDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新配置
     *
     * @param configGetVO AdminConfigGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminConfigGetVO configGetVO) {
        ConfigDO configDO = BeanMapperComponent.map(configGetVO, ConfigDO.class);
        boolean flag = configService.updateById(configDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminConfigGetVOCache(configDO.getId());
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除配置(更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        ConfigDO configDO = new ConfigDO();
        configDO.setId(id);
        configDO.setState(StateEnum.DELETE.getKey());
        configDO.setGmtModified(Date.from(Instant.now()));
        boolean flag = configService.remove2StatusById(configDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminConfigGetVOCache(id);
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        boolean flag = configService.removeBatch2UpdateStatus(idArray);
        if (flag) {
            for (BigInteger id : idArray) {
                adminBusinessAsyncTask.removeAdminConfigGetVOCache(id);
            }
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminConfigGetVO>
     * @param adminConfigGetVO AdminConfigGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminConfigGetVO>
     */
    @Override
    public IPage<AdminConfigGetVO> list2page(@NotNull final Page<AdminConfigGetVO> pageAdmin,
            @NotNull final AdminConfigGetVO adminConfigGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<ConfigDO> configDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        ConfigDO configDO = BeanMapperComponent.map(adminConfigGetVO, ConfigDO.class);
        configService.list2page(configDOPage, configDO, orderByField, isAsc);
        if (null != configDOPage.getRecords() && CollectionUtils.isNotEmpty(configDOPage.getRecords())) {
            List<AdminConfigGetVO> list = ListUtils.newArrayList(configDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (ConfigDO configInfoDO : configDOPage.getRecords()) {
                AdminConfigGetVO configGetVO = this.configDO2AdminConfigGetVO(configInfoDO);
                list.add(configGetVO);
            }
        }
        pageAdmin.setTotal(configDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminConfigGetVO AdminConfigGetVO
     *
     * @return String
     */
    @Override
    public String validationConfig(@NotNull final AdminConfigGetVO adminConfigGetVO) {
        AdminConfigBO configBO = BeanMapperComponent.map(adminConfigGetVO, AdminConfigBO.class);
        return HibernateValidationUtils.validateEntity(configBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminConfigGetVO 缓存
     *
     * @param id BigInteger
     */
    @Override
    public void removeCacheById(@NotNull final BigInteger id) {
        ConfigDO configDO = configService.getTById(id);
        if (null != configDO) {
            AdminConfigGetVO adminConfigGetVO = this.configDO2AdminConfigGetVO(configDO);
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_CONFIG_CACHE, "getById", adminConfigGetVO.getId()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_CONFIG_CACHE, "getByTitle", adminConfigGetVO.getTitle()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_CONFIG_CACHE, "getBySole", adminConfigGetVO.getSole()));
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        return configService.getMax2Sort();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return configService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 AdminConfigGetVO  得到 列表
     *
     * @param adminConfigGetVO AdminConfigGetVO
     * @return List
     */
    @Override
    public List<AdminConfigGetVO> list(@NotNull final AdminConfigGetVO adminConfigGetVO) {
        ConfigDO configDO = BeanMapperComponent.map(adminConfigGetVO, ConfigDO.class);
        List<ConfigDO> list = configService.listByT(configDO);
        if (ListUtils.isNotEmpty(list)) {
            return BeanMapperComponent.mapList(list, AdminConfigGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 AdminConfigGetVO  得到 列表
     *
     * @param adminConfigGetVO AdminConfigGetVO
     * @return List
     */
    @Override
    public List<AdminConfigGetVO> list2Init(@NotNull final AdminConfigGetVO adminConfigGetVO) {
        ConfigDO configDO = new ConfigDO();
        BeanUtils.copyProperties(adminConfigGetVO, configDO);
        List<ConfigDO> list = configService.listByT(configDO);
        if (ListUtils.isNotEmpty(list)) {
            int i;
            List<AdminConfigGetVO> listVO = new ArrayList<>();
            for (i = 0; i < list.size(); i++) {
                AdminConfigGetVO configGetVO = new AdminConfigGetVO();
                BeanUtils.copyProperties(list.get(i), configGetVO);
                listVO.add(configGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 用于分页 代码生成器专用
     *
     * @param page Page
     * @param tableArray List<String>
     * @return IPage
     */
    @Override
    public IPage<Map<String, String>> listTableByMap(@NotNull final Page<Map<String, String>> page,
            @Nullable final List<String> tableArray) {
        return configService.listTableByMap(page, tableArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 代码生成器专用
     *
     * @param tableName String 表名
     * @return List
     */
    @Override
    public List<Map<String, String>> getTableByTableName(@NotNull final String tableName) {
        return configService.getTableByTableName(tableName);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表详情 代码生成器专用
     *
     * @param tableName String
     * @return List
     */
    @Override
    public List<Map<String, String>> getTableDetailByTableName(@NotNull final String tableName) {
        return configService.getTableDetailByTableName(tableName);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ConfigDO 转为 AdminConfigGetVO
     *
     * @param configDO ConfigDO
     * @return AdminConfigGetVO
     */
    private AdminConfigGetVO configDO2AdminConfigGetVO(@NotNull final ConfigDO configDO) {
        AdminConfigGetVO adminConfigGetVO = BeanMapperComponent.map(configDO, AdminConfigGetVO.class);
        if (null != adminConfigGetVO.getState()) {
            adminConfigGetVO.setStatus(StateEnum.getValueByKey(adminConfigGetVO.getState()));
        }
        if (null != adminConfigGetVO.getCategory()) {
            adminConfigGetVO.setType(ConfigTypeEnum.getValueByKey(adminConfigGetVO.getCategory()));
        }
        if (null != adminConfigGetVO.getGmtCreateIp()) {
            adminConfigGetVO.setIp(IpUtils.intToIpv4String(adminConfigGetVO.getGmtCreateIp()));
        }
        return adminConfigGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminConfigServiceImpl class

/* End of file AdminConfigServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/AdminConfigServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
