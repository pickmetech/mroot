/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.user.AdminUserBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.AesManageComponent;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.DigestManageComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.enums.BusinessEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.enums.UserTypeEnum;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.CacheKeyGeneratorUtils;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.PairUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.UserDO;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.service.admin.common.constant.AdminGlobalMessage;
import wang.encoding.mroot.service.admin.common.task.AdminBusinessAsyncTask;
import wang.encoding.mroot.service.admin.system.AdminUserService;
import wang.encoding.mroot.service.system.UserService;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;


import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * 用户后台 Service 实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.ADMIN_USER_CACHE)
public class AdminUserServiceImpl implements AdminUserService {

    private final UserService userService;

    private final AdminBusinessAsyncTask adminBusinessAsyncTask;

    @Autowired
    @Lazy
    public AdminUserServiceImpl(UserService userService, AdminBusinessAsyncTask adminBusinessAsyncTask) {
        this.userService = userService;
        this.adminBusinessAsyncTask = adminBusinessAsyncTask;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有数目
     *
     * @return int
     */
    @Override
    public int getCount() {
        UserDO userDO = new UserDO();
        return userService.countByT(userDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 登录
     *
     * @param username String 用户名
     * @param password String 密码
     * @return ResultData
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public PairUtils login(@NotNull final String username, @NotNull final String password) {
        PairUtils<String, String> pair;
        UserDO userDOResult = userService.login(username, password);
        if (null != userDOResult) {
            if (StateEnum.DISABLE.getKey() == userDOResult.getState()) {
                pair = PairUtils
                        .of(String.valueOf(BusinessEnum.FAILED.getKey()), AdminGlobalMessage.LOGIN_DISABLE_FAILED);
                return pair;
            }
            if (StateEnum.DELETE.getKey() == userDOResult.getState()) {
                pair = PairUtils
                        .of(String.valueOf(BusinessEnum.FAILED.getKey()), AdminGlobalMessage.LOGIN_DELETE_FAILED);
                return pair;
            }
            pair = PairUtils.of(String.valueOf(BusinessEnum.SUCCEED.getKey()), AdminGlobalMessage.LOGIN_SUCCEED);
            return pair;
        } else {
            pair = PairUtils.of(String.valueOf(BusinessEnum.FAILED.getKey()), AdminGlobalMessage.LOGIN_FAILED);
            return pair;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过用户和密码查询
     *
     * @param username String 用户名
     * @param password String 密码
     * @return AdminUserBO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminUserBO getByUsernameAndPassword(@NotNull final String username, @NotNull final String password) {
        UserDO userDO = userService.getByUsernameAndPassword(username, password);
        if (null != userDO) {
            return BeanMapperComponent.map(userDO, AdminUserBO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminUserGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminUserGetVO getById(@NotNull final BigInteger id) {
        UserDO userDO = userService.getTById(id);
        if (null != userDO) {
            AdminUserGetVO adminUserGetVO = this.userDO2AdminUserGetVO(userDO);
            adminUserGetVO.setIp(IpUtils.intToIpv4String(adminUserGetVO.getGmtCreateIp()));
            this.aseDecrypt(adminUserGetVO);
            return adminUserGetVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户名查询 AdminUserGetVO
     *
     * @param username String 用户名
     * @return AdminUserGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminUserGetVO getByUsername(@NotNull final String username) {
        UserDO userDO = new UserDO();
        userDO.setUsername(username);
        UserDO userDOInfo = userService.getByModel(userDO);
        if (null != userDOInfo) {
            return this.userDO2AdminUserGetVO(userDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据昵称查询 AdminUserGetVO
     *
     * @param nickName String 昵称
     * @return AdminUserGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminUserGetVO getByNickName(@NotNull final String nickName) {
        UserDO userDO = new UserDO();
        userDO.setNickName(nickName);
        UserDO userDOInfo = userService.getByModel(userDO);
        if (null != userDOInfo) {
            return this.userDO2AdminUserGetVO(userDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据电子邮箱查询 AdminUserGetVO
     *
     * @param email String 电子邮箱
     * @return AdminUserGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminUserGetVO getByEmail(@NotNull final String email) {
        UserDO userDO = new UserDO();
        userDO.setEmail(AesManageComponent.getEncrypt(email));
        UserDO userDOInfo = userService.getByModel(userDO);
        if (null != userDOInfo) {
            return this.userDO2AdminUserGetVO(userDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据手机号码查询 AdminUserGetVO
     *
     * @param phone String 手机号码
     * @return AdminUserGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminUserGetVO getByPhone(@NotNull final String phone) {
        UserDO userDO = new UserDO();
        userDO.setPhone(AesManageComponent.getEncrypt(phone));
        UserDO userDOInfo = userService.getByModel(userDO);
        if (null != userDOInfo) {
            return this.userDO2AdminUserGetVO(userDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增用户
     *
     * @param userVO AdminUserGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminUserGetVO userVO) {
        UserDO userDO = BeanMapperComponent.map(userVO, UserDO.class);
        // 加密
        this.aseEncrypt(userDO);
        return userService.saveByT(userDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新用户
     *
     * @param userVO AdminUserGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminUserGetVO userVO) {
        UserDO userDO = BeanMapperComponent.map(userVO, UserDO.class);
        boolean flag = userService.updateById(userDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminUserGetVOCache(userVO.getId());
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除用户(更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        UserDO userDO = new UserDO();
        userDO.setId(id);
        userDO.setState(StateEnum.DELETE.getKey());
        userDO.setGmtModified(Date.from(Instant.now()));
        boolean flag = userService.remove2StatusById(userDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminUserGetVOCache(id);
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        boolean flag = userService.removeBatch2UpdateStatus(idArray);
        if (flag) {
            for (BigInteger id : idArray) {
                adminBusinessAsyncTask.removeAdminUserGetVOCache(id);
            }
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复用户(更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean recoverById(@NotNull final BigInteger id) {
        UserDO userDO = new UserDO();
        userDO.setId(id);
        userDO.setState(StateEnum.NORMAL.getKey());
        userDO.setGmtModified(Date.from(Instant.now()));
        boolean flag = userService.recover2StatusById(userDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminUserGetVOCache(id);
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean recoverBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return userService.recoverBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminUserGetVO>
     * @param adminUserGetVO AdminUserGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminUserGetVO>
     */
    @Override
    public IPage<AdminUserGetVO> list2page(@NotNull final Page<AdminUserGetVO> pageAdmin,
            @NotNull final AdminUserGetVO adminUserGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<UserDO> userDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        UserDO userDO = BeanMapperComponent.map(adminUserGetVO, UserDO.class);
        userService.list2page(userDOPage, userDO, orderByField, isAsc);
        if (null != userDOPage.getRecords() && CollectionUtils.isNotEmpty(userDOPage.getRecords())) {
            List<AdminUserGetVO> list = ListUtils.newArrayList(userDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (UserDO userInfoDO : userDOPage.getRecords()) {
                AdminUserGetVO userPageVO = BeanMapperComponent.map(userInfoDO, AdminUserGetVO.class);
                userPageVO.setStatus(StateEnum.getValueByKey(userPageVO.getState()));
                userPageVO.setType(UserTypeEnum.getValueByKey(userPageVO.getCategory()));
                this.aseDecrypt(userPageVO);
                list.add(userPageVO);
            }
        }
        pageAdmin.setTotal(userDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param userVO AdminUserGetVO
     *
     * @return String
     */
    @Override
    public String validationUser(@NotNull final AdminUserGetVO userVO) {
        AdminUserBO userBO = BeanMapperComponent.map(userVO, AdminUserBO.class);
        return HibernateValidationUtils.validateEntity(userBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminUserGetVO 缓存
     *
     * @param id BigInteger
     */
    @Override
    public void removeCacheById(@NotNull final BigInteger id) {
        UserDO userDO = userService.getTById(id);
        if (null != userDO) {
            AdminUserGetVO adminUserGetVO = this.userDO2AdminUserGetVO(userDO);
            adminUserGetVO.setIp(IpUtils.intToIpv4String(adminUserGetVO.getGmtCreateIp()));
            this.aseDecrypt(adminUserGetVO);
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "login", adminUserGetVO.getUsername(),
                            adminUserGetVO.getPassword()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "getByUsernameAndPassword",
                            adminUserGetVO.getUsername(), adminUserGetVO.getPassword()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "getById", adminUserGetVO.getId()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "getByUsername",
                            adminUserGetVO.getUsername()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "getByNickName",
                            adminUserGetVO.getNickName()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "getByEmail", adminUserGetVO.getEmail()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_USER_CACHE, "getByPhone", adminUserGetVO.getPhone()));
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return userService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * UserDO 转为 AdminUserGetVO
     *
     * @param userDO UserDO
     * @return AdminUserGetVO
     */
    private AdminUserGetVO userDO2AdminUserGetVO(@NotNull final UserDO userDO) {
        AdminUserGetVO adminUserGetVO = BeanMapperComponent.map(userDO, AdminUserGetVO.class);
        if (null != adminUserGetVO.getState()) {
            adminUserGetVO.setStatus(StateEnum.getValueByKey(adminUserGetVO.getState()));
        }
        if (null != adminUserGetVO.getCategory()) {
            adminUserGetVO.setType(UserTypeEnum.getValueByKey(adminUserGetVO.getCategory()));
        }
        return adminUserGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 数据解密
     *
     * @param userVO    AdminUserGetVO     用户
     */
    private void aseDecrypt(@NotNull final AdminUserGetVO userVO) {
        // 电子邮箱
        if (null != userVO.getEmail() && StringUtils.isNotBlank(userVO.getEmail())) {
            userVO.setEmail(AesManageComponent.decrypt(userVO.getEmail()));
        }
        // 手机号码
        if (null != userVO.getPhone() && StringUtils.isNotBlank(userVO.getPhone())) {
            userVO.setPhone(AesManageComponent.decrypt(userVO.getPhone()));
        }
        // 真实姓名
        if (null != userVO.getRealName() && StringUtils.isNotBlank(userVO.getRealName())) {
            userVO.setRealName(AesManageComponent.decrypt(userVO.getRealName()));
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 数据加密
     *
     * @param userDO       UserDO  用户
     */
    private void aseEncrypt(@NotNull final UserDO userDO) {
        // 密码
        userDO.setPassword(DigestManageComponent.getSha512(userDO.getPassword()));

        // 电子邮箱
        if (null != userDO.getEmail() && StringUtils.isNotBlank(userDO.getEmail())) {
            userDO.setEmail(AesManageComponent.getEncrypt(userDO.getEmail()));
        }

        // 手机号码
        if (null != userDO.getPhone() && StringUtils.isNotBlank(userDO.getPhone())) {
            userDO.setPhone(AesManageComponent.getEncrypt(userDO.getPhone()));
        }
        // 真实姓名
        if (null != userDO.getRealName() && StringUtils.isNotBlank(userDO.getRealName())) {
            userDO.setRealName(AesManageComponent.getEncrypt(userDO.getRealName()));
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminUserServiceImpl class

/* End of file AdminUserServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/AdminUserServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
