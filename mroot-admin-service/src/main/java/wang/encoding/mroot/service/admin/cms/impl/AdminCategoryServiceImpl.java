/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.cms.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.cms.category.AdminCategoryBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.domain.entity.cms.CategoryDO;
import wang.encoding.mroot.service.admin.cms.AdminCategoryService;
import wang.encoding.mroot.service.cms.CategoryService;
import wang.encoding.mroot.vo.admin.entity.cms.category.AdminCategoryGetVO;
import wang.encoding.mroot.common.enums.CategoryTypeEnum;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 后台 文章分类 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminCategoryServiceImpl implements AdminCategoryService {


    private final CategoryService categoryService;

    @Autowired
    @Lazy
    public AdminCategoryServiceImpl(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminCategoryGetVO
     */
    @Override
    public AdminCategoryGetVO getById(@NotNull final BigInteger id) {
        CategoryDO categoryDO = categoryService.getTById(id);
        if (null != categoryDO) {
            return this.categoryDO2AdminCategoryGetVO(categoryDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminCategoryGetVO
     *
     * @param title String 名称
     * @return AdminCategoryGetVO
     */
    @Override
    public AdminCategoryGetVO getByTitle(@NotNull final String title) {
        CategoryDO categoryDO = new CategoryDO();
        categoryDO.setTitle(title);
        CategoryDO categoryDOInfo = categoryService.getByModel(categoryDO);
        if (null != categoryDOInfo) {
            return this.categoryDO2AdminCategoryGetVO(categoryDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminCategoryGetVO
     *
     * @param sole String 标识
     * @return AdminCategoryGetVO
     */
    @Override
    public AdminCategoryGetVO getBySole(@NotNull final String sole) {
        CategoryDO categoryDO = new CategoryDO();
        categoryDO.setSole(sole);
        CategoryDO categoryDOInfo = categoryService.getByModel(categoryDO);
        if (null != categoryDOInfo) {
            return this.categoryDO2AdminCategoryGetVO(categoryDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章分类
     *
     * @param categoryGetVO AdminCategoryGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminCategoryGetVO categoryGetVO) {
        CategoryDO categoryDO = BeanMapperComponent.map(categoryGetVO, CategoryDO.class);
        return categoryService.saveByT(categoryDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章分类
     *
     * @param categoryGetVO AdminCategoryGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminCategoryGetVO categoryGetVO) {
        CategoryDO categoryDO = BeanMapperComponent.map(categoryGetVO, CategoryDO.class);
        return categoryService.updateById(categoryDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章分类 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        CategoryDO categoryDO = new CategoryDO();
        categoryDO.setId(id);
        categoryDO.setState(StateEnum.DELETE.getKey());
        categoryDO.setGmtModified(Date.from(Instant.now()));
        return categoryService.remove2StatusById(categoryDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 文章分类 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return categoryService.removeBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminCategoryGetVO>
     * @param adminCategoryGetVO AdminCategoryGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminCategoryGetVO>
     */
    @Override
    public IPage<AdminCategoryGetVO> list2page(@NotNull final Page<AdminCategoryGetVO> pageAdmin,
            @NotNull final AdminCategoryGetVO adminCategoryGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<CategoryDO> categoryDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        CategoryDO categoryDO = BeanMapperComponent.map(adminCategoryGetVO, CategoryDO.class);
        categoryService.list2page(categoryDOPage, categoryDO, orderByField, isAsc);
        if (null != categoryDOPage.getRecords() && CollectionUtils.isNotEmpty(categoryDOPage.getRecords())) {
            List<AdminCategoryGetVO> list = ListUtils.newArrayList(categoryDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (CategoryDO categoryInfoDO : categoryDOPage.getRecords()) {
                AdminCategoryGetVO categoryGetVO = this.categoryDO2AdminCategoryGetVO(categoryInfoDO);
                list.add(categoryGetVO);
            }
        }
        pageAdmin.setTotal(categoryDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminCategoryGetVO AdminCategoryGetVO
     *
     * @return String
     */
    @Override
    public String validationCategory(@NotNull final AdminCategoryGetVO adminCategoryGetVO) {
        AdminCategoryBO categoryBO = BeanMapperComponent.map(adminCategoryGetVO, AdminCategoryBO.class);
        return HibernateValidationUtils.validateEntity(categoryBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        return categoryService.getMax2Sort();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return categoryService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminCategoryGetVO 列表
     * @param adminCategoryGetVO AdminCategoryGetVO
     * @return List
     */
    @Override
    public List<AdminCategoryGetVO> list(@NotNull final AdminCategoryGetVO adminCategoryGetVO) {
        CategoryDO categoryDO = BeanMapperComponent.map(adminCategoryGetVO, CategoryDO.class);
        List<CategoryDO> list = categoryService.listByT(categoryDO);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminCategoryGetVO> listVO = new ArrayList<>();
            for (CategoryDO categoryInfoDO : list) {
                AdminCategoryGetVO categoryGetVO = this.categoryDO2AdminCategoryGetVO(categoryInfoDO);
                listVO.add(categoryGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 类型小于 3 文章分类集合
     *
     * @return List
     */
    @Override
    public List<AdminCategoryGetVO> listTypeLt3() {
        List<CategoryDO> list = categoryService.listTypeLt3();
        if (ListUtils.isNotEmpty(list)) {
            return BeanMapperComponent.mapList(list, AdminCategoryGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 文章分类集合
     *
     * @return List
     */
    @Override
    public List<AdminCategoryGetVO> listType() {
        List<CategoryDO> list = categoryService.listType();
        if (ListUtils.isNotEmpty(list)) {
            return BeanMapperComponent.mapList(list, AdminCategoryGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * CategoryDO 转为 AdminCategoryGetVO
     *
     * @param categoryDO CategoryDO
     * @return AdminCategoryGetVO
     */
    private AdminCategoryGetVO categoryDO2AdminCategoryGetVO(@NotNull final CategoryDO categoryDO) {
        AdminCategoryGetVO adminCategoryGetVO = BeanMapperComponent.map(categoryDO, AdminCategoryGetVO.class);
        if (null != adminCategoryGetVO.getState()) {
            adminCategoryGetVO.setStatus(StateEnum.getValueByKey(adminCategoryGetVO.getState()));
        }
        if (null != adminCategoryGetVO.getGmtCreateIp()) {
            adminCategoryGetVO.setIp(IpUtils.intToIpv4String(adminCategoryGetVO.getGmtCreateIp()));
        }
        if (null != adminCategoryGetVO.getCategory()) {
            adminCategoryGetVO.setType(CategoryTypeEnum.getValueByKey(adminCategoryGetVO.getCategory()));
        }
        if (null != adminCategoryGetVO.getPid() && BigIntegerUtils.gt(adminCategoryGetVO.getPid(), BigInteger.ZERO)) {
            AdminCategoryGetVO parentCategory = this.getById(adminCategoryGetVO.getPid());
            if (null != parentCategory) {
                adminCategoryGetVO.setParentCategory(parentCategory);
            }
        }
        return adminCategoryGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminCategoryServiceImpl class

/* End of file AdminCategoryServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/cms/impl/AdminCategoryServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
