/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.cms.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.cms.article.AdminArticleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.cms.ArticleDO;
import wang.encoding.mroot.service.admin.cms.AdminArticleService;
import wang.encoding.mroot.service.admin.cms.AdminCategoryService;
import wang.encoding.mroot.service.cms.ArticleService;
import wang.encoding.mroot.vo.admin.entity.cms.article.AdminArticleGetVO;
import wang.encoding.mroot.vo.admin.entity.cms.category.AdminCategoryGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 后台 文章 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminArticleServiceImpl implements AdminArticleService {


    private final ArticleService articleService;

    private final AdminCategoryService adminCategoryService;

    @Autowired
    @Lazy
    public AdminArticleServiceImpl(ArticleService articleService, AdminCategoryService adminCategoryService) {
        this.articleService = articleService;
        this.adminCategoryService = adminCategoryService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有数目
     *
     * @return int
     */
    @Override
    public int getCount() {
        ArticleDO articleDO = new ArticleDO();
        return articleService.countByT(articleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminArticleGetVO
     */
    @Override
    public AdminArticleGetVO getById(@NotNull final BigInteger id) {
        ArticleDO articleDO = articleService.getTById(id);
        if (null != articleDO) {
            return this.articleDO2AdminArticleGetVO(articleDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminArticleGetVO
     *
     * @param title String 名称
     * @return AdminArticleGetVO
     */
    @Override
    public AdminArticleGetVO getByTitle(@NotNull final String title) {
        ArticleDO articleDO = new ArticleDO();
        articleDO.setTitle(title);
        ArticleDO articleDOInfo = articleService.getByModel(articleDO);
        if (null != articleDOInfo) {
            return this.articleDO2AdminArticleGetVO(articleDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章
     *
     * @param articleGetVO AdminArticleGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminArticleGetVO articleGetVO) {
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        return articleService.saveByT(articleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章
     *
     * @param articleGetVO AdminArticleGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminArticleGetVO articleGetVO) {
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        return articleService.updateById(articleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        ArticleDO articleDO = new ArticleDO();
        articleDO.setId(id);
        articleDO.setState(StateEnum.DELETE.getKey());
        articleDO.setGmtModified(Date.from(Instant.now()));
        return articleService.remove2StatusById(articleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 文章 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return articleService.removeBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminArticleGetVO>
     * @param adminArticleGetVO AdminArticleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminArticleGetVO>
     */
    @Override
    public IPage<AdminArticleGetVO> list2page(@NotNull final Page<AdminArticleGetVO> pageAdmin,
            @NotNull final AdminArticleGetVO adminArticleGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<ArticleDO> articleDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        ArticleDO articleDO = BeanMapperComponent.map(adminArticleGetVO, ArticleDO.class);
        articleService.list2page(articleDOPage, articleDO, orderByField, isAsc);
        if (null != articleDOPage.getRecords() && CollectionUtils.isNotEmpty(articleDOPage.getRecords())) {
            List<AdminArticleGetVO> list = ListUtils.newArrayList(articleDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (ArticleDO articleInfoDO : articleDOPage.getRecords()) {
                AdminArticleGetVO articleGetVO = this.articleDO2AdminArticleGetVO(articleInfoDO);
                list.add(articleGetVO);
            }
        }
        pageAdmin.setTotal(articleDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminArticleGetVO AdminArticleGetVO
     *
     * @return String
     */
    @Override
    public String validationArticle(@NotNull final AdminArticleGetVO adminArticleGetVO) {
        AdminArticleBO articleBO = BeanMapperComponent.map(adminArticleGetVO, AdminArticleBO.class);
        return HibernateValidationUtils.validateEntity(articleBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        return articleService.getMax2Sort();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return articleService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminArticleGetVO 列表
     * @param adminArticleGetVO AdminArticleGetVO
     * @return List
     */
    @Override
    public List<AdminArticleGetVO> list(@NotNull final AdminArticleGetVO adminArticleGetVO) {
        ArticleDO articleDO = BeanMapperComponent.map(adminArticleGetVO, ArticleDO.class);
        List<ArticleDO> list = articleService.listByT(articleDO);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminArticleGetVO> listVO = new ArrayList<>();
            for (ArticleDO articleInfoDO : list) {
                AdminArticleGetVO articleGetVO = this.articleDO2AdminArticleGetVO(articleInfoDO);
                listVO.add(articleGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  AdminArticleGetVO 集合
     * @param adminArticleGetVO  AdminArticleGetVO 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    @Override
    public List<AdminArticleGetVO> list(final AdminArticleGetVO adminArticleGetVO, final int count,
            @NotNull final String column, final boolean isAsc) {
        ArticleDO articleDO = BeanMapperComponent.map(adminArticleGetVO, ArticleDO.class);
        List<ArticleDO> list = articleService.listByT(articleDO, count, column, isAsc);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminArticleGetVO> listVO = new ArrayList<>();
            for (ArticleDO articleInfoDO : list) {
                AdminArticleGetVO articleGetVO = this.articleDO2AdminArticleGetVO(articleInfoDO);
                listVO.add(articleGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ArticleDO 转为 AdminArticleGetVO
     *
     * @param articleDO ArticleDO
     * @return AdminArticleGetVO
     */
    private AdminArticleGetVO articleDO2AdminArticleGetVO(@NotNull final ArticleDO articleDO) {
        AdminArticleGetVO adminArticleGetVO = BeanMapperComponent.map(articleDO, AdminArticleGetVO.class);
        if (null != adminArticleGetVO.getState()) {
            adminArticleGetVO.setStatus(StateEnum.getValueByKey(adminArticleGetVO.getState()));
        }
        if (null != adminArticleGetVO.getGmtCreateIp()) {
            adminArticleGetVO.setIp(IpUtils.intToIpv4String(adminArticleGetVO.getGmtCreateIp()));
        }
        if (null != adminArticleGetVO.getCategoryId()) {
            AdminCategoryGetVO adminCategoryGetVO = adminCategoryService.getById(adminArticleGetVO.getCategoryId());
            if (null != adminCategoryGetVO) {
                adminArticleGetVO.setCategory(adminCategoryGetVO);
            }
        }
        return adminArticleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminArticleServiceImpl class

/* End of file AdminArticleServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/cms/impl/AdminArticleServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
