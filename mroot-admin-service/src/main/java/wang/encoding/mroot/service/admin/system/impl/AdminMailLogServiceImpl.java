/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.maillog.AdminMailLogBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.MailLogDO;
import wang.encoding.mroot.service.admin.system.AdminMailLogService;
import wang.encoding.mroot.service.system.MailLogService;
import wang.encoding.mroot.vo.admin.entity.system.maillog.AdminMailLogGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 后台 电子邮箱记录 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminMailLogServiceImpl implements AdminMailLogService {


    private final MailLogService mailLogService;

    @Autowired
    @Lazy
    public AdminMailLogServiceImpl(MailLogService mailLogService) {
        this.mailLogService = mailLogService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminMailLogGetVO
     */
    @Override
    public AdminMailLogGetVO getById(@NotNull final BigInteger id) {
        MailLogDO mailLogDO = mailLogService.getTById(id);
        if (null != mailLogDO) {
            return this.mailLogDO2AdminMailLogGetVO(mailLogDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 发送邮箱 查询 AdminMailLogGetVO
     *
     * @param fromMail String 发送邮箱
     * @return AdminMailLogGetVO
     */
    @Override
    public AdminMailLogGetVO getByFromMail(@NotNull final String fromMail) {
        MailLogDO mailLogDO = new MailLogDO();
        mailLogDO.setFromMail(fromMail);
        MailLogDO mailLogDOInfo = mailLogService.getByModel(mailLogDO);
        if (null != mailLogDOInfo) {
            return this.mailLogDO2AdminMailLogGetVO(mailLogDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 接收邮箱 查询 AdminMailLogGetVO
     *
     * @param toMail String 接收邮箱
     * @return AdminMailLogGetVO
     */
    @Override
    public AdminMailLogGetVO getByToMail(@NotNull final String toMail) {
        MailLogDO mailLogDO = new MailLogDO();
        mailLogDO.setToMail(toMail);
        MailLogDO mailLogDOInfo = mailLogService.getByModel(mailLogDO);
        if (null != mailLogDOInfo) {
            return this.mailLogDO2AdminMailLogGetVO(mailLogDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 电子邮箱记录
     *
     * @param mailLogGetVO AdminMailLogGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminMailLogGetVO mailLogGetVO) {
        MailLogDO mailLogDO = BeanMapperComponent.map(mailLogGetVO, MailLogDO.class);
        return mailLogService.saveByT(mailLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 电子邮箱记录
     *
     * @param mailLogGetVO AdminMailLogGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminMailLogGetVO mailLogGetVO) {
        MailLogDO mailLogDO = BeanMapperComponent.map(mailLogGetVO, MailLogDO.class);
        return mailLogService.updateById(mailLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 电子邮箱记录 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        MailLogDO mailLogDO = new MailLogDO();
        mailLogDO.setId(id);
        mailLogDO.setState(StateEnum.DELETE.getKey());
        mailLogDO.setGmtModified(Date.from(Instant.now()));
        return mailLogService.remove2StatusById(mailLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 电子邮箱记录 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return mailLogService.removeBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminMailLogGetVO>
     * @param adminMailLogGetVO AdminMailLogGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminMailLogGetVO>
     */
    @Override
    public IPage<AdminMailLogGetVO> list2page(@NotNull final Page<AdminMailLogGetVO> pageAdmin,
            @NotNull final AdminMailLogGetVO adminMailLogGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<MailLogDO> mailLogDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        MailLogDO mailLogDO = BeanMapperComponent.map(adminMailLogGetVO, MailLogDO.class);
        mailLogService.list2page(mailLogDOPage, mailLogDO, orderByField, isAsc);
        if (null != mailLogDOPage.getRecords() && CollectionUtils.isNotEmpty(mailLogDOPage.getRecords())) {
            List<AdminMailLogGetVO> list = ListUtils.newArrayList(mailLogDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (MailLogDO mailLogInfoDO : mailLogDOPage.getRecords()) {
                AdminMailLogGetVO mailLogGetVO = this.mailLogDO2AdminMailLogGetVO(mailLogInfoDO);
                list.add(mailLogGetVO);
            }
        }
        pageAdmin.setTotal(mailLogDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminMailLogGetVO AdminMailLogGetVO
     *
     * @return String
     */
    @Override
    public String validationMailLog(@NotNull final AdminMailLogGetVO adminMailLogGetVO) {
        AdminMailLogBO mailLogBO = BeanMapperComponent.map(adminMailLogGetVO, AdminMailLogBO.class);
        return HibernateValidationUtils.validateEntity(mailLogBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminMailLogGetVO 列表
     * @param adminMailLogGetVO AdminMailLogGetVO
     * @return List
     */
    @Override
    public List<AdminMailLogGetVO> list(@NotNull final AdminMailLogGetVO adminMailLogGetVO) {
        MailLogDO mailLogDO = BeanMapperComponent.map(adminMailLogGetVO, MailLogDO.class);
        List<MailLogDO> list = mailLogService.listByT(mailLogDO);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminMailLogGetVO> listVO = new ArrayList<>();
            for (MailLogDO mailLogInfoDO : list) {
                AdminMailLogGetVO mailLogGetVO = this.mailLogDO2AdminMailLogGetVO(mailLogInfoDO);
                listVO.add(mailLogGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  AdminMailLogGetVO 集合
     * @param adminMailLogGetVO  AdminMailLogGetVO 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    @Override
    public List<AdminMailLogGetVO> list(final AdminMailLogGetVO adminMailLogGetVO, final int count,
            @NotNull final String column, final boolean isAsc) {
        MailLogDO mailLogDO = BeanMapperComponent.map(adminMailLogGetVO, MailLogDO.class);
        List<MailLogDO> list = mailLogService.listByT(mailLogDO, count, column, isAsc);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminMailLogGetVO> listVO = new ArrayList<>();
            for (MailLogDO mailLogInfoDO : list) {
                AdminMailLogGetVO mailLogGetVO = this.mailLogDO2AdminMailLogGetVO(mailLogInfoDO);
                listVO.add(mailLogGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * MailLogDO 转为 AdminMailLogGetVO
     *
     * @param mailLogDO MailLogDO
     * @return AdminMailLogGetVO
     */
    private AdminMailLogGetVO mailLogDO2AdminMailLogGetVO(@NotNull final MailLogDO mailLogDO) {
        AdminMailLogGetVO adminMailLogGetVO = BeanMapperComponent.map(mailLogDO, AdminMailLogGetVO.class);
        if (null != adminMailLogGetVO.getState()) {
            adminMailLogGetVO.setStatus(StateEnum.getValueByKey(adminMailLogGetVO.getState()));
        }
        if (null != adminMailLogGetVO.getGmtCreateIp()) {
            adminMailLogGetVO.setIp(IpUtils.intToIpv4String(adminMailLogGetVO.getGmtCreateIp()));
        }
        return adminMailLogGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminMailLogServiceImpl class

/* End of file AdminMailLogServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/AdminMailLogServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
