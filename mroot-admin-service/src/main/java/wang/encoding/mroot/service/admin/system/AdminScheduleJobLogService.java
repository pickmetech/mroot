/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.schedulejoblog.AdminScheduleJobLogGetVO;

import java.math.BigInteger;


/**
 * 后台 定时任务记录 Service 接口
 *
 * @author ErYang
 */
public interface AdminScheduleJobLogService {


    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminScheduleJobLogGetVO
     */
    AdminScheduleJobLogGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminScheduleJobLogGetVO
     *
     * @param title String 名称
     * @return AdminScheduleJobLogGetVO
     */
    AdminScheduleJobLogGetVO getByTitle(@NotNull final String title);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 定时任务记录
     *
     * @param scheduleJobLogGetVO AdminScheduleJobLogGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminScheduleJobLogGetVO scheduleJobLogGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务记录
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean deleteById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 定时任务记录
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean deleteBatch(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminScheduleJobLogGetVO>
     * @param adminScheduleJobLogGetVO AdminScheduleJobLogGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminScheduleJobLogGetVO>
     */
    IPage<AdminScheduleJobLogGetVO> list2page(@NotNull final Page<AdminScheduleJobLogGetVO> pageAdmin,
            @NotNull final AdminScheduleJobLogGetVO adminScheduleJobLogGetVO, @Nullable String orderByField,
            boolean isAsc);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobLogService interface

/* End of file ScheduleJobLogService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/ScheduleJobLog.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
