package wang.encoding.mroot.common.util.concurrent.limiter;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


/**
 *时间间隔限制器
 *
 * @author ErYang
 */
public class TimeIntervalLimiterUtils {

    private final AtomicLong lastTimeAtom = new AtomicLong(0);

    private long windowSizeMillis;

    public TimeIntervalLimiterUtils(long interval, TimeUnit timeUnit) {
        this.windowSizeMillis = timeUnit.toMillis(interval);
    }

    // -------------------------------------------------------------------------------------------------

    public boolean tryAcquire() {
        long currentTime = System.currentTimeMillis();
        long lastTime = lastTimeAtom.get();
        return currentTime - lastTime >= windowSizeMillis && lastTimeAtom.compareAndSet(lastTime, currentTime);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End TimeIntervalLimiterUtils class

/* End of file TimeIntervalLimiterUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/concurrent/limiter/TimeIntervalLimiterUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
