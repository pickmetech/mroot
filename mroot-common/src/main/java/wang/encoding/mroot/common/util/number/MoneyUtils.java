/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.number;


import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.annotation.NotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * 货币工具类
 *
 * 1.元和分之间的转换
 * 2.货币格式化成字符串
 * 3.字符串转换成货币
 *
 * @author ErYang
 *
 */
public class MoneyUtils {

    private static final ThreadLocal<DecimalFormat> DEFAULT_FORMAT = createThreadLocalNumberFormat("0.00");

    private static final ThreadLocal<DecimalFormat> PRETTY_FORMAT = createThreadLocalNumberFormat("#,##0.00");

    /**
     * ThreadLocal重用MessageDigest
     * @param pattern String
     * @return ThreadLocal
     */
    private static ThreadLocal<DecimalFormat> createThreadLocalNumberFormat(final String pattern) {
        return ThreadLocal.withInitial(() -> {
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern(pattern);
            return df;
        });
    }

    // -------------------------------------------------------------------------------------------------

    /* 元和分的转换 */

    /**
     * 人民币金额单位转换，分转换成元，取两位小数 例如：150 => 1.5
     *
     * @param num BigDecimal
     * @return BigDecimal
     */
    public static BigDecimal fen2yuan(@NotNull final BigDecimal num) {
        return num.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 人民币金额单位转换，分转换成元，取两位小数 例如：150 => 1.5
     *
     * @param num long
     * @return BigDecimal
     */
    public static BigDecimal fen2yuan(final long num) {
        return fen2yuan(new BigDecimal(num));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 人民币金额单位转换，分转换成元，取两位小数 例如：150 => 1.5
     *
     * @param num String
     * @return BigDecimal
     */
    public static BigDecimal fen2yuan(@NotNull final String num) {
        if (StringUtils.isEmpty(num)) {
            return new BigDecimal(0);
        }
        return fen2yuan(new BigDecimal(num));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 人民币金额单位转换，元转换成分，例如：1 => 100
     *
     * @param y String
     * @return BigDecimal
     */
    public static BigDecimal yuan2fen(@NotNull final String y) {
        return new BigDecimal(Math.round(new BigDecimal(y).multiply(new BigDecimal(100)).doubleValue()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 人民币金额单位转换，元转换成分，例如：1 => 100
     *
     * @param y double
     * @return BigDecimal
     */
    public static BigDecimal yuan2fen(final double y) {
        return yuan2fen(String.valueOf(y));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 人民币金额单位转换，元转换成分，例如：1 => 100
     *
     * @param y BigDecimal
     * @return BigDecimal
     */
    public static BigDecimal yuan2fen(@NotNull final BigDecimal y) {
        if (y != null) {
            return yuan2fen(y.toString());
        } else {
            return new BigDecimal(0);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /* 格式化输出 */

    /**
     * 格式化金额，例如：1=>1.00
     *
     * @param number BigDecimal
     * @return String
     */
    public static String format(@NotNull final BigDecimal number) {
        return format(number.doubleValue());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 格式化金额，默认格式：00.0 ,例如：1=>1.00
     *
     * @param number double
     * @return String
     */
    public static String format(final double number) {
        return DEFAULT_FORMAT.get().format(number);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 格式化金额，默认格式：#,##0.00 ,例如：33999999932.3333d 输出：33,999,999,932.33
     *
     * @param number BigDecimal
     * @return String
     */
    public static String prettyFormat(@NotNull final BigDecimal number) {
        return prettyFormat(number.doubleValue());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 格式化金额，默认格式：#,##0.00 ,例如：33999999932.3333d 输出：33,999,999,932.33
     *
     * @param number double
     * @return String
     */
    public static String prettyFormat(final double number) {
        return PRETTY_FORMAT.get().format(number);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 格式化金额，当pattern为空时，pattern默认为#,##0.00
     *
     * @param number BigDecimal
     * @param pattern String
     * @return String
     */
    public static String format(@NotNull final BigDecimal number, @NotNull final String pattern) {
        return format(number.doubleValue(), pattern);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 格式化金额，当pattern为空时，pattern默认为#,##0.00
     *
     * @param number double
     * @param pattern String
     * @return String
     */
    public static String format(final double number, @NotNull final String pattern) {
        DecimalFormat df;
        if (StringUtils.isEmpty(pattern)) {
            df = PRETTY_FORMAT.get();
        } else {
            df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern(pattern);
        }
        return df.format(number);
    }

    // -------------------------------------------------------------------------------------------------

    /* 转换金额字符串为金额 */

    /**
     * 分析格式为0.00格式的字符串
     *
     * @param numberStr String
     * @return BigDecimal
     * @throws ParseException
     */
    public static BigDecimal parseString(@NotNull final String numberStr) throws ParseException {
        return new BigDecimal(DEFAULT_FORMAT.get().parse(numberStr).doubleValue());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 分析格式为#,##0.00格式的字符串
     *
     * @param numberStr String
     * @return BigDecimal
     * @throws ParseException
     */
    public static BigDecimal parsePrettyString(@NotNull final String numberStr) throws ParseException {
        return new BigDecimal(PRETTY_FORMAT.get().parse(numberStr).doubleValue());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按格式分析字符串，当pattern为空时，pattern默认为#,##0.00
     *
     * @param numberStr String
     * @param pattern String
     * @return BigDecimal
     * @throws ParseException
     */
    public static BigDecimal parseString(@NotNull final String numberStr, @NotNull final String pattern)
            throws ParseException {
        DecimalFormat df;
        if (StringUtils.isEmpty(pattern)) {
            df = PRETTY_FORMAT.get();
        } else {
            df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern(pattern);
        }
        return new BigDecimal(df.parse(numberStr).doubleValue());
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MoneyUtils class

/* End of file MoneyUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/number/MoneyUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
