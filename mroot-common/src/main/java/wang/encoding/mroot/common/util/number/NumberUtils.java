/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.number;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

import java.util.Locale;

/**
 * 数字的工具类
 * 1.原始类型数字与byte[]的双向转换(Guava)
 * 2.判断字符串是否数字, 是否16进制字符串(Common Lang3)
 * 3.10机制/16进制字符串 与 原始类型数字/数字对象 的双向转换(参考Common Lang3自写)
 *
 * @author ErYang
 */
public class NumberUtils {

    private static final double DEFAULT_DOUBLE_EPSILON = 0.00001d;

    /**
     * 因为double的精度问题, 允许两个double在0.00001内的误差为相等
     * @param d1 double
     * @param d2 double
     * @return boolean
     */
    public static boolean equalsWithin(@NotNull final double d1, @NotNull final double d2) {
        return DEFAULT_DOUBLE_EPSILON > Math.abs(d1 - d2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 因为double的精度问题, 允许两个double在epsilon内的误差为相等
     *
     * @param d1 double
     * @param d2 double
     * @param epsilon double
     * @return boolean
     */
    public static boolean equalsWithin(@NotNull final double d1, @NotNull final double d2,
            @NotNull final double epsilon) {
        return Math.abs(d1 - d2) < epsilon;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * bytes[] 与原始类型数字转换
     *
     * @param value int
     * @return byte[]
     */
    public static byte[] toBytes(@NotNull final int value) {
        return Ints.toByteArray(value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * bytes[] 与原始类型数字转换
     *
     * @param value long
     * @return byte[]
     */
    public static byte[] toBytes(@NotNull final long value) {
        return Longs.toByteArray(value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * bytes[] 与原始类型数字转换 copy from ElasticSearch Numbers
     *
     * @param val double
     * @return byte[]
     */
    public static byte[] toBytes(@NotNull final double val) {
        return toBytes(Double.doubleToRawLongBits(val));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * bytes[] 与原始类型数字转换
     *
     * @param bytes byte[]
     * @return int
     */
    public static int toInt(@NotNull final byte[] bytes) {
        return Ints.fromByteArray(bytes);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * bytes[] 与原始类型数字转换
     *
     * @param bytes byte[]
     * @return long
     */
    public static long toLong(@NotNull final byte[] bytes) {
        return Longs.fromByteArray(bytes);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * bytes[] 与原始类型数字转换 copy from ElasticSearch Numbers
     *
     * @param bytes byte[]
     * @return double
     */
    public static double toDouble(@NotNull final byte[] bytes) {
        return Double.longBitsToDouble(toLong(bytes));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断字符串是否合法数字
     *
     * @param str String
     * @return boolean
     */
    public static boolean isNumber(@Nullable final String str) {
        return org.apache.commons.lang3.math.NumberUtils.isCreatable(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断字符串是否16进制
     *
     * @param value String
     * @return boolean
     */
    public static boolean isHexNumber(@Nullable final String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }

        int index = value.startsWith("-") ? 1 : 0;
        return value.startsWith("0x", index) || value.startsWith("0X", index) || value.startsWith("#", index);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String转化为int
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return int
     */
    public static int toInt(@NotNull final String str) {
        return Integer.parseInt(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为int.
     * 当str为空或非数字字符串时，返回default值.
     *
     * @param str String
     * @param defaultValue int
     * @return int
     */
    public static int toInt(@Nullable final String str, @NotNull final int defaultValue) {
        return org.apache.commons.lang3.math.NumberUtils.toInt(str, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为long.
     * 当str或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return long
     */
    public static long toLong(@NotNull final String str) {
        return Long.parseLong(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为long.
     * 当str为空或非数字字符串时，返回default值
     *
     * @param str String
     * @param defaultValue long
     * @return long
     */
    public static long toLong(@Nullable final String str, @NotNull final long defaultValue) {
        return org.apache.commons.lang3.math.NumberUtils.toLong(str, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为double.
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return double
     */
    public static double toDouble(@NotNull final String str) {
        // 统一行为，不要有时候抛NPE，有时候抛NumberFormatException
        if (null == str) {
            throw new NumberFormatException("null");
        }
        return Double.parseDouble(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为double
     * 当str为空或非数字字符串时，返回default值
     *
     * @param str String
     * @param defaultValue double
     * @return double
     */
    public static double toDouble(@Nullable final String str, @NotNull final double defaultValue) {
        return org.apache.commons.lang3.math.NumberUtils.toDouble(str, defaultValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为Integer
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return Integer
     */
    public static Integer toIntObject(@NotNull final String str) {
        return Integer.valueOf(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为Integer.
     * 当str为空或非数字字符串时，返回default值
     *
     * @param str String
     * @param defaultValue Integer
     * @return Integer
     */
    public static Integer toIntObject(@Nullable final String str, @NotNull final Integer defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        try {
            return Integer.valueOf(str);
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为Long
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return Long
     */
    public static Long toLongObject(@NotNull final String str) {
        return Long.valueOf(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为Long
     * 当str为空或非数字字符串时，返回default值
     *
     * @param str String
     * @param defaultValue Long
     * @return Long
     */
    public static Long toLongObject(@Nullable final String str, @NotNull final Long defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        try {
            return Long.valueOf(str);
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为Double
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return Double
     */
    public static Double toDoubleObject(@NotNull final String str) {
        // 统一行为，不要有时候抛NPE，有时候抛NumberFormatException
        if (null == str) {
            throw new NumberFormatException("null");
        }
        return Double.valueOf(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将10进制的String安全的转化为Long
     * 当str为空或非数字字符串时，返回default值
     *
     * @param str String
     * @param defaultValue Double
     * @return Double
     */
    public static Double toDoubleObject(@Nullable final String str, @NotNull final Double defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        try {
            return Double.valueOf(str);
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将16进制的String转化为Integer
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return Integer
     */
    public static Integer hexToIntObject(@NotNull final String str) {
        // 统一行为，不要有时候抛NPE，有时候抛NumberFormatException
        if (null == str) {
            throw new NumberFormatException("null");
        }
        return Integer.decode(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将16进制的String转化为Integer，出错时返回默认值.
     *
     * @param str String
     * @param defaultValue  Integer
     * @return Integer
     */
    public static Integer hexToIntObject(@Nullable final String str, @NotNull final Integer defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        try {
            return Integer.decode(str);
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将16进制的String转化为Long
     * 当str为空或非数字字符串时抛NumberFormatException
     *
     * @param str String
     * @return Long
     */
    public static Long hexToLongObject(@NotNull final String str) {
        // 统一行为，不要有时候抛NPE，有时候抛NumberFormatException
        if (null == str) {
            throw new NumberFormatException("null");
        }
        return Long.decode(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将16进制的String转化为Long，出错时返回默认值
     *
     * @param str String
     * @param defaultValue Long
     * @return Long
     */
    public static Long hexToLongObject(@Nullable final String str, @NotNull final Long defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        try {
            return Long.decode(str);
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义了原子类型与对象类型的参数，保证不会用错函数会导致额外AutoBoxing转换
     *
     * @param i int
     * @return String
     */
    public static String toString(final int i) {
        return Integer.toString(i);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义了原子类型与对象类型的参数，保证不会用错函数会导致额外AutoBoxing转换
     *
     * @param i Integer
     * @return String
     */
    public static String toString(@NotNull final Integer i) {
        return i.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义了原子类型与对象类型的参数，保证不会用错函数会导致额外AutoBoxing转换
     *
     * @param l long
     * @return String
     */
    public static String toString(final long l) {
        return Long.toString(l);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义了原子类型与对象类型的参数，保证不会用错函数会导致额外AutoBoxing转换
     *
     * @param l Long
     * @return String
     */
    public static String toString(@NotNull final Long l) {
        return l.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义了原子类型与对象类型的参数，保证不会用错函数会导致额外AutoBoxing转换
     *
     * @param d double
     * @return String
     */
    public static String toString(final double d) {
        return Double.toString(d);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义了原子类型与对象类型的参数，保证不会用错函数会导致额外AutoBoxing转换
     *
     * @param d Double
     * @return String
     */
    public static String toString(@NotNull final Double d) {
        return d.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 输出格式化为小数后两位的double字符串
     *
     * @param d double
     * @return String
     */
    public static String to2DigitString(final double d) {
        return String.format(Locale.ROOT, "%.2f", d);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *
     * 安全的将小于Integer.MAX的long转为int，否则抛出IllegalArgumentException异常
     *
     * @param x long
     * @return int
     */
    public static int toInt32(final long x) {
        if ((int) x == x) {
            return (int) x;
        }
        throw new IllegalArgumentException(">>>>>>>>" + x + "超出int范围<<<<<<<<");
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End NumberUtils class

/* End of file NumberUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/number/NumberUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
