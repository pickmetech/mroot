package wang.encoding.mroot.common.util;


import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import wang.encoding.mroot.common.annotation.Nullable;

import java.io.Serializable;

/**
 * 引入一个简简单单的 PairUtils 用于返回值返回两个元素
 * Twitter Common
 *
 * @author ErYang
 */
public class PairUtils<L, R> implements Serializable {


    private static final long serialVersionUID = 8278243672708905417L;


    @Nullable
    private final L left;
    @Nullable
    private final R right;


    public PairUtils(@Nullable L left, @Nullable R right) {
        this.left = left;
        this.right = right;
    }

    // -------------------------------------------------------------------------------------------------

    @Nullable
    public L getLeft() {
        return left;
    }

    // -------------------------------------------------------------------------------------------------

    @Nullable
    public R getRight() {
        return right;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的泛型 自动构造合适的 PairUtils
     */
    public static <L, R> PairUtils<L, R> of(@Nullable L left, @Nullable R right) {
        return new PairUtils<>(left, right);
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (null == o || getClass() != o.getClass()) {
            return false;
        }
        PairUtils other = (PairUtils) o;
        if (null == left) {
            if (null != other.left) {
                return false;
            }
        } else if (!left.equals(other.left)) {
            return false;
        }
        if (null == right) {
            return null == other.right;
        } else {
            return right.equals(other.right);
        }
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(left).append(right).toHashCode();
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("left", left).append("right", right).toString();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End PairUtils class

/* End of file PairUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/PairUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
