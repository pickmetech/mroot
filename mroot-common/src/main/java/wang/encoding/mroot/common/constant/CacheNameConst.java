/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.constant;


/**
 * Cache 缓存名称
 *
 * @author ErYang
 */
public class CacheNameConst {

    /**
     * 缓存分隔符
     */
    public static final String REDIS_CACHE_SEPARATOR = "::";
    /**
     * 公共 缓存 前缀
     */
    public static final String COMMON_PREFIX = "commonCache";
    /**
     * 重置 是
     */
    public static final String COMMON_RELOAD_YES = "yes";
    /**
     * 重置 否
     */
    public static final String COMMON_RELOAD_NO = "no";
    /**
     * 重置数据库系统配置
     */
    public static final String COMMON_RELOAD_DATABASE_CONFIG = "commonReloadConfigCache";
    /**
     * 重置资源文件
     */
    public static final String COMMON_RELOAD_RESOURCE = "commonReloadResourceCache";
    /**
     * 后台 缓存 前缀
     */
    public static final String ADMIN_PREFIX = "admin";
    /**
     * 后台 configCache
     */
    public static final String ADMIN_CONFIG_CACHE = "adminConfigCache";
    /**
     * 后台 ruleCache
     */
    public static final String ADMIN_RULE_CACHE = "adminRuleCache";
    /**
     * 后台 userCache
     */
    public static final String ADMIN_USER_CACHE = "adminUserCache";
    /**
     * 后台 roleCache
     */
    public static final String ADMIN_ROLE_CACHE = "adminRoleCache";
    /**
     * 后台 shiroSession
     */
    public static final String ADMIN_SHIRO_SESSION_CACHE = "adminShiroSession";
    /**
     * 博客 缓存 前缀
     */
    public static final String BLOG_PREFIX = "blog";
    /**
     * 博客 categoryCache
     */
    public static final String BLOG_CATEGORY_CACHE = "blogCategoryCache";
    /**
     * 博客 articleCache
     */
    public static final String BLOG_ARTICLE_CACHE = "blogArticleCache";

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CacheNameConst class

/* End of file CacheNameConst.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/constant/CacheNameConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
