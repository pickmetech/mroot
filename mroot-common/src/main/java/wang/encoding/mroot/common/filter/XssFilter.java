/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.filter;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.util.BooleanUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 拦截防止 xss 注入
 * 通过 jsoup 过滤请求参数内的特定字符
 *
 * @author ErYang
 */
@Slf4j
public class XssFilter implements Filter {

    /**
     * 是否过滤富文本内容
     */
    private static boolean IS_INCLUDE_RICH_TEXT = false;

    /**
     * .标识
     */
    private static final String DOT_IDENTIFY = ".";

    private List<String> excludes = ListUtils.newArrayList();

    @Override
    public void init(FilterConfig filterConfig) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>XssFilter Init<<<<<<<<");
        }
        // 过滤富文本
        String isIncludeRichText = filterConfig.getInitParameter("isIncludeRichText");
        if (StringUtils.isNotBlank(isIncludeRichText)) {
            IS_INCLUDE_RICH_TEXT = BooleanUtils.toBoolean(isIncludeRichText);
        }
        // 排除
        String temp = filterConfig.getInitParameter("excludes");
        if (StringUtils.isNotBlank(temp)) {
            String[] url = ",".split(temp);
            if (ArrayUtils.isNotEmpty(url)) {
                int i;
                int length = url.length;
                for (i = 0; i < length; i++) {
                    excludes.add(url[i]);
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String uri = httpServletRequest.getRequestURI();

        if (!uri.contains(DOT_IDENTIFY)) {
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>XssFilter[{}]<<<<<<<<", httpServletRequest.getRequestURI());
            }

            if (this.handleExcludeUrl(httpServletRequest)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
            XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper(httpServletRequest,
                    IS_INCLUDE_RICH_TEXT);
            filterChain.doFilter(xssRequest, servletResponse);
        } else {
            // 静态请求直接跳出
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void destroy() {

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 过滤 url
     *
     * @param request  HttpServletRequest
     *
     * @return boolean
     */
    private boolean handleExcludeUrl(final HttpServletRequest request) {
        if (excludes.isEmpty()) {
            return false;
        }
        // url
        String url = request.getServletPath();
        Pattern p;
        Matcher m;
        for (String pattern : excludes) {
            p = Pattern.compile(pattern);
            m = p.matcher(url);
            if (m.find()) {
                return true;
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End XssFilter class

/* End of file XssFilter.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/filter/XssFilter.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
