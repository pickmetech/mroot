/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.io;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.util.ExceptionUtils;
import wang.encoding.mroot.common.util.PlatformsUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * 关于文件的工具集
 * 主要是调用 JDK 自带的 Files 工具类，少量代码调用 Guava Files 固定 encoding 为 UTF8
 * 1.文件读写
 * 2.文件及目录操作
 *
 * @author ErYang
 */
@Slf4j
public class FileUtils {


    /**
     * 获取文件名(不包含路径)
     *
     * @param fullName String
     * @return String
     */
    public static String getFileName(@NotNull final String fullName) {
        int last = fullName.lastIndexOf(PlatformsUtils.FILE_PATH_SEPARATOR_CHAR);
        return fullName.substring(last + 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取文件名的扩展名部分(不包含.)
     *
     *  com.google.common.io.Files
     *
     * @param file File
     * @return String
     */
    public static String getFileExtension(final File file) {
        return com.google.common.io.Files.getFileExtension(file.getName());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取文件名的扩展名部分(不包含.)
     *
     * com.google.common.io.Files
     *
     * @param fullName String
     * @return String
     */
    public static String getFileExtension(final String fullName) {
        return com.google.common.io.Files.getFileExtension(fullName);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除目录及所有子目录
     * @param dir File
     */
    public static void deleteDir(@NotNull final File dir) {
        if (dir.exists()) {
            deleteDir(dir.toPath());
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 计算目录或文件的总大小
     * 当给定对象为文件时，直接调用 File.length()
     * 当给定对象为目录时，遍历目录下的所有文件和目录，递归计算其大小，求和返回
     *
     * @param file 目录或文件
     * @return long 总大小，bytes长度
     */
    public static long size(@NotNull final File file) {
        if (file.isDirectory()) {
            long size = 0L;
            File[] subFiles = file.listFiles();
            if (ArrayUtils.isEmpty(subFiles)) {
                return 0L;
            }
            if (null != subFiles) {
                for (File subFile : subFiles) {
                    size += size(subFile);
                }
            }
            return size;
        } else {
            return file.length();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建文件或更新时间戳
     *
     * com.google.common.io.Files
     *
     * @param filePath String
     * @throws IOException
     */
    public static void touch(@NotNull final String filePath) throws IOException {
        touch(new File(filePath));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建文件或更新时间戳
     *
     * com.google.common.io.Files
     *
     * @param file File
     * @throws IOException
     */
    public static void touch(@NotNull final File file) throws IOException {
        com.google.common.io.Files.touch(file);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断目录是否存在, from Jodd
     *
     * @param dirPath String
     * @return boolean
     */
    public static boolean isDirExists(@NotNull final String dirPath) {
        if (null == dirPath) {
            return false;
        }
        return isDirExists(getPath(dirPath));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断目录是否存在, from Jodd
     *
     * @param dirPath Path
     * @return boolean
     */
    public static boolean isDirExists(@NotNull final Path dirPath) {
        return null != dirPath && Files.exists(dirPath) && Files.isDirectory(dirPath);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断目录是否存在, from Jodd
     *
     * @param dir File
     * @return boolean
     */
    public static boolean isDirExists(@NotNull final File dir) {
        if (null == dir) {
            return false;
        }
        return isDirExists(dir.toPath());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 确保目录存在, 如不存在则创建
     *
     * @param dirPath String
     * @throws IOException
     */
    public static void makefileDirExists(@NotNull final String dirPath) throws IOException {
        makefileDirExists(getPath(dirPath));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 确保目录存在, 如不存在则创建
     *
     * @param file File
     * @throws IOException
     */
    public static void makefileDirExists(@NotNull final File file) throws IOException {
        makefileDirExists(file.toPath());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 确保目录存在, 如不存在则创建
     *
     * @param dirPath Path
     * @throws IOException
     */
    public static void makefileDirExists(@NotNull final Path dirPath) throws IOException {
        Files.createDirectories(dirPath);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 确保父目录及其父目录直到根目录都已经创建
     *
     */
    public static void makesureParentDirExists(@NotNull final File file) throws IOException {
        makefileDirExists(file.getParentFile());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除目录及所有子目录
     *
     * @param dir Path
     */
    public static void deleteDir(@NotNull final Path dir) {
        try {
            // 后序遍历 先删掉子目录中的文件/目录
            Files.walkFileTree(dir, deleteFileVisitor);
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>删除目录及所有子目录失败[{}]<<<<<<<<", dir, e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 path
     * @param filePath String
     * @return Path
     */
    private static Path getPath(@NotNull final String filePath) {
        return Paths.get(filePath);
    }

    // -------------------------------------------------------------------------------------------------

    private static FileVisitor<Path> deleteFileVisitor = new SimpleFileVisitor<Path>() {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            Files.delete(dir);
            return FileVisitResult.CONTINUE;
        }
    };

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FileUtils class

/* End of file FileUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/io/FileUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
