/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.constant.SecurityConst;
import wang.encoding.mroot.common.util.CharsetsUtils;
import wang.encoding.mroot.common.util.text.EncodeUtils;
import wang.encoding.mroot.common.util.ExceptionUtils;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;

/**
 * AES对称加密和解密
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class AesManageComponent {

    private static AesManageComponent aesManageComponent;
    /**
     * AES加密
     */
    private static final String AES = "AES";
    /**
     * 默认加密算法
     */
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";

    // -------------------------------------------------------------------------------------------------

    private final SecurityConst securityConst;

    @Autowired
    public AesManageComponent(SecurityConst securityConst) {
        this.securityConst = securityConst;
    }

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[AesManageComponent]<<<<<<<<");
        }
        aesManageComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加密
     * 加密方式： AES128(CBC/PKCS5Padding) + Hex + 私钥
     *
     * @param str String 待加密内容
     * @return String 加密后HEX编码的内容
     */
    public static String getEncrypt(@NotNull final String str) {
        // 需要编码  编解码方式有 Base64 HEX
        return EncodeUtils.encodeHex(AesManageComponent.encrypt(str));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加密
     * 加密方式： AES128(CBC/PKCS5Padding) + Hex + 私钥
     *
     * @param str String 待加密内容
     * @return byte[]  加密后的内容
     */
    private static byte[] encrypt(@NotNull final String str) {
        try {
            // IVParameterSpec
            IvParameterSpec ivParameterSpec = AesManageComponent.initIvParameterSpec();
            // 两个参数，第一个为私钥字节数组， 第二个为加密方式 AES或者DES
            SecretKeySpec secretKeySpec = AesManageComponent.initSecretKeySpec();
            // 实例化加密类，参数为加密方式，要写全
            // PKCS5Padding 比 PKCS7Padding 效率高 PKCS7Padding可支持 IOS 加解密
            Cipher cipher = AesManageComponent.initCipher();
            // 初始化 此方法可以采用三种方式，按加密算法要求来添加
            // （1）无第三个参数
            // （2）第三个参数为 SecureRandom random = new SecureRandom(); 中 random 对象随机数(AES不可采用这种方法)
            // （3）采用此代码中的 IVParameterSpec
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
            // 加密操作 返回加密后的字节数组
            // 加密后需要编码  编解码方式有 Base64 HEX
            return cipher.doFinal(str.getBytes(CharsetsUtils.UTF_8));
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>执行加密失败[{}]：{}<<<<<<<<", str, e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 解密
     *
     * @param encrypted String 待解密Hex编码内容
     * @return String 解密后的内容
     */
    public static String decrypt(@NotNull final String encrypted) {
        try {
            byte[] byteMi = EncodeUtils.decodeHex(encrypted);
            IvParameterSpec ivParameterSpec = AesManageComponent.initIvParameterSpec();
            SecretKeySpec secretKeySpec = AesManageComponent.initSecretKeySpec();
            Cipher cipher = AesManageComponent.initCipher();
            // 与加密时不同 MODE Cipher.DECRYPT_MODE
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] decryptedData = cipher.doFinal(byteMi);
            return new String(decryptedData, CharsetsUtils.UTF_8);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>执行解密失败[{}]：{}<<<<<<<<", encrypted, e.getMessage());
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 IvParameterSpec
     *
     * @return IvParameterSpec
     */
    private static IvParameterSpec initIvParameterSpec() {
        return new IvParameterSpec(aesManageComponent.securityConst.getAesInit().getBytes(CharsetsUtils.UTF_8));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 SecretKeySpec
     *
     * @return SecretKeySpec
     */
    private static SecretKeySpec initSecretKeySpec() {
        return new SecretKeySpec(aesManageComponent.securityConst.getAesKey().getBytes(CharsetsUtils.UTF_8), AES);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 Cipher
     *
     * @return Cipher
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     */
    private static Cipher initCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
        return Cipher.getInstance(CIPHER_ALGORITHM);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AesManageComponent class

/* End of file AesManageComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/AesManageComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
