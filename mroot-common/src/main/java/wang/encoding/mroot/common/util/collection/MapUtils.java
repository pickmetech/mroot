/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.collection;

import com.google.common.base.Preconditions;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import org.apache.commons.lang3.Validate;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * 关于Map的工具集合
 * 1. 常用函数(如是否为空, 两个map的Diff对比，针对value值的排序)
 * 2. 对于并发Map，增加putIfAbsent(返回最终值版), createIfAbsent这两个重要函数(from Common Lang3)
 * 3. 便捷的构造函数(via guava,Java Collections，并增加了用数组，List等方式初始化Map的函数)
 * 4. JDK Collections的empty,singleton
 *
 * @author ErYang
 */
public class MapUtils {


    public static final float DEFAULT_LOAD_FACTOR = 0.75f;


    /**
     * 判断是否为空
     * @param map Map<?, ?>
     * @return boolean
     */
    public static boolean isEmpty(@Nullable final Map<?, ?> map) {
        return (null == map) || map.isEmpty();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否不为空
     * @param map Map<?, ?>
     * @return boolean
     */
    public static boolean isNotEmpty(@Nullable final Map<?, ?> map) {
        return (null != map) && !map.isEmpty();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 这个方法以要添加到ConcurrentMap中的键的值为参数，就像普通的put()方法，
     * 但是只有在map不包含这个键时，才能将键加入到map中。如果map已经包含这个键，那么这个键的现有值就会保留。
     * ConcurrentMap的putIfAbsent()返回之前的Value，此函数封装返回最终存储在Map中的Value
     *
     * org.apache.commons.lang3.concurrent.ConcurrentUtils#putIfAbsent(ConcurrentMap, Object, Object)
     *
     * @param map ConcurrentMap<K, V>
     * @param key K
     * @param value K
     * @param <K> K
     * @param <V> V
     * @return <K, V>
     */
    public static <K, V> V putIfAbsentReturnLast(@NotNull final ConcurrentMap<K, V> map, @NotNull final K key,
            @NotNull final V value) {
        final V result = map.putIfAbsent(key, value);
        return null != result ? result : value;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 如果Key不存在则创建，返回最后存储在Map中的Value.
     * 如果创建Value对象有一定成本, 直接使用PutIfAbsent可能重复浪费，则使用此类，传入一个被回调的ValueCreator，Lazy创建对象。
     * org.apache.commons.lang3.concurrent.ConcurrentUtils#createIfAbsent(ConcurrentMap, Object,
     * org.apache.commons.lang3.concurrent.ConcurrentInitializer)
     *
     * @param map ConcurrentMap<K, V>
     * @param key K
     * @param creator ValueCreator<? extends V>
     * @param <K> K
     * @param <V> V
     * @return <K, V>
     */
    public static <K, V> V createIfAbsentReturnLast(@NotNull final ConcurrentMap<K, V> map, @NotNull final K key,
            @NotNull final ValueCreator<? extends V> creator) {
        final V value = map.get(key);
        if (null == value) {
            return putIfAbsentReturnLast(map, key, creator.get());
        }
        return value;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Lazy创建Value值的回调类
     * MapUtil#createIfAbsentReturnLast(ConcurrentMap, Object, ValueCreator)
     *
     */
    public interface ValueCreator<T> {

        /**
         * 得到
         * @return T
         */
        T get();

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型, 构造类型正确的HashMap.
     * 注意HashMap中有0.75的加载因子的影响, 需要进行运算后才能正确初始化HashMap的大小.
     * 加载因子也是HashMap中减少Hash冲突的重要一环，如果读写频繁，总记录数不多的Map，可以比默认值0.75进一步降低，建议0.5
     * com.google.common.collect.Maps#newHashMap(int)
     * @param expectedSize int
     * @param loadFactor loadFactor
     * @param <K> K
     * @param <V> V
     * @return <K, V>
     */
    public static <K, V> HashMap<K, V> newHashMapWithCapacity(int expectedSize, float loadFactor) {
        int finalSize = (int) (expectedSize / loadFactor + 1.0F);
        return new HashMap<>(finalSize, loadFactor);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型, 构造类型正确的HashMap
     * 同时初始化第一个元素
     *
     * @param key K
     * @param value V
     * @param <K> K
     * @param <V> V
     * @return <K, V>
     */
    public static <K, V> HashMap<K, V> newHashMap(@NotNull final K key, @NotNull final V value) {
        HashMap<K, V> map = new HashMap<>(16);
        map.put(key, value);
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型, 构造类型正确的HashMap
     * 同时初始化元素
     *
     * @param keys K[]
     * @param values  V[]
     * @param <K> K
     * @param <V> V
     * @return <K, V>
     */
    public static <K, V> HashMap<K, V> newHashMap(@NotNull final K[] keys, @NotNull final V[] values) {
        Validate.isTrue(keys.length == values.length, ">>>>>>>>keys的长度是 %d ,values的长度是 %d<<<<<<<<", keys.length,
                values.length);

        HashMap<K, V> map = new HashMap<>(keys.length * 2);

        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }

        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型, 构造类型正确的HashMap
     * 同时初始化元素
     *
     * @param keys List<K>
     * @param values List<V>
     * @param <K> K
     * @param <V> V
     * @return HashMap
     */
    public static <K, V> HashMap<K, V> newHashMap(@NotNull final List<K> keys, @NotNull final List<V> values) {
        Validate.isTrue(keys.size() == values.size(), ">>>>>>>>keys的长度是 %s ,values的长度是 %s<<<<<<<<", keys.size(),
                values.size());

        HashMap<K, V> map = new HashMap<>(keys.size() * 2);
        Iterator<K> keyIt = keys.iterator();
        Iterator<V> valueIt = values.iterator();

        while (keyIt.hasNext()) {
            map.put(keyIt.next(), valueIt.next());
        }

        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的TreeMap
     * com.google.common.collect.Maps#newTreeMap()
     *
     * @param <K> K
     * @param <V> V
     * @return TreeMap
     */
    public static <K extends Comparable, V> TreeMap<K, V> newSortedMap() {
        return new TreeMap<>();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的TreeMap
     *  com.google.common.collect.Maps#newTreeMap(Comparator)
     *
     * @param comparator Comparator<C>
     * @param <C> Comparator
     * @param <K> K
     * @param <V> V
     * @return TreeMap
     */
    public static <C, K extends C, V> TreeMap<K, V> newSortedMap(@Nullable Comparator<C> comparator) {
        return Maps.newTreeMap(comparator);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 相比HashMap，当key是枚举类时, 性能与空间占用俱佳.
     *
     * @param type Class<K>
     * @param <K> K
     * @param <V> V
     * @return EnumMap
     */
    public static <K extends Enum<K>, V> EnumMap<K, V> newEnumMap(@NotNull Class<K> type) {
        return new EnumMap<>(Preconditions.checkNotNull(type));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的ConcurrentHashMap.
     *
     * @param <K> K
     * @param <V> V
     * @return ConcurrentHashMap
     */
    public static <K, V> ConcurrentHashMap<K, V> newConcurrentHashMap() {
        return new ConcurrentHashMap<>(16);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的ConcurrentSkipListMap.
     *
     * @param <K> K
     * @param <V> V
     * @return ConcurrentSkipListMap
     */
    public static <K, V> ConcurrentSkipListMap<K, V> newConcurrentSortedMap() {
        return new ConcurrentSkipListMap<>();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回一个空的结构特殊的Map，节约空间.
     * 注意返回的Map不可写, 写入会抛出UnsupportedOperationException.
     *
     * JDK Collections的常用构造函数
     * Collections#emptyMap()
     */
    public static <K, V> Map<K, V> emptyMap() {
        return Collections.emptyMap();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 如果map为null，转化为一个安全的空Map
     * 注意返回的Map不可写, 写入会抛出UnsupportedOperationException.
     *
     * JDK Collections的常用构造函数
     * Collections#emptyMap()
     *
     * @param map Map
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V> Map<K, V> emptyMapIfNull(final Map<K, V> map) {
        return null == map ? (Map<K, V>) Collections.EMPTY_MAP : map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回一个只含一个元素但结构特殊的Map，节约空间.
     * 注意返回的Map不可写, 写入会抛出UnsupportedOperationException.
     *
     * Collections#singletonMap(Object, Object)
     * JDK Collections的常用构造函数
     *
     * @param key K
     * @param value V
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V> Map<K, V> singletonMap(final K key, final V value) {
        return Collections.singletonMap(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回包装后不可修改的Map.
     * 如果尝试修改，会抛出UnsupportedOperationException
     *
     * Collections#unmodifiableMap(Map)
     * JDK Collections的常用构造函数
     *
     * @param m Map
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V> Map<K, V> unmodifiableMap(final Map<? extends K, ? extends V> m) {
        return Collections.unmodifiableMap(m);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回包装后不可修改的有序Map.
     *
     * Collections#unmodifiableSortedMap(SortedMap)
     * JDK Collections的常用构造函数
     *
     * @param m SortedMap
     * @param <K> K
     * @param <V> V
     * @return SortedMap
     */
    public static <K, V> SortedMap<K, V> unmodifiableSortedMap(final SortedMap<K, ? extends V> m) {
        return Collections.unmodifiableSortedMap(m);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对两个Map进行比较，返回MapDifference，然后各种妙用.
     * 包括key的差集，key的交集，以及key相同但value不同的元素
     *
     * com.google.common.collect.MapDifference
     *
     * @param left Map
     * @param right Map
     * @param <K> K
     * @param <V> V
     * @return MapDifference
     */
    public static <K, V> MapDifference<K, V> difference(Map<? extends K, ? extends V> left,
            Map<? extends K, ? extends V> right) {
        return Maps.difference(left, right);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对一个Map按Value进行排序，返回排序LinkedHashMap，多用于Value是Counter的情况
     *
     * @param map Map
     * @param reverse 按Value的倒序 or 正序排列
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V extends Comparable> Map<K, V> sortByValue(Map<K, V> map, final boolean reverse) {
        return sortByValueInternal(map, reverse ?
                Ordering.from(new ComparableEntryValueComparator<K, V>()).reverse() :
                new ComparableEntryValueComparator<>());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对一个Map按Value进行排序，返回排序LinkedHashMap.
     *
     * @param map Map
     * @param comparator Comparator
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V> Map<K, V> sortByValue(Map<K, V> map, final Comparator<? super V> comparator) {
        return sortByValueInternal(map, new EntryValueComparator<>(comparator));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对一个Map按Value进行排序，返回排序LinkedHashMap.
     *
     * @param map Map
     * @param comparator Comparator
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    private static <K, V> Map<K, V> sortByValueInternal(Map<K, V> map, Comparator<Entry<K, V>> comparator) {
        Set<Entry<K, V>> entrySet = map.entrySet();
        Entry<K, V>[] entryArray = entrySet.toArray(new Entry[0]);

        Arrays.sort(entryArray, comparator);

        Map<K, V> result = new LinkedHashMap<>();
        for (Entry<K, V> entry : entryArray) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对一个Map按Value进行排序，返回排序LinkedHashMap，最多只返回n条，多用于Value是Counter的情况.
     *
     * @param map Map
     * @param reverse 按Value的倒序 or 正序排列
     * @param n int
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V extends Comparable> Map<K, V> topNByValue(Map<K, V> map, final boolean reverse, int n) {
        return topNByValueInternal(map, n, reverse ?
                Ordering.from(new ComparableEntryValueComparator<K, V>()).reverse() :
                new ComparableEntryValueComparator<>());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对一个Map按Value进行排序，返回排序LinkedHashMap, 最多只返回n条，多用于Value是Counter的情况.
     *
     * @param map Map
     * @param comparator Comparator
     * @param n int
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    public static <K, V> Map<K, V> topNByValue(Map<K, V> map, final Comparator<? super V> comparator, int n) {
        return topNByValueInternal(map, n, new EntryValueComparator<>(comparator));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对一个Map按Value进行排序，返回排序LinkedHashMap, 最多只返回n条，多用于Value是Counter的情况.
     *
     * @param map Map
     * @param comparator Comparator
     * @param n int
     * @param <K> K
     * @param <V> V
     * @return Map
     */
    private static <K, V> Map<K, V> topNByValueInternal(Map<K, V> map, int n, Comparator<Entry<K, V>> comparator) {
        Set<Entry<K, V>> entrySet = map.entrySet();
        Entry<K, V>[] entryArray = entrySet.toArray(new Entry[0]);
        Arrays.sort(entryArray, comparator);

        Map<K, V> result = new LinkedHashMap<>(16);
        int size = Math.min(n, entryArray.length);
        for (int i = 0; i < size; i++) {
            Entry<K, V> entry = entryArray[i];
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    private static final class ComparableEntryValueComparator<K, V extends Comparable>
            implements Comparator<Entry<K, V>> {
        @Override
        public int compare(Entry<K, V> o1, Entry<K, V> o2) {
            return (o1.getValue()).compareTo(o2.getValue());
        }
    }

    // -------------------------------------------------------------------------------------------------

    private static final class EntryValueComparator<K, V> implements Comparator<Entry<K, V>> {
        private final Comparator<? super V> comparator;

        private EntryValueComparator(Comparator<? super V> comparator2) {
            this.comparator = comparator2;
        }

        @Override
        public int compare(Entry<K, V> o1, Entry<K, V> o2) {
            return comparator.compare(o1.getValue(), o2.getValue());
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MapUtils class

/* End of file MapUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/collection/MapUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
