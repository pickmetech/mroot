/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * 实现深度的 BeanOfClassA <-> BeanOfClassB 复制
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class BeanMapperComponent {


    private final Mapper dozerMapper;

    @Autowired
    public BeanMapperComponent(Mapper dozerMapper) {
        this.dozerMapper = dozerMapper;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * beanMapperComponent
     */
    private static BeanMapperComponent beanMapperComponent;

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[BeanMapperComponent]<<<<<<<<");
        }
        beanMapperComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 简单的复制出新类型对象
     *
     * @param source           S
     * @param destinationClass Class
     * @return D
     */
    public static <S, D> D map(@NotNull final S source, @NotNull final Class<D> destinationClass) {
        return beanMapperComponent.dozerMapper.map(source, destinationClass);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 简单的复制出新对象 ArrayList
     *
     * @param sourceList       Iterable
     * @param destinationClass Class
     * @return List
     */
    public static <S, D> List<D> mapList(@NotNull final Iterable<S> sourceList,
            @NotNull final Class<D> destinationClass) {
        List<D> destinationClassList = new ArrayList<>();
        for (S source : sourceList) {
            if (null != source) {
                destinationClassList.add(beanMapperComponent.dozerMapper.map(source, destinationClass));
            }
        }
        return destinationClassList;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 简单的复制出新对象 Set
     *
     * @param sourceList       Iterable
     * @param destinationClass Class
     * @return Set
     */
    public static <S, D> Set<D> mapSet(@NotNull final Iterable<S> sourceList,
            @NotNull final Class<D> destinationClass) {
        Set<D> destinationClassSet = new HashSet<>();
        for (S source : sourceList) {
            if (null != source) {
                destinationClassSet.add(beanMapperComponent.dozerMapper.map(source, destinationClass));
            }
        }
        return destinationClassSet;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 简单的复制出新对象 TreeSet
     *
     * @param sourceList       Iterable
     * @param destinationClass Class
     * @return TreeSet
     */
    public static <S, D> TreeSet<D> mapTreeSet(@NotNull final Iterable<S> sourceList,
            @NotNull final Class<D> destinationClass) {
        TreeSet<D> destinationClassSet = new TreeSet<>();
        for (S source : sourceList) {
            if (null != source) {
                destinationClassSet.add(beanMapperComponent.dozerMapper.map(source, destinationClass));
            }
        }
        return destinationClassSet;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BeanMapperComponent class

/* End of file BeanMapperComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/BeanMapperComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
