/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.time;


import org.apache.commons.lang3.Validate;
import wang.encoding.mroot.common.annotation.NotNull;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * 日期工具类
 * 在不方便使用joda-time时，使用本类降低Date处理的复杂度与性能消耗
 * 封装Common Lang3及移植Jodd的最常用日期方法
 *
 * @author ErYang
 */
public class DateUtils {

    /**
     * 1秒毫秒数
     */
    public static final long MILLIS_PER_SECOND = 1000;
    /**
     *1分钟毫秒数
     */
    public static final long MILLIS_PER_MINUTE = 60 * MILLIS_PER_SECOND;
    /**
     *1小时毫秒数
     */
    public static final long MILLIS_PER_HOUR = 60 * MILLIS_PER_MINUTE;
    /**
     *1天毫秒数
     */
    public static final long MILLIS_PER_DAY = 24 * MILLIS_PER_HOUR;

    private static final int[] MONTH_LENGTH = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    // -------------------------------------------------------------------------------------------------

    /**
     * 是否同一天
     * org.apache.commons.lang3.time.DateUtils#isSameDay(Date, Date)
     *
     * @param date1 Date
     * @param date2 Date
     * @return boolean
     */
    public static boolean isSameDay(@NotNull final Date date1, @NotNull final Date date2) {
        return org.apache.commons.lang3.time.DateUtils.isSameDay(date1, date2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 是否同一时刻
     *
     * @param date1 Date
     * @param date2 Date
     * @return boolean
     */
    public static boolean isSameTime(@NotNull final Date date1, @NotNull final Date date2) {
        // date.getMillisOf() 比date.getTime()快
        return 0 == date1.compareTo(date2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断日期是否在范围内，包含相等的日期
     *
     * @param date Date
     * @param start Date
     * @param end Date
     * @return boolean
     */
    public static boolean isBetween(@NotNull final Date date, @NotNull final Date start, @NotNull final Date end) {
        if (null == date || null == start || null == end || start.after(end)) {
            throw new IllegalArgumentException(">>>>>>>>日期不能为空,结束日期要在开始日期后面<<<<<<<<");
        }
        return !date.before(start) && !date.after(end);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加月
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date addMonths(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addMonths(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 减月
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date subMonths(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addMonths(date, -amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加周
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date addWeeks(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addWeeks(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 减周
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date subWeeks(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addWeeks(date, -amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加天
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date addDays(@NotNull final Date date, final int amount) {
        return org.apache.commons.lang3.time.DateUtils.addDays(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 减天
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date subDays(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addDays(date, -amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加小时
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date addHours(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addHours(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 减小时
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date subHours(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addHours(date, -amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 加分钟
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date addMinutes(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addMinutes(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 减分钟
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date subMinutes(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addMinutes(date, -amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 终于到了，续秒
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date addSeconds(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addSeconds(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 减秒
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date subSeconds(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.addSeconds(date, -amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置年份, 公元纪年
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setYears(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.setYears(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置月份, 1-12
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setMonths(@NotNull final Date date, int amount) {
        int end = 12;
        if (1 > amount || end < amount) {
            throw new IllegalArgumentException(">>>>>>>>月份只能是1到12之间<<<<<<<<");
        }
        return org.apache.commons.lang3.time.DateUtils.setMonths(date, amount - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置日期, 1-31
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setDays(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.setDays(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置小时, 0-23
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setHours(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.setHours(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置分钟, 0-59
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setMinutes(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.setMinutes(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置秒, 0-59
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setSeconds(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.setSeconds(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置毫秒
     *
     * @param date Date
     * @param amount int
     * @return Date
     */
    public static Date setMilliseconds(@NotNull final Date date, int amount) {
        return org.apache.commons.lang3.time.DateUtils.setMilliseconds(date, amount);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得日期是一周的第几天. 已改为中国习惯，1 是Monday，而不是Sundays
     *
     * @param date Date
     * @return int
     */
    public static int getDayOfWeek(@NotNull final Date date) {
        int result = getWithMondayFirst(date, Calendar.DAY_OF_WEEK);
        return 1 == result ? 7 : result - 1;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得日期是一年的第几天，返回值从1开始
     *
     * @param date Date
     * @return int
     */
    public static int getDayOfYear(@NotNull final Date date) {
        return get(date, Calendar.DAY_OF_YEAR);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得日期是一月的第几周，返回值从1开始
     * 开始的一周，只要有一天在那个月里都算. 已改为中国习惯，1 是Monday，而不是Sunday
     *
     * @param date Date
     * @return int
     */
    public static int getWeekOfMonth(@NotNull final Date date) {
        return getWithMondayFirst(date, Calendar.WEEK_OF_MONTH);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得日期是一年的第几周，返回值从1开始.
     * 开始的一周，只要有一天在那一年里都算.已改为中国习惯，1 是Monday，而不是Sunday
     *
     * @param date Date
     * @return int
     */
    public static int getWeekOfYear(@NotNull final Date date) {
        return getWithMondayFirst(date, Calendar.WEEK_OF_YEAR);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到日期
     * @param date Date
     * @param field int
     * @return int
     */
    private static int get(final Date date, int field) {
        Validate.notNull(date, ">>>>>>>>日期不能为空<<<<<<<<");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return cal.get(field);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到日期
     *
     * @param date Date
     * @param field int
     * @return int
     */
    private static int getWithMondayFirst(final Date date, int field) {
        Validate.notNull(date, ">>>>>>>>日期不能为空<<<<<<<<");
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        return cal.get(field);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-1-1 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date beginOfYear(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.YEAR);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-12-31 23:59:59.999
     *
     * @param date Date
     * @return Date
     */
    public static Date endOfYear(@NotNull final Date date) {
        return new Date(nextYear(date).getTime() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2017-1-1 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date nextYear(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.YEAR);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-11-1 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date beginOfMonth(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.MONTH);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-11-30 23:59:59.999
     *
     * @param date Date
     * @return Date
     */
    public static Date endOfMonth(@NotNull final Date date) {
        return new Date(nextMonth(date).getTime() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-12-1 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date nextMonth(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.MONTH);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2017-1-20 07:33:23, 则返回2017-1-16 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date beginOfWeek(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils
                .truncate(DateUtils.subDays(date, DateUtils.getDayOfWeek(date) - 1), Calendar.DATE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2017-1-20 07:33:23, 则返回2017-1-22 23:59:59.999
     *
     * @param date Date
     * @return Date
     */
    public static Date endOfWeek(@NotNull final Date date) {
        return new Date(nextWeek(date).getTime() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2017-1-23 07:33:23, 则返回2017-1-22 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date nextWeek(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils
                .truncate(DateUtils.addDays(date, 8 - DateUtils.getDayOfWeek(date)), Calendar.DATE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-11-10 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date beginOfDate(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.DATE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2017-1-23 07:33:23, 则返回2017-1-23 23:59:59.999
     *
     * @param date Date
     * @return Date
     */
    public static Date endOfDate(@NotNull final Date date) {
        return new Date(nextDate(date).getTime() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-11-10 07:33:23, 则返回2016-11-11 00:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date nextDate(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.DATE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-12-10 07:33:23, 则返回2016-12-10 07:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date beginOfHour(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.HOUR_OF_DAY);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2017-1-23 07:33:23, 则返回2017-1-23 07:59:59.999
     *
     * @param date Date
     * @return Date
     */
    public static Date endOfHour(@NotNull final Date date) {
        return new Date(nextHour(date).getTime() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-12-10 07:33:23, 则返回2016-12-10 08:00:00
     *
     * @param date Date
     * @return Date
     */
    public static Date nextHour(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.HOUR_OF_DAY);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-12-10 07:33:23, 则返回2016-12-10 07:33:00
     *
     * @param date Date
     * @return Date
     */
    public static Date beginOfMinute(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.MINUTE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2017-1-23 07:33:23, 则返回2017-1-23 07:33:59.999
     *
     * @param date Date
     * @return Date
     */
    public static Date endOfMinute(@NotNull final Date date) {
        return new Date(nextMinute(date).getTime() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 2016-12-10 07:33:23, 则返回2016-12-10 07:34:00
     *
     * @param date Date
     * @return Date
     */
    public static Date nextMinute(@NotNull final Date date) {
        return org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.MINUTE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 是否闰年
     *
     * @param date Date
     * @return boolean
     */
    public static boolean isLeapYear(@NotNull final Date date) {
        return isLeapYear(get(date, Calendar.YEAR));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 是否闰年，copy from Jodd Core的TimeUtil
     * 参数是公元计数, 如2016
     *
     * @param y int
     * @return boolean
     */
    public static boolean isLeapYear(int y) {
        boolean result = false;
        // 必须能被4整除
        int divisible4 = y % 4;
        // 在改革之前
        boolean reform = y < 1582;
        // 不是一个世纪
        int century = y % 100;
        // 或者是400的倍数
        int multiple400 = y % 400;

        if (0 == divisible4) {
            if (reform || (0 != century) || (0 == multiple400)) {
                // 闰年
                result = true;
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取某个月有多少天, 考虑闰年等因数, 移植Jodd Core的TimeUtil
     *
     * @param date Date
     * @return int
     */
    public static int getMonthLength(@NotNull final Date date) {
        int year = get(date, Calendar.YEAR);
        int month = get(date, Calendar.MONTH);
        return getMonthLength(year, month);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取某个月有多少天, 考虑闰年等因数, 移植Jodd Core的TimeUtil
     *
     * @param year year
     * @param month int
     * @return int
     */
    public static int getMonthLength(int year, int month) {
        int end = 12;
        if ((1 > month) || (end < month)) {
            throw new IllegalArgumentException(">>>>>>>>无效的月份:" + month + "<<<<<<<<");
        }
        int month2 = 2;
        if (month2 == month) {
            return isLeapYear(year) ? 29 : 28;
        }

        return MONTH_LENGTH[month];
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DateUtils class

/* End of file DateUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/time/DateUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
