/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.collection;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 关于List的工具集合
 * 1. 常用函数(如是否为空，sort/binarySearch/shuffle/reverse(JDK Collection)
 * 2. 各种构造函数(guava and JDK Collection)
 * 3. 各种扩展List类型的创建函数
 * 4. 集合运算：交集，并集, 差集, 补集， Commons Collection，但对其不合理的地方做了修正
 *
 * @author ErYang
 *
 */
public class ListUtils {


    /**
     * 判断是否为空
     *
     * @param list List
     * @return boolean
     */
    public static boolean isEmpty(@Nullable final List<?> list) {
        return (null == list) || list.isEmpty();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否不为空
     *
     * @param list List
     * @return boolean
     */
    public static boolean isNotEmpty(@Nullable final List<?> list) {
        return (null != list) && !(list.isEmpty());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取第一个元素, 如果List为空返回 null
     *
     * @param list List
     * @param <T> T
     * @return T
     */
    public static <T> T getFirst(@Nullable final List<T> list) {
        if (isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 取最后一个元素，如果List为空返回null
     *
     * @param list List
     * @param <T> T
     * @return T
     */
    public static <T> T getLast(@Nullable final List<T> list) {
        if (isEmpty(list)) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的ArrayList
     *
     * Guava的构造函数
     * com.google.common.collect.Lists#newArrayList()
     *
     * @param size int
     * @param <T> T
     * @return ArrayList
     */
    public static <T> ArrayList<T> newArrayList(final int size) {
        return new ArrayList<>(size);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的ArrayList, 并初始化元素.
     *
     * Guava的构造函数
     * com.google.common.collect.Lists#newArrayList(Object...)
     *
     * @param elements T
     * @param <T> T
     * @return ArrayList
     */
    @SafeVarargs
    public static <T> ArrayList<T> newArrayList(@NotNull final T... elements) {
        return Lists.newArrayList(elements);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的ArrayList, 并初始化元素
     *
     * Guava的构造函数
     * see com.google.common.collect.Lists#newArrayList(Iterable)
     *
     * @param elements T
     * @param <T> T
     * @return ArrayList
     */
    public static <T> ArrayList<T> newArrayList(@NotNull final Iterable<T> elements) {
        return Lists.newArrayList(elements);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的ArrayList, 并初始化数组大小
     *
     * Guava的构造函数
     * com.google.common.collect.Lists#newArrayListWithCapacity(int)
     *
     * @param initSize int
     * @param <T> T
     * @return ArrayList
     */
    public static <T> ArrayList<T> newArrayListWithCapacity(final int initSize) {
        return new ArrayList<>(initSize);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的LinkedList
     *
     * Guava的构造函数
     * com.google.common.collect.Lists#newLinkedList()
     *
     * @param elements Iterable
     * @param <T> T
     * @return LinkedList
     */
    public static <T> LinkedList<T> newLinkedList(@NotNull final Iterable<? extends T> elements) {
        return Lists.newLinkedList(elements);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型转换的CopyOnWriteArrayList, 并初始化元素
     *
     * @param elements T
     * @param <T> T
     * @return CopyOnWriteArrayList
     */
    @SafeVarargs
    public static <T> CopyOnWriteArrayList<T> newCopyOnWriteArrayList(@NotNull final T... elements) {
        return new CopyOnWriteArrayList<>(elements);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回一个空的结构特殊的List，节约空间
     * 注意返回的List不可写, 写入会抛出UnsupportedOperationException.
     *
     * JDK Collections的常用构造函数
     * Collections#emptyList()
     *
     * @param <T> T
     * @return List
     */
    public static <T> List<T> emptyList() {
        return Collections.emptyList();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 如果list为null，转化为一个安全的空List.
     * 注意返回的List不可写, 写入会抛出UnsupportedOperationException.
     *
     * JDK Collections的常用构造函数
     * Collections#emptyList()
     *
     * @param list List
     * @param <T> T
     * @return List
     */
    public static <T> List<T> emptyListIfNull(@Nullable final List<T> list) {
        List<T> emptyList = new Gson().fromJson(new Gson().toJson(Collections.EMPTY_LIST), new TypeToken<List<T>>() {
        }.getType());
        return null == list ? emptyList : list;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回只含一个元素但结构特殊的List，节约空间.
     * 注意返回的List不可写, 写入会抛出UnsupportedOperationException.
     *
     * JDK Collections的常用构造函数
     * Collections#singletonList(Object)
     *
     * @param o T
     * @param <T> T
     * @return List
     */
    public static <T> List<T> singletonList(@NotNull final T o) {
        return Collections.singletonList(o);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回包装后不可修改的List
     * 如果尝试写入会抛出UnsupportedOperationException.
     *
     * JDK Collections的常用构造函数
     * Collections#unmodifiableList(List)
     *
     * @param list List
     * @param <T> T
     * @return List
     */
    public static <T> List<T> unmodifiableList(@NotNull final List<? extends T> list) {
        return Collections.unmodifiableList(list);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回包装后同步的List，所有方法都会被synchronized原语同步.
     *
     * JDK Collections的常用构造函数
     * 用于CopyOnWriteArrayList与 ArrayDequeue均不符合的场景
     *
     * @param list List
     * @param <T> T
     * @return List
     */
    public static <T> List<T> synchronizedList(@NotNull final List<T> list) {
        return Collections.synchronizedList(list);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 升序排序, 采用JDK认为最优的排序算法, 使用元素自身的compareTo()方法
     *
     * JDK Collections的常用构造函数
     * Collections#sort(List)
     *
     *
     * @param list List
     * @param <T> T
     */
    public static <T extends Comparable<? super T>> void sort(@NotNull final List<T> list) {
        Collections.sort(list);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 倒序排序, 采用JDK认为最优的排序算法,使用元素自身的compareTo()方法
     *
     * JDK Collections的常用构造函数
     * Collections#sort(List)
     *
     * @param list List
     * @param <T> T
     */
    public static <T extends Comparable<? super T>> void sortReverse(@NotNull final List<T> list) {
        list.sort(Collections.reverseOrder());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 升序排序, 采用JDK认为最优的排序算法, 使用 Comparator
     *
     * JDK Collections的常用构造函数
     * Collections#sort(List, Comparator)
     *
     * @param list List
     * @param c Comparator
     * @param <T> T
     */
    public static <T> void sort(@NotNull final List<T> list, @NotNull final Comparator<? super T> c) {
        list.sort(c);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 倒序排序, 采用JDK认为最优的排序算法, 使用Comparator
     *
     * JDK Collections的常用构造函数
     * Collections#sort(List, Comparator)
     *
     * @param list List
     * @param c Comparator
     * @param <T> T
     */
    public static <T> void sortReverse(@NotNull final List<T> list, @NotNull final Comparator<? super T> c) {
        list.sort(Collections.reverseOrder(c));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 二分法快速查找对象, 使用Comparable对象自身的比较.
     * list必须已按升序排序
     * 如果不存在，返回一个负数，代表如果要插入这个对象，应该插入的位置
     *
     * JDK Collections的常用构造函数
     * Collections#binarySearch(List, Object)
     *
     * @param sortedList List
     * @param key T
     * @param <T> T
     * @return int
     */
    public static <T> int binarySearch(@NotNull final List<? extends Comparable<? super T>> sortedList, T key) {
        return Collections.binarySearch(sortedList, key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 二分法快速查找对象，使用Comparator
     * list必须已按升序排序.
     * 如果不存在，返回一个负数，代表如果要插入这个对象，应该插入的位置
     *
     * JDK Collections的常用构造函数
     * Collections#binarySearch(List, Object, Comparator)
     *
     * @param sortedList List
     * @param key T
     * @param c Comparator
     * @param <T> T
     * @return int
     */
    public static <T> int binarySearch(@NotNull final List<? extends T> sortedList, @NotNull final T key,
            @NotNull final Comparator<? super T> c) {
        return Collections.binarySearch(sortedList, key, c);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 随机乱序，使用默认的Random
     *
     * JDK Collections的常用构造函数
     * Collections#shuffle(List)
     *
     * @param list List
     */
    public static void shuffle(@NotNull final List<?> list) {
        Collections.shuffle(list);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 随机乱序，使用传入的Random.
     *
     * JDK Collections的常用构造函数
     * Collections#shuffle(List, Random)
     *
     *
     * @param list List
     * @param rnd Random
     */
    public static void shuffle(@NotNull final List<?> list, @NotNull final Random rnd) {
        Collections.shuffle(list, rnd);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回一个倒转顺序访问的List，仅仅是一个倒序的View，不会实际多生成一个List
     *
     * com.google.common.collect.Lists#reverse(List)
     *
     * @param list List
     * @param <T>  T
     * @return List
     */
    public static <T> List<T> reverse(@NotNull final List<T> list) {
        return Lists.reverse(list);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * List 分页函数
     *
     * @param list List
     * @param size int
     * @param <T> T
     * @return List
     */
    public static <T> List<List<T>> partition(@NotNull final List<T> list, int size) {
        return Lists.partition(list, size);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清理掉 List中的 Null 对象
     *
     * @param list List
     * @param <T> List
     */
    public static <T> void notNullList(@Nullable final List<T> list) {
        if (isEmpty(list)) {
            return;
        }
        // 清理掉null的集合
        list.removeIf(Objects::isNull);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清理掉 List中的 重复对象
     * @param list List
     */
    public static <T> void uniqueNotNullList(@Nullable final List<T> list) {
        if (isEmpty(list)) {
            return;
        }
        Iterator<T> ite = list.iterator();
        Set<T> set = new HashSet<>((int) (list.size() / 0.75F + 1.0F));

        while (ite.hasNext()) {
            T obj = ite.next();
            // 清理掉null的集合
            if (null == obj) {
                ite.remove();
                continue;
            }
            // 清理掉重复的集合
            if (set.contains(obj)) {
                ite.remove();
                continue;
            }
            set.add(obj);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * list1,list2的并集（在list1或list2中的对象），产生新List
     * 对比Apache Common Collection4 ListUtils, 优化了初始大小
     *
     * @param list1 List
     * @param list2 List
     * @param <E> E
     * @return List
     */
    public static <E> List<E> union(@NotNull final List<? extends E> list1, @NotNull final List<? extends E> list2) {
        final List<E> result = new ArrayList<>(list1.size() + list2.size());
        result.addAll(list1);
        result.addAll(list2);
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * list1, list2的交集（同时在list1和list2的对象），产生新List
     * Apache Common Collection4 ListUtils，但其做了不合理的去重，因此重新改为性能较低但不去重的版本
     *
     * 与List.retainAll()相比，考虑了的List中相同元素出现的次数,
     * 如"a"在list1出现两次，而在list2中只出现一次，则交集里会保留一个"a"
     *
     *
     * @param list1 List
     * @param list2 List
     * @param <T> T
     * @return List
     */
    public static <T> List<T> intersection(@NotNull final List<? extends T> list1,
            @NotNull final List<? extends T> list2) {
        List<? extends T> smaller = list1;
        List<? extends T> larger = list2;
        if (list1.size() > list2.size()) {
            smaller = list2;
            larger = list1;
        }

        // 克隆一个可修改的副本
        List<T> newSmaller = new ArrayList<>(smaller);
        List<T> result = new ArrayList<>(smaller.size());
        for (final T e : larger) {
            if (newSmaller.contains(e)) {
                result.add(e);
                newSmaller.remove(e);
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * list1, list2的差集（在list1，不在list2中的对象），产生新List.
     * 与List.removeAll()相比，会计算元素出现的次数，
     * 如"a"在list1出现两次，而在list2中只出现一次，则差集里会保留一个"a".
     *
     * @param list1 List
     * @param list2 List
     * @param <T> T
     * @return List
     */
    public static <T> List<T> difference(@NotNull final List<? extends T> list1,
            @NotNull final List<? extends T> list2) {
        final List<T> result = new ArrayList<>(list1);

        for (T t : list2) {
            result.remove(t);
        }

        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * list1, list2的补集（在list1或list2中，但不在交集中的对象，又叫反交集）产生新List.
     * Apache Common Collection4 ListUtils，但其并集－交集时，初始大小没有对交集*2，所以做了修改
     *
     * @param list1 List
     * @param list2 List
     * @param <T> T
     * @return List
     */
    public static <T> List<T> disjoint(@NotNull final List<? extends T> list1, @NotNull final List<? extends T> list2) {
        List<T> intersection = intersection(list1, list2);
        List<T> towIntersection = union(intersection, intersection);
        return difference(union(list1, list2), towIntersection);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ListUtils class

/* End of file ListUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/collection/ListUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
