/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.enums;


/**
 * 文章分类类型枚举类
 *
 * @author ErYang
 */
public enum CategoryTypeEnum {

    /**
     * 主分类
     */
    FIRST(1, "主分类"),
    /**
     * 次级分类
     */
    SECOND(2, "次级分类"),
    /**
     * 分类
     */
    THIRD(3, "分类");

    /**
     * 得到key
     */
    private int key;
    /**
     * 得到值
     *
     */
    private String value;

    /**
     * 根据 key 返回枚举值 value
     *
     * @param key int
     * @return value String
     */
    public static String getValueByKey(final int key) {
        CategoryTypeEnum[] values = values();
        for (CategoryTypeEnum categoryTypeEnum : values) {
            if (categoryTypeEnum.getKey() == key) {
                return categoryTypeEnum.getValue();
            }
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    CategoryTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryTypeEnum enum

/* End of file CategoryTypeEnum.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/enums/CategoryTypeEnum.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
