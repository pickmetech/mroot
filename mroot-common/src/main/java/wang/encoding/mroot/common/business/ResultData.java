/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.business;


import com.alibaba.fastjson.JSONObject;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * ResultData 用于返回值封装，也用于服务端与客户端的 json 数据通信
 * 一、主要应用场景：
 * 1：业务层需要返回多个返回值，例如要返回业务状态以及数据
 *
 * @author ErYang
 */
public class ResultData extends HashMap<String, Object> {

    private static final long serialVersionUID = -8391185608337544674L;

    /**
     * 状态标志
     */
    private static final String STATE = "state";
    /**
     * 成功状态
     */
    private static final String STATE_OK = "ok";
    /**
     * 失败状态
     */
    private static final String STATE_FAIL = "fail";
    /**
     * 重复提交状态
     */
    private static final String STATE_REPEAT = "repeat";
    /**
     * 错误状态
     */
    private static final String STATE_ERROR = "error";
    /**
     * 信息
     */
    public static final String MESSAGE = "message";
    /**
     * 数据
     */
    private static final String DATA = "data";
    /**
     * 数据 code
     */
    private static final String DATA_CODE = "code";

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建 ResultData
     *
     * @return ResultData
     */
    public static ResultData create() {
        return new ResultData();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建 ResultData 并赋值
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    public static ResultData create(@NotNull final String key, @NotNull final Object value) {
        return new ResultData().set(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建成功状态的 ResultData
     *
     * @return ResultData
     */
    public static ResultData ok() {
        return new ResultData().setOk();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建成功状态的 ResultData 并赋值
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    public static ResultData ok(@NotNull final String key, @NotNull final Object value) {
        return ResultData.ok().set(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建失败状态的 ResultData
     *
     * @return ResultData
     */
    public static ResultData fail() {
        return new ResultData().setFail();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建失败状态的 ResultData 并赋值
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    public static ResultData fail(@NotNull final String key, @NotNull final Object value) {
        return ResultData.fail().set(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建重复提交状态的 ResultData
     *
     * @return ResultData
     */
    public static ResultData repeat() {
        return new ResultData().setRepeat();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建重复提交状态的 ResultData 并赋值
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    public static ResultData repeat(@NotNull final String key, @NotNull final Object value) {
        return ResultData.repeat().set(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建错误状态的 ResultData
     *
     * @return ResultData
     */
    public static ResultData error() {
        return new ResultData().setError();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建错误状态的 ResultData 并赋值
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    public static ResultData error(@NotNull final String key, @NotNull final Object value) {
        return ResultData.error().set(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否是成功状态
     *
     * @return Boolean
     */
    public boolean isOk() {
        Object state = this.get(STATE);
        if (STATE_OK.equals(state)) {
            return true;
        }

        if (STATE_FAIL.equals(state)) {
            return false;
        }
        return false;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否是失败状态
     *
     * @return Boolean
     */
    public boolean isFail() {
        Object state = this.get(STATE);
        if (STATE_FAIL.equals(state)) {
            return true;
        }
        if (STATE_OK.equals(state)) {
            return false;
        }
        //throw new IllegalStateException("调用 isFail() 之前，必须先调用 ok()、fail() 或者 setOk()、setFail() 方法");
        return false;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置成功状态的 ResultData
     *
     * @return ResultData
     */
    public ResultData setOk() {
        super.put(STATE, STATE_OK);
        super.put(MESSAGE, LocaleMessageSourceComponent.getMessage("message.result.state.succeed"));
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置失败状态的 ResultData
     *
     * @return ResultData
     */
    public ResultData setFail() {
        super.put(STATE, STATE_FAIL);
        super.put(MESSAGE, LocaleMessageSourceComponent.getMessage("message.result.state.fail"));
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置重复提交状态的 ResultData
     *
     * @return ResultData
     */
    private ResultData setRepeat() {
        super.put(STATE, STATE_REPEAT);
        super.put(MESSAGE, LocaleMessageSourceComponent.getMessage("message.result.state.repeat"));
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置错误状态的 ResultData
     *
     * @return ResultData
     */
    public ResultData setError() {
        super.put(STATE, STATE_ERROR);
        super.put(MESSAGE, LocaleMessageSourceComponent.getMessage("message.result.state.error"));
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 map
     *
     * @param map Map
     * @return ResultData
     */
    public ResultData setData(final Map<String, Object> map) {
        if (!map.containsKey(DATA_CODE)) {
            map.put(DATA_CODE, STATE_OK);
        }
        if (!map.containsKey(MESSAGE)) {
            map.put(MESSAGE, LocaleMessageSourceComponent.getMessage("message.get.info.succeed"));
        }
        this.set(DATA, map);
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 默认 map 只存在 code 和 message 的情况
     *
     * @param code String
     * @param message  String
     * @return ResultData
     */
    public ResultData setDefaultData(final String code, final String message) {
        Map<String, Object> map = new HashMap<>(16);
        map.put(DATA_CODE, code);
        map.put(MESSAGE, message);
        this.set(DATA, map);
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 key-value
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    public ResultData set(@NotNull final String key, @NotNull final Object value) {
        super.put(key, value);
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 data
     *
     * @param map map
     * @return ResultData
     */
    public ResultData set(@NotNull final Map<String, Object> map) {
        super.putAll(map);
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 ResultData
     *
     * @param resultData ResultData
     * @return ResultData
     */
    public ResultData set(@NotNull final ResultData resultData) {
        super.putAll(resultData);
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 key-value
     *
     * @param key key
     * @return ResultData
     */
    public ResultData delete(@NotNull final String key) {
        super.remove(key);
        return this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 转为 FastJson
     *
     * @return JSONObject
     */
    public JSONObject toFastJson() {
        return (JSONObject) JSONObject.toJSON(this);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ResultData class

/* End of file ResultData.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/business/ResultData.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
