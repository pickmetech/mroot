/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.model.FetchRet;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.storage.model.FileListing;
import com.qiniu.util.Auth;
import org.apache.commons.io.IOUtils;
import wang.encoding.mroot.common.annotation.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 七牛工具类
 *
 * @author ErYang
 */
public class QiNiuUtils {

    /**
     * AK
     */
    public static String ACCESS_KEY;
    /**
     * SK
     */
    public static String SECRET_KEY;
    /**
     * 七牛机房
     */
    public static Zone ZONE;
    /**
     * 七牛存储空间名称
     */
    public static String BUCKET;
    /**
     * 七牛存储空间地址
     */
    public static String BASE_URL;
    /**
     * Configuration
     */
    public static Configuration configuration;
    /**
     * UploadManager
     */
    public static UploadManager uploadManager;
    /**
     * 上传本地
     */
    public static Boolean uploadLocal;

    /**
     * 默认数目
     */
    private static final Integer LIMIT_SIZE = 1000;

    /**
     * 获取七牛帐号下的所有存储空间
     *
     * @return String[] 存储空间
     * @throws QiniuException
     */
    public static String[] listBucket() throws QiniuException {
        return QiNiuUtils.getBucketManager().buckets();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛图片上传
     *
     * @param inputStream 上传文件输入流
     * @param bucketName  存储空间名称
     * @param key         存储空间内文件的key(名称)
     * @param mimeType    文件的MIME类型，可选参数，不传入会自动判断
     * @return String
     * @throws IOException
     */
    public static String uploadFile(@NotNull final InputStream inputStream, @NotNull final String bucketName,
            @NotNull final String key, @NotNull final String mimeType) throws IOException {
        // 获取 token
        String token = QiNiuUtils.getUploadToken(bucketName);
        byte[] byteData = IOUtils.toByteArray(inputStream);
        Response response = uploadManager.put(byteData, key, token, null, mimeType, false);
        inputStream.close();
        return response.bodyString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛图片上传
     *
     * @param inputStream 上传文件输入流
     * @param bucketName  存储空间名称
     * @param key         存储空间内文件的key(名称)
     * @return String
     * @throws IOException
     */
    public static String uploadFile(@NotNull final InputStream inputStream, @NotNull final String bucketName,
            @NotNull final String key) throws IOException {
        // 获取 token
        String token = QiNiuUtils.getUploadToken(bucketName);
        byte[] byteData = IOUtils.toByteArray(inputStream);
        Response response = uploadManager.put(byteData, key, token, null, null, false);
        inputStream.close();
        return response.bodyString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛图片上传
     *
     * @param filePath   上传文件的路径
     * @param fileName   上传文件的文件名
     * @param bucketName 存储空间名称
     * @param key        存储空间内文件的key(名称)
     * @return String
     * @throws IOException
     */
    public static String uploadFile(@NotNull final String filePath, @NotNull final String fileName,
            @NotNull final String bucketName, @NotNull final String key) throws IOException {
        // 获取 token
        String token = QiNiuUtils.getUploadToken(bucketName);
        InputStream is = new FileInputStream(new File(filePath + fileName));
        byte[] byteData = IOUtils.toByteArray(is);
        Response response = uploadManager.put(byteData, (key == null || "".equals(key)) ? fileName : key, token);
        is.close();
        return response.bodyString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛图片上传 若没有指定文件的key(名称) 则默认将fileName参数作为文件的key(名称)
     *
     * @param filePath   上传文件的路径
     * @param fileName   上传文件的文件名
     * @param bucketName 存储空间名称
     * @return String
     * @throws IOException
     */
    public static String uploadFile(@NotNull final String filePath, @NotNull final String fileName,
            @NotNull final String bucketName) throws IOException {
        // 获取 token
        String token = QiNiuUtils.getUploadToken(bucketName);
        InputStream is = new FileInputStream(new File(filePath + fileName));
        byte[] byteData = IOUtils.toByteArray(is);
        Response response = uploadManager.put(byteData, fileName, token);
        is.close();
        return response.bodyString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 提取网络资源并上传到七牛存储空间
     *
     * @param url        网络上一个资源文件的URL
     * @param bucketName 存储空间空间名称
     * @param key        存储空间内文件的key(名称)[唯一的]
     * @return String
     * @throws QiniuException
     */
    public static String fetchToBucket(@NotNull final String url, @NotNull final String bucketName,
            @NotNull final String key) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FetchRet petrel = bucketManager.fetch(url, bucketName, key);
        return petrel.key;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 提取网络资源并上传到七牛存储空间 不指定key(名称) 则默认使用 url 作为文件的key(名称)
     *
     * @param url        网络上一个资源文件的URL
     * @param bucketName 存储空间空间名称
     * @return String
     * @throws QiniuException
     */
    public static String fetchToBucket(@NotNull final String url, @NotNull final String bucketName)
            throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FetchRet petrel = bucketManager.fetch(url, bucketName);
        return petrel.key;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛存储空间内文件复制
     *
     * @param bucket       源存储空间名称
     * @param key          源存储空间里文件的key(名称)(唯一的)
     * @param targetBucket 目标存储空间
     * @param targetKey    目标存储空间里文件的key(名称)(唯一的)
     * @throws QiniuException
     */
    public static void copyFile(@NotNull final String bucket, @NotNull final String key,
            @NotNull final String targetBucket, @NotNull final String targetKey) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        bucketManager.copy(bucket, key, targetBucket, targetKey);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛存储空间内文件剪切
     *
     * @param bucket       源存储空间名称
     * @param key          源存储空间里文件的key(名称)(唯一的)
     * @param targetBucket 目标存储空间
     * @param targetKey    目标存储空间里文件的key(名称)(唯一的)
     * @throws QiniuException
     */
    public static void moveFile(@NotNull final String bucket, @NotNull final String key,
            @NotNull final String targetBucket, @NotNull final String targetKey) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        bucketManager.move(bucket, key, targetBucket, targetKey);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛存储空间内文件重命名
     *
     * @param bucket    源存储空间名称
     * @param key       存储空间里文件的key(名称)(唯一的)
     * @param targetKey 目标存储空间
     * @throws QiniuException
     */
    public static void renameFile(@NotNull final String bucket, @NotNull final String key,
            @NotNull final String targetKey) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        bucketManager.rename(bucket, key, targetKey);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛存储空间内文件删除
     *
     * @param bucket 存储空间名称
     * @param key    存储空间里文件的key(名称)(唯一的)
     * @throws QiniuException
     */
    public static void deleteFile(@NotNull final String bucket, @NotNull final String key) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        bucketManager.delete(bucket, key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取指定七牛存储空间下的指定数目文件信息
     *
     * @param bucketName 存储空间名称
     * @param prefix     文件key的前缀
     * @param limit      批量提取的最大数目
     * @return FileInfo[]
     * @throws QiniuException
     */
    public static FileInfo[] listFile(@NotNull final String bucketName, @NotNull final String prefix,
            @NotNull final Integer limit) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FileListing list = bucketManager.listFiles(bucketName, prefix, null, limit, null);
        if (null == list || null == list.items || 0 >= list.items.length) {
            return null;
        }
        return list.items;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取指定七牛存储空间下的文件信息
     *
     * @param bucketName 存储空间名称
     * @param prefix     文件key的前缀
     * @return FileInfo[]
     * @throws QiniuException
     */
    public static FileInfo[] listFile(@NotNull final String bucketName, @NotNull final String prefix)
            throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FileListing list = bucketManager.listFiles(bucketName, prefix, null, LIMIT_SIZE, null);
        if (null == list || null == list.items || 0 >= list.items.length) {
            return null;
        }
        return list.items;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取指定七牛存储空间下的文件信息
     *
     * @param bucketName 存储空间名称
     * @return FileInfo[]
     * @throws QiniuException
     */
    public static FileInfo[] findFiles(@NotNull final String bucketName) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FileListing list = bucketManager.listFiles(bucketName, null, null, LIMIT_SIZE, null);
        if (null == list || null == list.items || 0 >= list.items.length) {
            return null;
        }
        return list.items;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取指定七牛存储空间下的指定数目文件信息
     *
     * @param bucketName 存储空间名称
     * @param key        存储空间里文件的key(名称)(唯一的)
     * @param limit      批量提取的最大数目
     * @return FileInfo
     * @throws QiniuException
     */
    public static FileInfo getFile(@NotNull final String bucketName, @NotNull final String key,
            @NotNull final Integer limit) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FileListing list = bucketManager.listFiles(bucketName, key, null, limit, null);
        if (null == list || null == list.items || 0 >= list.items.length) {
            return null;
        }
        return (list.items)[0];
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取指定七牛存储空间下的指定数目文件信息
     *
     * @param bucketName 存储空间名称
     * @param key        存储空间里文件的key(名称)(唯一的)
     * @return FileInfo
     * @throws QiniuException
     */
    public static FileInfo getFile(@NotNull final String bucketName, @NotNull final String key) throws QiniuException {
        BucketManager bucketManager = QiNiuUtils.getBucketManager();
        FileListing list = bucketManager.listFiles(bucketName, key, null, LIMIT_SIZE, null);
        if (null == list || null == list.items || 0 >= list.items.length) {
            return null;
        }
        return (list.items)[0];
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取七牛存储空间内指定文件的访问URL
     *
     * @param key 存储空间里文件的key(名称)(唯一的)
     * @return String
     * @throws QiniuException
     */
    public static String getFileAccessUrl(@NotNull final String key) throws QiniuException {
        return BASE_URL + key;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取七牛存储空间返回结果中的 key 的地址
     *
     * @param response 解析上传成功的结果
     * @return String
     * @throws QiniuException
     */
    public static String getResponseKeyUrl(@NotNull final String response) throws QiniuException {
        // 解析上传成功的结果
        DefaultPutRet putRet = new Gson().fromJson(response, DefaultPutRet.class);
        return BASE_URL + putRet.key;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取七牛存储空间 私密空间 文件 地址
     *
     * @param publicUrl 公开 url 地址
     * @return String
     * @throws QiniuException
     */
    public static String getPrivateDownloadUrl(@NotNull final String publicUrl) throws QiniuException {
        // 1小时，可以自定义链接过期时间
        long expireInSeconds = 3600;
        return QiNiuUtils.getAuth().privateDownloadUrl(publicUrl, expireInSeconds);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 Auth
     *
     * @return Auth
     */
    private static Auth getAuth() {
        return Auth.create(ACCESS_KEY, SECRET_KEY);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 BucketManager
     *
     * @return BucketManager
     */
    private static BucketManager getBucketManager() {
        return new BucketManager(QiNiuUtils.getAuth(), configuration);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 上传 Token
     *
     * @param bucketName bucketName
     * @return BucketManager
     */
    private static String getUploadToken(@NotNull final String bucketName) {
        // 获取 token
        return QiNiuUtils.getAuth().uploadToken(bucketName);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QiNiuUtils class

/* End of file QiNiuUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/QiNiuUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
