package wang.encoding.mroot.common.util.concurrent.limiter;


import org.apache.commons.lang3.Validate;
import wang.encoding.mroot.common.annotation.NotNull;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 采样器
 *
 * 移植 Twitter Common, 优化使用ThreadLocalRandom
 * https://github.com/twitter/commons/blob/master/src/java/com/twitter/common/util/Sampler.java
 *
 * @author ErYang
 *
 */
public class SamplerUtils {

    private static final Double ALWAYS = 100d;
    private static final Double NEVER = (double) 0;

    private double threshold;

    protected SamplerUtils() {
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * @param selectPercent 采样率，在0-100 之间，可以有小数位
     */
    protected SamplerUtils(final double selectPercent) {
        Validate.isTrue((selectPercent >= 0) && (selectPercent <= 100), ">>>>>>>>无效的 selectPercent 值:<<<<<<<<" + selectPercent);
        this.threshold = selectPercent / 100;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 优化的创建函数，如果为0或100时，返回更直接的采样器
     *
     * @param selectPercent Double
     * @return SamplerUtils
     */
    public static SamplerUtils create(@NotNull final Double selectPercent) {
        if (selectPercent.equals(ALWAYS)) {
            return new AlwaysSamplerUtils();
        } else if (selectPercent.equals(NEVER)) {
            return new NeverSamplerUtils();
        } else {
            return new SamplerUtils(selectPercent);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断当前请求是否命中采样
     *
     * @return boolean
     */
    public boolean select() {
        return ThreadLocalRandom.current().nextDouble() < threshold;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 采样率为100时，总是返回true
     */
    protected static class AlwaysSamplerUtils extends SamplerUtils {
        @Override
        public boolean select() {
            return true;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 采样率为0时，总是返回false
     */
    protected static class NeverSamplerUtils extends SamplerUtils {
        @Override
        public boolean select() {
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SamplerUtils class

/* End of file SamplerUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/concurrent/limiter/SamplerUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
