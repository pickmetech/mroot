/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.constant.SecurityConst;
import wang.encoding.mroot.common.util.security.DigestUtils;

import javax.annotation.PostConstruct;

/**
 * 摘要加密工具类(非可逆加密)
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class DigestManageComponent {


    private static DigestManageComponent digestManageComponent;

    // -------------------------------------------------------------------------------------------------

    private final SecurityConst securityConst;

    @Autowired
    public DigestManageComponent(SecurityConst securityConst) {
        this.securityConst = securityConst;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[DigestManageComponent]<<<<<<<<");
        }
        digestManageComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

    /***
     * 利用 DigestUtil 的工具类实现 SHA-512 加密
     * @param str String 需要加密的字符串
     * @return String 加密后的报文
     */
    public static String getSha512(@NotNull final String str) {
        // 加密 带盐
        return DigestUtils.getSha512(str, digestManageComponent.securityConst.getDigestKey());
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DigestManageComponent class

/* End of file DigestManageComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/DigestManageComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
