/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.lionsoul.ip2region.DataBlock;
import org.lionsoul.ip2region.DbConfig;
import org.lionsoul.ip2region.DbSearcher;
import org.lionsoul.ip2region.Util;
import org.springframework.core.io.ClassPathResource;
import wang.encoding.mroot.common.annotation.NotNull;

import java.io.File;
import java.lang.reflect.Method;

/**
 * ip2region 工具类
 *
 * @author ErYang
 */
@Slf4j
public class Ip2RegionUtils {


    /**
     * 得到 ip 的地址信息
     * @param ip String
     * @return String
     */
    public static String getRegionInfo(@NotNull final String ip) {

        // 查询算法
        // B-tree
        //int algorithm = DbSearcher.BTREE_ALGORITHM;
        // Binary
        //DbSearcher.BINARY_ALGORITHM
        // Memory
        //DbSearcher.MEMORY_ALGORITYM
        try {
            // db
            ClassPathResource classPathResource = new ClassPathResource("ip2region.db");
            File tempFile = File.createTempFile("ip2region", ".db");
            FileUtils.copyInputStreamToFile(classPathResource.getInputStream(), tempFile);

            DbConfig config = new DbConfig();
            DbSearcher searcher = new DbSearcher(config, tempFile.getAbsolutePath());
            Method method;
            method = searcher.getClass().getMethod("btreeSearch", String.class);
            DataBlock dataBlock;
            if (!Util.isIpAddress(ip)) {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>IP[{}]地址错误<<<<<<<", ip);
                }
                return "";
            }
            dataBlock = (DataBlock) method.invoke(searcher, ip);
            return dataBlock.getRegion();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>无法加载ip2region.db文件<<<<<<<");
            }
            return "";
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Ip2RegionUtils class

/* End of file Ip2RegionUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/Ip2RegionUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
