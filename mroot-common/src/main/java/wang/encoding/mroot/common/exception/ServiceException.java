/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.common.exception;


import wang.encoding.mroot.common.annotation.NotNull;

/**
 * Service 异常
 *
 * @author ErYang
 */
public class ServiceException extends BaseException {

    private static final long serialVersionUID = 8991877692906303948L;

    /**
     * 异常信息
     */
    public ServiceException() {
        super();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 异常信息
     *
     * @param message String
     */
    public ServiceException(@NotNull final String message) {
        super(message);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 异常信息
     *
     * @param message String
     * @param cause   Throwable
     */
    public ServiceException(@NotNull final String message,
                            @NotNull final Throwable cause) {
        super(message, cause);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ServiceException class

/* End of file ServiceException.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/exception/ServiceException.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
