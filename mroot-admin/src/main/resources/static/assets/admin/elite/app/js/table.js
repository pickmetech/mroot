var EliteTable = function () {

    return {

        //主要模块初始化函数
        init: function () {

            /**
             * 搜索功能
             */
            $('#js_search').click(function () {
                var url = $(this).attr('data-url');
                var query = $('#js_search-form').find('input').serialize();
                query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g, '');
                query = query.replace(/^&/g, '');
                if (url.indexOf('?') > 0) {
                    url += '&' + query;
                } else {
                    url += '?' + query;
                }
                window.location.href = url;
            });

            // ------------------------------------------------------------------------

            // //回车自动提交
            // $('#js_search-form input').keypress(function (e) {
            //     if (e.keyCode === 13) {
            //         $('#js_search').click();
            //     }
            // });
            //
            // // ------------------------------------------------------------------------

            /**
             * 复选框选中
             * @type {*|jQuery.fn.init|jQuery|HTMLElement}
             */
            var table = $('#js_table');
            table.find('.js_table-check').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).prop("checked", true);
                        $(this).parents('tr').addClass("active");
                    } else {
                        $(this).prop("checked", false);
                        $(this).parents('tr').removeClass("active");
                    }
                });
            });

            table.on('change', 'tbody tr .js_table-checkboxes', function () {
                $(this).parents('tr').toggleClass("active");
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /**
         * pagination 分页
         * @param options
         */
        pagination: function (options) {
            $('#paginate').pagination({
                // 页面跳转的目标位置
                url: options.url,
                // 总条数
                totalRow: options.totalRow,
                // 每页显示条数
                pageSize: options.pageSize,
                // 当前页
                pageNumber: options.pageNumber,
                // 页面跳转时需要同时传递给服务端的自定义参数设置
                params: options.params
            });
        }

        // -------------------------------------------------------------------------------------------------

    };
}();
jQuery(document).ready(function () {
    EliteTable.init()
});
