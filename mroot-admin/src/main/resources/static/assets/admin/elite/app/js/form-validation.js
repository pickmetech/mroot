var EliteFormValidation = function () {

    /**
     * ajax 请求
     * @param form
     */
    var ajaxSubmit = function (form) {
        $('button:submit').attr('disabled', true);
        //表单提交
        var url = form.get(0).action;
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'JSON',
            data: form.serialize(),
            success: function (result) {
                $('button:submit').attr('disabled', false);
                $('#js_form-token').val(result.formToken);
                if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                    if (result.data && result.data.url) {
                        EliteTool.showSweetAlertSuccess({
                            title: result.message,
                            isSkip: true,
                            uri: result.data.url
                        });
                    } else {
                        EliteTool.showSweetAlertSuccess({
                            title: result.message,
                            isSkip: true
                        });
                    }
                } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                    if (AlertMessage.getMessageAlertAjaxValidation() === result.validation) {
                        showFormMsg(result.message);
                    } else {
                        EliteTool.showSweetAlertError({
                            title: result.message
                        });
                    }
                } else if (AlertMessage.getMessageAlertAjaxRepeat() === result.state) {
                    EliteTool.showSweetAlertWarning({
                        title: result.message
                    });
                } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                    EliteTool.showSweetAlertError({
                        title: result.message
                    });
                } else {
                    EliteTool.showSweetAlertError({
                        title: result.message
                    });
                }
            },
            error: function () {
                $('button:submit').attr('disabled', false);
                EliteTool.showSweetAlertError({
                    title: AlertMessage.getMessageAlertNetworkError()
                });
            }
        });

    };

    // -------------------------------------------------------------------------------------------------

    /**
     * 显示 form_msg
     */
    var showFormMsg = function (text) {
        var jsAlert = $('#js_form-msg');
        if (text) {
            $('#js_alert-text').html(text);
        }
        jsAlert.removeClass('hide').addClass("show");
    };

    // -------------------------------------------------------------------------------------------------

    /**
     * 隐藏 form_msg
     */
    var hideFormMsg = function () {
        var jsAlert = $('#js_form-msg');
        jsAlert.removeClass('show').addClass('hide');
    };

    // -------------------------------------------------------------------------------------------------

    return {

        init: function () {

        },

        /*代码生成表单验证*/
        formValidationGenerate: function () {

            $('#js_generate-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_generate-form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*用户表单验证*/
        formValidationUser: function () {

            $('#js_user-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    category: {
                        required: true
                    },
                    username: {
                        cnLetterNumUl5To18: true
                    },
                    nickName: {
                        cnLetterNumUl0To18: true
                    },
                    password: {
                        letterNum0To18: true
                    },
                    email: {
                        emailNullable: true
                    },
                    phone: {
                        phoneNum: true
                    },
                    realName: {
                        cn0To18: true
                    }
                },
                messages: {
                    category: {
                        required: UserValidation.getTypeRequired()
                    },
                    username: {
                        cnLetterNumUl5To18: UserValidation.getUsernameCnLetterNumUl5To18()
                    },
                    nickName: {
                        cnLetterNumUl0To18: UserValidation.getNickNameCnLetterNumUl0To18()
                    },
                    password: {
                        letterNum0To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    email: {
                        emailNullable: UserValidation.getEmail()
                    },
                    phone: {
                        phoneNum: UserValidation.getPhonePhoneNum()
                    },
                    realName: {
                        cn0To18: UserValidation.getRealNameCn0To18()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_user-form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*昵称表单验证*/
        formValidationNickname: function () {

            $('#js_nickName-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    nickName: {
                        cnLetterNumUl2To18: true
                    },
                    password: {
                        letterNum6To18: true
                    }
                },
                messages: {
                    nickName: {
                        cnLetterNumUl2To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    password: {
                        letterNum6To18: UserValidation.getPasswordLetterNum6To18()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_nickName-form');
                    ajaxSubmit(form);
                }
            });

            // ------------------------------------------------------------------------
        },

        // -------------------------------------------------------------------------------------------------

        /*密码表单验证*/
        formValidationPassword: function () {

            $('#js_password-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    oldPassword: {
                        letterNum6To18: true
                    },
                    password: {
                        letterNum6To18: true
                    },
                    confirmPassword: {
                        equalTo: '#js_password'
                    }
                },
                messages: {
                    oldPassword: {
                        letterNum6To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    password: {
                        letterNum6To18: UserValidation.getPasswordLetterNum6To18()
                    },
                    confirmPassword: {
                        equalTo: UserValidation.getConfirmPassword()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_password-form');
                    ajaxSubmit(form);
                }
            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*用户授权表单验证*/
        formValidationUserAuthorization: function () {

            $('#js_user-authorization-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {
                    hideFormMsg();

                    form = $('#js_user-authorization-form');
                    ajaxSubmit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*角色表单验证*/
        formValidationRole: function () {

            $('#js_role-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    sole: {
                        letterNumUl2To80: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    sole: {
                        letterNumUl2To80: RoleValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: RoleValidation.getTitlePattern()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: RoleValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_role-form');
                    ajaxSubmit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*角色授权表单验证*/
        formValidationRoleAuthorization: function () {

            $('#js_role-authorization-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    var ref = $('#js_rule-tree').jstree(true);
                    sel = ref.get_selected();

                    $('#js_rules').val(sel);

                    form = $('#js_role-authorization-form');
                    ajaxSubmit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*权限表单验证*/
        formValidationRule: function () {

            $('#js_rule-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    pid: {
                        zeroPositiveInteger: true
                    },
                    category: {
                        positiveInteger: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    url: {
                        letterNumUlBias1To255: true
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    pid: {
                        zeroPositiveInteger: RuleValidation.getPidRange()
                    },
                    category: {
                        positiveInteger: RuleValidation.getTypeRange()
                    },
                    title: {
                        letterNumUl2To80: RuleValidation.getTitlePattern()
                    },
                    url: {
                        letterNumUlBias1To255: RuleValidation.getUrlPattern()
                    },
                    sort: {
                        zeroPositiveInteger: RuleValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: RuleValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_rule-form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*系统配置表单验证*/
        formValidationConfig: function () {

            $('#js_config-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    category: {
                        positiveInteger: true
                    },
                    sole: {
                        letterNumUl2To80: true
                    },
                    content: {
                        cnLetterNumUlSymbol1To255: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    category: {
                        positiveInteger: ConfigValidation.getTypeRange()
                    },
                    sole: {
                        letterNumUl2To80: ConfigValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: ConfigValidation.getTitlePattern()
                    },
                    content: {
                        cnLetterNumUlSymbol1To255: ConfigValidation.getValuePattern()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: ConfigValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_config-form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*文章分类表单验证*/
        formValidationCategory: function () {

            $('#js_category-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    pid: {
                        zeroPositiveInteger: true
                    },
                    category: {
                        positiveInteger: true
                    },
                    sole: {
                        letterNumUl2To80: true
                    },
                    title: {
                        cnLetterSpaceNumUl2To80: true
                    },
                    sort: {
                        zeroPositiveInteger: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    pid: {
                        zeroPositiveInteger: CategoryValidation.getPidRange()
                    },
                    category: {
                        positiveInteger: CategoryValidation.getTypeRange()
                    },
                    sole: {
                        letterNumUl2To80: CategoryValidation.getNamePattern()
                    },
                    title: {
                        cnLetterSpaceNumUl2To80: CategoryValidation.getTitlePattern()
                    },
                    sort: {
                        zeroPositiveInteger: CategoryValidation.getSortRange()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: CategoryValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {


                    hideFormMsg();

                    form = $('#js_category-form');
                    ajaxSubmit(form);
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*文章表单验证*/
        formValidationArticle: function () {

            $('#js_article-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    categoryId: {
                        positiveInteger: true
                    },
                    title: {
                        cnLetterSpaceSignNumUl2To80: true
                    },
                    description: {
                        required: true
                    }
                },
                messages: {
                    categoryId: {
                        positiveInteger: ArticleValidation.getCategoryIdPattern()
                    },
                    title: {
                        cnLetterSpaceSignNumUl2To80: ArticleValidation.getTitlePattern()
                    },
                    description: {
                        required: ArticleValidation.getDescriptionPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {


                    if (UE.getEditor('ja_article-content').hasContents()) {
                        hideFormMsg();
                        form = $('#js_article-form');
                        ajaxSubmit(form);
                    } else {
                        showFormMsg(ArticleValidation.getContentPattern());
                    }
                }
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /*系统配置表单验证*/
        formValidationScheduleJob: function () {

            $('#js_schedule-job-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    sole: {
                        letterNumUl2To80: true
                    },
                    title: {
                        letterNumUl2To80: true
                    },
                    beanName: {
                        letterNum2To100: true
                    },
                    methodName: {
                        letterNum1To100: true
                    },
                    params: {
                        maxlength: 255
                    },
                    cronExpression: {
                        required: true
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: true
                    }
                },
                messages: {
                    sole: {
                        letterNumUl2To80: ScheduleJobValidation.getNamePattern()
                    },
                    title: {
                        letterNumUl2To80: ScheduleJobValidation.getTitlePattern()
                    },
                    beanName: {
                        letterNum2To100: ScheduleJobValidation.getBeanNamePattern()
                    },
                    methodName: {
                        letterNum1To100: ScheduleJobValidation.getMethodNamePattern()
                    },
                    params: {
                        maxlength: ScheduleJobValidation.getParamsPattern()
                    },
                    cronExpression: {
                        required: ScheduleJobValidation.getCronExpressionPattern()
                    },
                    remark: {
                        cnLetterNumUlSymbol0To200: ScheduleJobValidation.getRemarkPattern()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();
                    form = $('#js_schedule-job-form');
                    ajaxSubmit(form);
                }

            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /**
         * 测试邮箱
         */
        formValidationMailSendTest: function () {

            $('#js_mail-test-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    email: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: MailValidation.getEmail()
                    }
                },
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_mail-test-form');
                    ajaxSubmit(form);
                }
            });

            // ------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------

        /**
         * 清除缓存
         */
        formValidationCache: function () {

            $('#js_cache-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'help-block help-block-error', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {},
                messages: {},
                invalidHandler: function (event, validator) {
                    showFormMsg();
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                    $(element)
                        .closest('.form-group').addClass('error');
                },
                success: function (label) {
                    $(label)
                        .closest('.form-group').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    var $help_block = element.parents('div').children('.help-block');
                    if ($help_block) {
                        $help_block.hide();
                    }
                    error.insertAfter(element);
                },
                submitHandler: function (form) {

                    hideFormMsg();

                    form = $('#js_cache-form');
                    ajaxSubmit(form);
                }
            });

            // ------------------------------------------------------------------------

        }

        // -------------------------------------------------------------------------------------------------

    };

}();
