var EliteTool = function () {

    var handleInit = function () {

        // ajax 确认请求
        $('.js_ajax-confirm-confirmation').click(function (e) {
            var url = $(this).attr('data-url');
            var options = EliteTool.showSweetAlertConfirm();
            Swal.fire({
                type: options.type,
                title: AlertMessage.getMessageAlertConfirmationTitle(),
                showCancelButton: options.showCancelButton,
                confirmButtonText: options.confirmButtonText,
                confirmButtonClass: options.confirmButtonClass,
                cancelButtonText: options.cancelButtonText,
                cancelButtonClass: options.cancelButtonClass,
                buttonsStyling: options.buttonsStyling,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function () {
                        // 禁止再点击
                        $(this).prop('disabled', true);
                        $.ajax({
                            url: url,
                            method: 'POST',
                            dataType: 'JSON',
                            success: function (result) {
                                if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                                    EliteTool.showSweetAlertSuccess({
                                        title: result.message,
                                        isSkip: true
                                    });
                                } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                                    EliteTool.showSweetAlertError({
                                        title: result.message
                                    });
                                } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                                    EliteTool.showSweetAlertError({
                                        title: result.message
                                    });
                                } else {
                                    EliteTool.showSweetAlertError({
                                        title: result.message
                                    });
                                }
                            },
                            error: function () {
                                EliteTool.showSweetAlertError({
                                    title: AlertMessage.getMessageAlertNetworkError()
                                });
                            }
                        });

                    });
                }
            });

        });

        // -------------------------------------------------------------------------------------------------

        // ajax post 请求
        $('.js_ajax-post').on('click', function () {
            // 请求地址
            var target, query, form;
            // 请求标识 form 或者 url
            var target_form = $(this).attr('data-form');
            var that = this;
            // 是否需要确认
            var need_confirm = false;
            // 表单请求或者 url 请求
            if ('submit' === ($(this).attr('type')) || (target = $(this).attr('data-url'))) {
                form = $('.' + target_form);
                // 无元素
                if (undefined === form.get(0)) {
                    return false;
                } else if (form.get(0).nodeName === 'INPUT' || form.get(0).nodeName === 'SELECT'
                    || form.get(0).nodeName === 'TEXTAREA') { // 按钮请求
                    // 选择框时必须确认
                    form.each(function (k, v) {
                        if (v.type === 'checkbox' && v.checked === true) {
                            need_confirm = true;
                        }
                    });
                    if (!need_confirm) {
                        EliteTool.showSweetAlertWarning({
                            title: AlertMessage.getMessageAlertNotInfo()
                        });
                        return false;
                    }
                    // 确认
                    if ($(this).hasClass('js_ajax-confirm')) {
                        if (need_confirm) {
                            var options = EliteTool.showSweetAlertConfirm();
                            Swal.fire({
                                type: options.type,
                                title: AlertMessage.getMessageAlertConfirmationTitle(),
                                showCancelButton: options.showCancelButton,
                                confirmButtonText: options.confirmButtonText,
                                confirmButtonClass: options.confirmButtonClass,
                                cancelButtonText: options.cancelButtonText,
                                cancelButtonClass: options.cancelButtonClass,
                                buttonsStyling: options.buttonsStyling,
                                closeOnConfirm: false,
                                showLoaderOnConfirm: true,
                                preConfirm: function () {
                                    return new Promise(function () {
                                        // 禁止再点击
                                        $(that).prop('disabled', true);
                                        $.ajax({
                                            url: target,
                                            method: 'POST',
                                            data: form.serialize(),
                                            dataType: 'JSON',
                                            success: function (result) {
                                                if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                                                    EliteTool.showSweetAlertSuccess({
                                                        title: result.message,
                                                        isSkip: true
                                                    });
                                                } else if ("fail" === result.state) {
                                                    EliteTool.showSweetAlertError({
                                                        title: result.message
                                                    });
                                                } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                                                    EliteTool.showSweetAlertWarning({
                                                        title: result.message
                                                    });
                                                } else {
                                                    EliteTool.showSweetAlertError({
                                                        title: result.message
                                                    });
                                                }
                                            },
                                            error: function () {
                                                EliteTool.showSweetAlertError({
                                                    title: AlertMessage.getMessageAlertNetworkError()
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                            return false;
                        }
                        return false;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return false;
        });

        // -------------------------------------------------------------------------------------------------

    };

    /**
     * 改变语言
     */
    var handleLanguage = function () {
        // ajax 请求 简体中文
        $('#js_language-zh').on('click', function () {
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: 'POST',
                success: function () {
                    location.reload();
                },
                error: function () {
                    EliteTool.showSweetAlertError({
                        title: AlertMessage.getMessageAlertNetworkError()
                    });
                }
            });
        });

        // -------------------------------------------------------------------------------------------------

        // ajax 请求 英文
        $('#js_language-en').on('click', function () {
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: 'POST',
                success: function () {
                    location.reload();
                },
                error: function () {
                    EliteTool.showSweetAlertError({
                        title: AlertMessage.getMessageAlertNetworkError()
                    });
                }
            });
        });

        // -------------------------------------------------------------------------------------------------

    };

    // 处理 Bootstrap switches
    var handleBootstrapSwitch = function () {
        if (!$().bootstrapSwitch) {
            return;
        }
        $('.js_switch').bootstrapSwitch();
    };

    // -------------------------------------------------------------------------------------------------

    // 处理 Select 2
    var handleSelect2 = function () {
        if (!$().select2) {
            return;
        }
        $('.js_select2').select2({
            language: $CURRENT_LANGUAGE
        });
    };

    // -------------------------------------------------------------------------------------------------

    return {

        // 主要模块初始化函数
        init: function () {
            handleLanguage();
            handleInit();
            handleBootstrapSwitch();
            handleSelect2();
        },

        // -------------------------------------------------------------------------------------------------

        // 顶部导航高亮
        highlight_top_nav: function (url) {
            $('#sidebarnav').find('a[href="' + url + '"]').addClass('active');
            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').addClass('active');
            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').parent('ul')
                .addClass('in');

            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .addClass('active');

            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .children('a').addClass('active');

            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .parent('ul').addClass('in');

            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .parent('ul').parent('li').addClass('active');

            $('#sidebarnav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .parent('ul').parent('li').children('a').addClass('active');
        },

        // -------------------------------------------------------------------------------------------------

        // TouchSpin
        highlightTouchSpin: function (dom, maxSort, max, step) {
            if (!max) {
                max = maxSort + 100
            }
            if (!step) {
                step = 1
            }
            $(dom).TouchSpin({
                initval: maxSort,
                min: 0,
                max: max,
                stepinterval: step,
                maxboostedstep: 10000000
                //prefix: '$'
            });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert弹出提示
        showSweetAlert: function (options) {
            options = $.extend(true, {
                type: '',// 类型
                title: '',// 名称
                showCancelButton: false,// 是否显示取消按钮
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            //类型
            var type = options.type;
            var title = options.title;
            var confirmButtonText = AlertMessage.getMessageAlertSweetAlertConfirmButtonText();
            var confirmButtonClass = 'btn btn-info btn-custom waves-effect waves-light btn-lg';
            var cancelButtonText = AlertMessage.getMessageAlertSweetAlertCancelButtonText();
            var cancelButtonClass = 'btn btn-white btn-custom waves-effect waves-light btn-lg';
            var showCancelButton = options.showCancelButton;
            var isSkip = options.isSkip;
            var uri = options.uri;
            Swal.fire({
                type: type,
                title: title,
                showCancelButton: showCancelButton,
                confirmButtonText: confirmButtonText,
                confirmButtonClass: confirmButtonClass,
                cancelButtonText: cancelButtonText,
                cancelButtonClass: cancelButtonClass,
                allowOutsideClick: false,
                allowEscapeKey: false,
                buttonsStyling: false
            }).then(function (result) {
                // 点击确定
                if (result.value) {
                    if (isSkip) {
                        if (uri) {
                            location.href = uri;
                        } else {
                            location.reload();
                        }
                    }
                }
            });
        },

        // sweet_alert 成功提示
        showSweetAlertSuccess: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                showCancelButton: false,// 是否显示取消按钮
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var type = 'success';
            var title = options.title;
            var isSkip = options.isSkip;
            var uri = options.uri;
            EliteTool.showSweetAlert({
                type: type,
                title: title,
                isSkip: isSkip,
                uri: uri
            });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 错误提示
        showSweetAlertError: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                showCancelButton: false,// 是否显示取消按钮
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var type = 'error';
            var title = options.title;
            var isSkip = options.isSkip;
            var uri = options.uri;
            EliteTool.showSweetAlert({
                type: type,
                title: title,
                isSkip: isSkip,
                uri: uri
            });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 警告提示
        showSweetAlertWarning: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                showCancelButton: false,// 是否显示取消按钮
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var type = 'warning';
            var title = options.title;
            var isSkip = options.isSkip;
            var uri = options.uri;
            EliteTool.showSweetAlert({
                type: type,
                title: title,
                isSkip: isSkip,
                uri: uri
            });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 确认提示
        showSweetAlertConfirm: function () {
            var options = {};
            options.type = 'info';
            options.showCancelButton = true;
            options.confirmButtonText = AlertMessage.getMessageAlertSweetAlertConfirmButtonText();
            options.confirmButtonClass = 'btn btn-info btn-custom waves-effect waves-light btn-lg';
            options.cancelButtonText = AlertMessage.getMessageAlertSweetAlertCancelButtonText();
            options.cancelButtonClass = 'btn btn-white btn-custom waves-effect waves-light btn-lg';
            options.buttonsStyling = false;
            return options;
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 提示信息 定时关闭
        showSweetAlertTimesClose: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                timer: '',// 多少秒关闭
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var title = options.title;
            var text = options.timer + AlertMessage.getMessageAlertCountDownTimer();
            var timer = options.timer * 1000;
            var isSkip = options.isSkip;
            var uri = options.uri;
            Swal.fire({
                    title: title,
                    text: text,
                    timer: timer,
                    showConfirmButton: false
                },
                function () {
                    if (isSkip) {
                        if (uri) {
                            location.href = uri;
                        } else {
                            location.reload();
                        }
                    }
                });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 提示信息 定时关闭
        showSweetAlertInfoTimesClose: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                timer: '',// 多少秒关闭
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var type = 'info';
            var title = options.title;
            var text = options.timer + AlertMessage.getMessageAlertCountDownTimer();
            var timer = options.timer * 1000;
            var isSkip = options.isSkip;
            var uri = options.uri;
            Swal.fire({
                    type: type,
                    title: title,
                    text: text,
                    timer: timer,
                    showConfirmButton: false
                },
                function () {
                    if (isSkip) {
                        if (uri) {
                            location.href = uri;
                        } else {
                            location.reload();
                        }
                    }
                });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 提示警告信息 定时关闭
        showSweetAlertWarningTimesClose: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                timer: '',// 多少秒关闭
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var type = 'warning';
            var title = options.title;
            var text = options.timer + AlertMessage.getMessageAlertCountDownTimer();
            var timer = options.timer * 1000;
            var isSkip = options.isSkip;
            var uri = options.uri;
            Swal.fire({
                    type: type,
                    title: title,
                    text: text,
                    timer: timer,
                    showConfirmButton: false
                },
                function () {
                    if (isSkip) {
                        if (uri) {
                            location.href = uri;
                        } else {
                            location.reload();
                        }
                    }
                });
        },

        // -------------------------------------------------------------------------------------------------

        // sweet_alert 提示错误信息 定时关闭
        showSweetAlertErrorTimesClose: function (options) {
            options = $.extend(true, {
                title: '',// 名称
                timer: '',// 多少秒关闭
                isSkip: false,// 是否进行跳转
                uri: '' // 跳转链接
            }, options);
            var type = 'error';
            var title = options.title;
            var text = options.timer + AlertMessage.getMessageAlertCountDownTimer();
            var timer = options.timer * 1000;
            var isSkip = options.isSkip;
            var uri = options.uri;
            Swal.fire({
                    type: type,
                    title: title,
                    text: text,
                    timer: timer,
                    showConfirmButton: false
                },
                function () {
                    if (isSkip) {
                        if (uri) {
                            location.href = uri;
                        } else {
                            location.reload();
                        }
                    }
                });
        }

        // -------------------------------------------------------------------------------------------------

    };
}();

jQuery(document).ready(function () {
    EliteTool.init()
});
