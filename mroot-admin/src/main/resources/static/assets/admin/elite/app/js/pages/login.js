var EliteSnippetLogin = function () {

    /**
     * 错误信息
     * @param type
     * @param msg
     */
    var showErrorMsg = function (type, msg) {
        var jsAlert = $('<div class="alert alert-' + type + ' "\
         role="alert"><span></span></div>');

        $('#js_alert-msg').find('.alert').remove();

        jsAlert.find('span').html(msg);

        $('#js_alert-msg').html(jsAlert);
    };

    // ------------------------------------------------------------------------

    /**
     * 错误信息
     */
    var hideErrorMsg = function () {
        $('#js_alert-msg').find('.alert').remove();
    };

    // ------------------------------------------------------------------------

    var handleVerifyImg = function () {
        //刷新验证码
        var verifyImg = $('.js_verify-img').attr('src');
        $('.js_verify-img').click(function () {
            if (verifyImg.indexOf('?') > 0) {
                $('.js_verify-img').attr('src', verifyImg + '&random=' + Math.random());
            } else {
                $('.js_verify-img').attr('src', verifyImg.replace(/\?.*$/, '') + '?' + Math.random());
            }
        });

    };

    /**
     * 带验证码 提交表单
     */
    var handleVerifyCodeSubmit = function () {

        $('#js_verify-code-submit').click(function (e) {
            e.preventDefault();

            $('#js_verify-code-modal').modal('hide');
            $('#js_verify-code').val($('#js_code').val());
            $('#js_code').val('');

            var form = $('#js_login-form');

            var btn = $("#js_login-submit");
            form.validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'invalid-feedback', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    username: {
                        required: true,
                        cnLetterNumUl5To18: true
                    },
                    password: {
                        required: true,
                        letterNum6To18: true
                    },
                    verifyCode: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: LoginValidation.getUsernameRequired(),
                        cnLetterNumUl5To18: LoginValidation.getUsernameCnLetterNumUl5To18()
                    },
                    password: {
                        required: LoginValidation.getPasswordRequired(),
                        letterNum6To18: LoginValidation.getPasswordLetterNum6To18()
                    },
                    verifyCode: {
                        required: LoginValidation.getVerifyCodeRequired()
                    }
                },
                invalidHandler: function (event, validator) { //显示在表单提交错误提示
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                },
                success: function (label) {
                    label.remove();
                },
                errorPlacement: function (error, element) {
                    element.parent('div').append(error);
                },
                submitHandler: function (form) {
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.text(btn.data("logging-text")).attr('disabled', true);

            //表单提交
            var url = form.get(0).action;
            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'JSON',
                data: form.serialize(),
                success: function (result) {
                    btn.text(btn.data("login-text")).attr('disabled', false);
                    $('#js_form-token').val(result.formToken);
                    if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                        hideErrorMsg();
                        if (result.url) {
                            window.location.href = result.url;
                        }
                    } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                        showErrorMsg('danger', result.message);
                    } else if (AlertMessage.getMessageAlertAjaxRepeat() === result.state) {
                        showErrorMsg('info', result.message);
                    } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                        showErrorMsg('warning', result.message);
                    } else {
                        showErrorMsg('primary', result.message);
                    }
                },
                error: function () {
                    btn.text(btn.data("login-text")).attr('disabled', false);
                    showErrorMsg(form, 'danger', AlertMessage.getMessageAlertNetworkError());
                }
            });
        });
    };

    // ------------------------------------------------------------------------

    return {
        init: function () {
            handleVerifyImg();
            handleVerifyCodeSubmit();
        },
        /**
         * 改变语言
         */
        handleLanguage: function () {
            // ajax 请求 简体中文
            $('#js_language-zh').on('click', function () {
                var url = $(this).attr('data-url');
                $.ajax({
                    url: url,
                    method: 'POST',
                    success: function () {
                        location.reload();
                    },
                    error: function () {
                        EliteTool.showSweetAlertError({
                            title: AlertMessage.getMessageAlertNetworkError()
                        });
                    }
                });
            });

            // ajax 请求 英文
            $('#js_language-en').on('click', function () {
                var url = $(this).attr('data-url');
                $.ajax({
                    url: url,
                    method: 'POST',
                    success: function () {
                        location.reload();
                    },
                    error: function () {
                        EliteTool.showSweetAlertError({
                            title: AlertMessage.getMessageAlertNetworkError()
                        });
                    }
                });
            });

            // -------------------------------------------------------------------------------------------------

        },

        // -------------------------------------------------------------------------------------------------
        /*登录表单验证*/
        formValidationLogin: function () {

            var btn = $("#js_login-submit");
            $('#js_login-form').validate({
                errorElement: 'span', // 容器默认输入错误消息
                errorClass: 'invalid-feedback', // 默认的输入错误消息类
                focusInvalid: false, // 不自动移到最后一个错误输入
                rules: {
                    username: {
                        required: true,
                        cnLetterNumUl5To18: true
                    },
                    password: {
                        required: true,
                        letterNum6To18: true
                    }
                },
                messages: {
                    username: {
                        required: LoginValidation.getUsernameRequired(),
                        cnLetterNumUl5To18: LoginValidation.getUsernameCnLetterNumUl5To18()
                    },
                    password: {
                        required: LoginValidation.getPasswordRequired(),
                        letterNum6To18: LoginValidation.getPasswordLetterNum6To18()
                    }
                },
                invalidHandler: function (event, validator) { //显示在表单提交错误提示
                },
                highlight: function (element) { // 标出错误的输入
                    // 设置错误类
                },
                success: function (label) {
                    label.remove();
                },
                errorPlacement: function (error, element) {
                    element.parent('div').append(error);
                },
                submitHandler: function (form) {
                    btn.text(btn.data("logging-text")).attr('disabled', true);
                    form = $('#js_login-form');

                    var isVerifyCode = $('#js_is-verify-code').val();
                    if ("show" !== isVerifyCode) {
                        //表单提交
                        var url = form.get(0).action;
                        $.ajax({
                            url: url,
                            method: 'POST',
                            dataType: 'JSON',
                            data: form.serialize(),
                            success: function (result) {
                                btn.text(btn.data("login-text")).attr('disabled', false);
                                $('#js_form-token').val(result.formToken);
                                if (AlertMessage.getMessageAlertAjaxOk() === result.state) {
                                    hideErrorMsg();
                                    if (result.url) {
                                        window.location.href = result.url;
                                    }
                                } else if (AlertMessage.getMessageAlertAjaxFail() === result.state) {
                                    $('#js_is-verify-code').val(result.isVerifyCode);
                                    showErrorMsg('danger', result.message);
                                } else if (AlertMessage.getMessageAlertAjaxRepeat() === result.state) {
                                    $('#js_is-verify-code').val(result.isVerifyCode);
                                    showErrorMsg('info', result.message);
                                } else if (AlertMessage.getMessageAlertAjaxError() === result.state) {
                                    showErrorMsg('warning', result.message);
                                } else {
                                    $('#js_is-verify-code').val(result.isVerifyCode);
                                    showErrorMsg('primary', result.message);
                                }
                            },
                            error: function () {
                                btn.text(btn.data("login-text")).attr('disabled', false);
                                showErrorMsg(form, 'danger', AlertMessage.getMessageAlertNetworkError());
                            }
                        });
                    } else {
                        hideErrorMsg();
                        // 刷新验证码
                        $('.js_verify-img').click();
                        $('#js_code').val('');
                        btn.text(btn.data("login-text")).attr('disabled', false);
                        $('#js_verify-code-modal').modal({backdrop: 'static', keyboard: false});
                        $('#js_verify-code-modal').modal('show');
                    }
                }
            });

            // ------------------------------------------------------------------------

        }


    };
}();

jQuery(document).ready(function () {
    EliteSnippetLogin.init();
});
