<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>${I18N("login.page.metaTitle")}-${I18N('message.system.name')}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
    <meta name="robots" content="noarchive,nofollow">

    <meta name="keywords"
          content="mroot,MRoot,kotlin,Kotlin,Spring,Spring Boot,Spring Boot2,小小木,快速开发,快速开发平台,${I18N('message.system.name')}">
    <meta name="description" content="MRoot是基于Spring Boot2使用Kotlin编写的快速开发平台-${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['ELITE']}/favicon.ico">
    <link rel="apple-touch-icon" href="${GLOBAL_RESOURCE_MAP['ELITE']}/apple-touch-icon.png">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 页面样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/login-register-lock.css">
    <#-- /* 页面样式结束 */ -->

    <#-- /* 自定义样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/style.css">
    <#-- /* 自定义样式开始 */ -->


    <!--[if lte IE 8]>
    <script>window.location = '${CONTEXT_PATH}/index/error/oops';</script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/html5shiv/html5shiv.js"></script>
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/respond/respond.js"></script>
    <![endif]-->

</head>

<body class="skin-default card-no-border">

<#-- preloader 开始 -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">${I18N('message.system.name')}</p>
    </div>
</div>
<#-- preloader 结束 -->

<#-- 主要布局开始 -->
<section id="wrapper">
    <div class="login-register"
         style="background-image:url(${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/background/login-register.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="js_login-form"
                      action="${CONTEXT_PATH}/do/login"
                      method="post" autocomplete="off">
                    <h3 class="text-center m-b-20"><img
                                src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-1.png"></h3>

                    <div id="js_alert-msg"></div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" name="username" type="text"
                                   placeholder="${I18N('login.username.input')}" value="123456"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" name="password" type="password"
                                   placeholder="${I18N('login.password.input')}" value="123456"></div>
                    </div>

                    <input id="js_form-token" type="hidden" name="formToken" value="${formToken}">
                    <input id="js_is-verify-code" type="hidden" name="isVerifyCode" value="${isVerifyCode}">
                    <input id="js_verify-code" type="hidden" name="verifyCode" value="">

                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn waves-effect waves-light btn-block btn-lg btn-info btn-rounded"
                                    type="submit"
                                    data-login-text="${I18N('login.label')}"
                                    data-logging-text="${I18N('logging.label')}"
                                    id="js_login-submit">${I18N('login.label')}</button>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a class="waves-effect waves-light" id="js_language-zh" data-value="zh"
                                   data-url="${CONTEXT_PATH}/language/zh"
                                   href="javascript:">
                                    ${I18N("message.language.zh")}
                                </a>
                                <a class="waves-effect waves-light" id="js_language-en"
                                   data-value="en"
                                   data-url="${CONTEXT_PATH}/language/en"
                                   href="javascript:">
                                    ${I18N("message.language.en")}
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">${I18N("message.system.name")}</div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</section>
<#-- 主要布局结束 -->

<!-- 验证码弹窗开始 -->
<div class="modal fade" id="js_verify-code-modal" tabindex="-1" role="dialog">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${I18N('login.verifyCode.input')}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <img class="js_verify-img" src="${CONTEXT_PATH}/verify/image"
                         title="${I18N('login.verifyCode.alert')}"
                         alt="${I18N('login.verifyCode.alert')}">
                </div>
                <div class="form-group">
                    <input class="form-control" id="js_code"
                           type="text"
                           value=""
                           maxlength="6"
                           placeholder="${I18N('login.verifyCode.input')}" autocomplete="off">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                    <span>${I18N('message.btn.close')}</span>
                </button>
                <button id="js_verify-code-submit"
                        class="btn btn-danger waves-effect waves-light"><span>${I18N('login.label')}</span>
                </button>
            </div>
        </div>
    </div>

</div>
<!-- 验证码弹窗结束 -->

<#if ("正式环境" == PROFILE)>
    <div style="display: none;">
        <script type="text/javascript" src="https://s13.cnzz.com/z_stat.php?id=1274001409&web_id=1274001409"></script>
        <br>
    </div>
</#if>

<#-- /* 表单验证国际化提示信息 */ -->
<#include "/elite/common/js.ftl">
<#include "/elite/login/loginjs.ftl" />
<#-- /* .表单验证国际化提示信息 */ -->

<#-- /* javascript 开始 */ -->

<#-- jquery 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jquery/jquery-3.2.1.min.js"></script>
<#-- jquery 结束  -->

<#--  Bootstrap 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/popper/popper.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap/js/bootstrap.min.js"></script>
<#-- Bootstrap 结束  -->

<#-- 页面  scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jquery-validation/jquery.validate.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jquery-validation/custom.additional-methods.js"></script>
<#-- 页面  scripts 结束 -->

<!-- 自定义 scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/js/pages/login.js"></script>
<script type="text/javascript">
    $(function () {
        $(".preloader").fadeOut();
        EliteSnippetLogin.handleLanguage();
        EliteSnippetLogin.formValidationLogin();
    });
</script>
<!-- 自定义 scripts 结束 -->

<#-- /* /.javascript 结束 */ -->
</body>
<#-- /* /.body结束 */ -->
</html>
