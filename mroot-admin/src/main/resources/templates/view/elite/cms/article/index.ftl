<@OVERRIDE name="PAGE_SCRIPT_STYLE">
<#-- Footable CSS -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/footable/css/footable.bootstrap.min.css">
</@OVERRIDE>
<@OVERRIDE name="CUSTOM_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/footable-page.css">
</@OVERRIDE>

<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/indexalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">${I18N("message.table.head.title")}</h4>

                <div class="m-t-20 m-b-20">


                    <div class="dt-buttons m-l-10">

                        <@shiro.hasPermission name="${model}/add">
                            <div class="btn-group">
                                <a class="btn btn-primary waves-effect waves-light" href="${add}"><i
                                            class="fa fa-plus"></i><span
                                            class="p-l-5">${I18N("message.table.add.btn")}</span>
                                </a>
                            </div>
                        </@shiro.hasPermission>

                        <@shiro.hasPermission name="${model}/deletebatch or ${model}/recyclebin">

                            <div class="btn-group m-l-10">
                                <button type="button"
                                        class="btn btn-info dropdown-toggle waves-effect waves-light"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        id="dropdownMenuButton">
                                    ${I18N("message.table.more.btn")}
                                </button>
                                <div class="dropdown-menu"
                                     aria-labelledby="dropdownMenuButton">
                                    <@shiro.hasPermission name="${model}/recyclebin">
                                        <a class="dropdown-item waves-effect waves-light js_ajax-post js_ajax-confirm"
                                           data-url="${deleteBatch}"
                                           data-form="js_table-checkboxes"
                                           href="javascript:">
                                            <i class="fa fa-trash"></i><span
                                                    class="p-l-5">${I18N("message.table.delete.btn")}</span>
                                        </a>
                                    </@shiro.hasPermission>
                                    <@shiro.hasPermission name="${model}/recyclebin">
                                        <a class="dropdown-item waves-effect waves-light" href="${recycleBin}">
                                            <i class="far fa-trash-alt"></i><span
                                                    class="p-l-5">${I18N("message.table.recycleBin.btn")}</span></a>
                                    </@shiro.hasPermission>
                                </div>
                            </div>

                        </@shiro.hasPermission>

                    </div>

                    <#-- 搜索工具栏开始 -->

                    <div class="input-group col-lg-3 col-xlg-3">
                        <label class="sr-only">${I18N('message.table.title.text')}</label>
                        <div class="input-group" id="js_search-form">
                            <input type="text" class="form-control"
                                   name="title" value="${title}" autocomplete="off"
                                   placeholder="${I18N('message.table.title.text')}">
                            <div class="input-group-append">
                                <button id="js_search" data-url="${index}" type="button"
                                        class="btn btn-success waves-effect waves-light"><span
                                            class="fas fa-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <#-- 搜索工具栏结束 -->

                </div>

                <div class="table-responsive">
                    <table id="js_table"
                           class="table m-t-30 table-hover no-wrap contact-list footable footable-paging footable-paging-center">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-checkbox m-b-0">
                                    <input type="checkbox" class="custom-control-input js_table-check"
                                           data-set="#js_table .js_table-checkboxes"
                                           autocomplete="off">
                                    <span class="custom-control-label p-l-3">#</span>
                                </label>
                            </th>
                            <#assign th=[
                            "message.table.id",
                            "message.cms.article.list.table.categoryId",
                            "message.table.title",
                            "message.cms.article.list.table.cover",
                            "message.cms.article.list.table.pageView",
                            "message.cms.article.list.table.priority",
                            "message.cms.article.list.table.linkUri",
                            "message.cms.article.list.table.display",
                            "message.table.state",
                            "message.table.gmtCreate",
                            "message.table.gmtCreateIp",
                            "message.table.gmtModified",
                            "message.table.operate.title"
                            ]/>
                            <@tableTh th></@tableTh>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <#if page.records??  && (0 < page.records?size)>
                            <#list page.records as item>
                                <tr>
                                    <td class="align-middle">
                                        <label class="custom-control custom-checkbox m-b-0">
                                            <input type="checkbox" class="custom-control-input js_table-checkboxes"
                                                   name="id[]"
                                                   value="${item.id}" autocomplete="off">
                                            <span class="custom-control-label p-l-3">#</span>
                                        </label>
                                    </td>
                                    <td>${item.id}</td>
                                    <td><@defaultStr item.category.title></@defaultStr></td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<@defaultStr item.title></@defaultStr>"
                                        ><@subStr str=item.title length=item.title?length></@subStr></span>
                                    </td>
                                    <td><span class="cursor-pointer"
                                              data-toggle="popover"
                                              data-placement="bottom"
                                              data-html="true"
                                              data-trigger="hover"
                                              <#if item.cover>
                                                  data-content='<img class="cover-thumbnail" src="<@defaultStr item.cover></@defaultStr>">'
                                              </#if>
                                        > <@subStr str=item.cover length=item.cover?length></@subStr></span>
                                    </td>
                                    <td><@defaultStr item.pageView></@defaultStr></td>
                                    <td><@defaultStr item.priority></@defaultStr></td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<@defaultStr item.linkUri></@defaultStr>"
                                        ><@subStr str=item.linkUri length=item.linkUri?length></@subStr></span>
                                    </td>
                                    <td><@boole item.display></@boole></td>
                                    <td><@status item.status></@status></td>
                                    <td><@dateFormat item.gmtCreate></@dateFormat></td>
                                    <td><@defaultStr item.ip></@defaultStr></td>
                                    <td><@dateFormat item.gmtModified></@dateFormat></td>
                                    <td><@tableOperate item.id></@tableOperate></td>
                                    <td></td>
                                </tr>
                            </#list>
                        <#else>
                            <tr>
                                <td class="text-center" colspan="15"><span>${I18N("message.table.empty.content")}</span>
                                </td>
                            </tr>
                        </#if>
                        </tbody>
                        <tfoot>

                        <@shiro.hasPermission name="${model}/add">
                            <tr>
                                <td colspan="15">

                                    <a class="btn btn-info waves-effect waves-light" href="${add}">
                                        <i class="fa fa-plus"></i><span
                                                class="p-l-5">${I18N("message.table.add.btn")}</span>
                                    </a>

                                </td>
                            </tr>
                        </@shiro.hasPermission>

                        <tr class="footable-paging">
                            <td colspan="15">
                                <#-- 分页开始 -->
                                <div id="paginate" class="m-t-30 footable-pagination-wrapper"></div>
                                <#-- 分页结束 -->
                            </td>
                        </tr>

                        </tfoot>
                    </table>
                </div>

            </div>
        </div>


    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            // 分页
            EliteTable.pagination({
                url: '${index}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}',
                params: function () {
                    return {
                        <#if title??>title: '${title}'</#if>
                    };
                }
            });

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
