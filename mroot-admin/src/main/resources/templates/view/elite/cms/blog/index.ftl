<@OVERRIDE name="PAGE_SCRIPT_STYLE">
<#-- Footable CSS -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/footable/css/footable.bootstrap.min.css">
</@OVERRIDE>
<@OVERRIDE name="CUSTOM_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/footable-page.css">
</@OVERRIDE>

<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/indexalert.ftl">
    </div>

    <div class="col-12">

        <#if page.records??  && (0 < page.records?size)>

            <div class="row">
                <#list page.records as item>
                    <div class="col-lg-3 col-md-6 m-b-30">

                        <div class="card">
                            <#if item.cover??>
                                <img class="card-img-top img-responsive cover-blog" src="${item.cover}"
                                     alt="${item.title}">
                            <#else>
                                <img class="card-img-top img-responsive cover-blog"
                                     src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-5.png"
                                     alt="${item.title}">
                            </#if>
                            <div class="card-body">
                                <h4 class="card-title">${item.title}</h4>
                                <p class="card-text">${item.description}</p>
                                <a href="${view}/${item.id}"
                                   class="btn btn-primary waves-effect waves-light"><span>${I18N("message.cms.blog.list.table.details")}</span></a>
                            </div>
                        </div>
                    </div>
                </#list>
                <table class="table footable footable-paging-center">
                    <thead>
                    <tr></tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                    <tr class="footable-paging">
                        <td>
                            <#-- 分页开始 -->
                            <div id="paginate" class="footable-pagination-wrapper"></div>
                            <#-- 分页结束 -->
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        <#else>

            <div class="row">
                <div class="col-12 m-t-30">
                    <div class="card text-center">
                        <div class="card-body p-t-30">
                            <h4 class="card-title">${I18N("message.table.empty.content")}</h4>
                        </div>
                    </div>
                </div>
            </div>

        </#if>
    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            // 分页
            EliteTable.pagination({
                url: '${index}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}'
            });

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
