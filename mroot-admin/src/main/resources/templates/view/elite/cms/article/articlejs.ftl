<script>

    /**
     * 文章提示信息
     */
    var ArticleValidation = function () {

        var categoryId_pattern = '${I18N("jquery.validation.cms.article.categoryId.pattern")}';
        var title_pattern = '${I18N("jquery.validation.cms.article.title.pattern")}';
        var description_pattern = '${I18N("jquery.validation.cms.article.description.length")}';
        var content_pattern = '${I18N("jquery.validation.cms.articleContent.content.length")}';

        return {

            getCategoryIdPattern: function () {
                return categoryId_pattern;
            },

            getTitlePattern: function () {
                return title_pattern;
            },

            getDescriptionPattern: function () {
                return description_pattern;
            },

            getContentPattern: function () {
                return content_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
