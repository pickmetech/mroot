<#-- 页面提示信息模板函数 -->
<#if message??>
    <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>${message}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span>
        </button>
    </div>
</#if>
<#if failMessage??>
    <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>${failMessage}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span>
        </button>
    </div>
</#if>
<#if failListMessage??>
    <div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>
            <#list failListMessage as item>
                ${item}<br>
            </#list>
        </strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span>
        </button>
    </div>
</#if>
