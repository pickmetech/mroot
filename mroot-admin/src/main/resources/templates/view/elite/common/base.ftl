<#include "/elite/common/macro.ftl">
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>
        <#if headTitle??>
            ${headTitle}_${I18N("message.metaTitle")}
        <#else>
            ${I18N('message.metaTitle')}
        </#if>
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
    <meta name="robots" content="noarchive,nofollow">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">


    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['ELITE']}/favicon.ico">
    <link rel="apple-touch-icon" href="${GLOBAL_RESOURCE_MAP['ELITE']}/apple-touch-icon.png">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 页面插件样式开始 */ -->
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/sweetalert2/sweetalert2.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap-switch/bootstrap-switch.min.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css">
    <@BLOCK name="PAGE_SCRIPT_STYLE" ></@BLOCK>
    <#-- /* /.页面插件样式结束 */ -->

    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/style.css">

    <#-- /* 页面样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/bootstrap-switch.css">
    <@BLOCK name="PAGE_STYLE" ></@BLOCK>
    <#-- /* 页面样式开始 */ -->

    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/custom.css">

    <#-- /* 自定义样式开始 */ -->
    <@BLOCK name="CUSTOM_STYLE" ></@BLOCK>
    <#-- /* .自定义样式结束 */ -->

    <!--[if lte IE 8]>
    <script>window.location = '${CONTEXT_PATH}/index/error/oops';</script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/html5shiv/html5shiv.js"></script>
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/respond/respond.js"></script>
    <![endif]-->

</head>

<body class="horizontal-nav skin-default fixed-layout">


<#-- preloader 开始 -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">${I18N('message.system.name')}</p>
    </div>
</div>
<#-- preloader 结束 -->

<#-- 页面开始 -->
<div id="main-wrapper">

    <#-- Topbar header 开始 -->
    <#include "/elite/common/header.ftl">
    <#-- Topbar header 结束 -->

    <#-- page-wrapper 开始 -->
    <div class="page-wrapper">

        <#-- Container fluid  开始 -->
        <div class="container-fluid">

            <#-- 面包屑 开始 -->

            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">${breadcrumbTitle}</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <#if breadcrumbUrl??>
                                <li class="breadcrumb-item active">${breadcrumbUrl.title}</li>
                            </#if>
                            <#if breadcrumbMenu??>
                                <li class="breadcrumb-item active">${breadcrumbMenu.title}</li>
                            </#if>
                            <#if breadcrumbChile??>
                                <li class="breadcrumb-item active"><a
                                            href="${CONTEXT_PATH}${breadcrumbChile.url}"
                                            class="waves-effect waves-light">${breadcrumbChile.title}</a>
                                </li>
                            </#if>
                        </ol>
                    </div>
                </div>
            </div>

            <#-- 面包屑 结束 -->

            <#-- Page Content 开始 -->
            <div class="row">
                <@BLOCK name="MAIN_CONTENT"></@BLOCK>
            </div>
            <#-- Page Content 结束 -->

            <#-- 配色 开始 -->

            <div class="right-sidebar">
                <div class="slimscrollright">
                    <div class="rpanel-title">${I18N('message.right.panel.title')}<span><i
                                    class="ti-close right-side-toggle"></i></span>
                    </div>
                    <div class="r-panel-body">
                        <ul id="themecolors" class="m-t-20">
                            <li><b>${I18N('message.right.panel.light.title')}</b></li>
                            <li><a href="javascript:void(0)" data-skin="skin-default"
                                   class="default-theme working">1</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-green" class="green-theme">2</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-red" class="red-theme">3</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-blue" class="blue-theme">4</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-purple" class="purple-theme">5</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-megna" class="megna-theme">6</a>
                            </li>
                            <li class="d-block m-t-30"><b>${I18N('message.right.panel.dark.title')}</b></li>
                            <li><a href="javascript:void(0)" data-skin="skin-default-dark"
                                   class="default-dark-theme">7</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-green-dark" class="green-dark-theme">8</a>
                            </li>
                            <li><a href="javascript:void(0)" data-skin="skin-red-dark" class="red-dark-theme">9</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-blue-dark" class="blue-dark-theme">10</a>
                            </li>
                            <li><a href="javascript:void(0)" data-skin="skin-purple-dark"
                                   class="purple-dark-theme">11</a></li>
                            <li><a href="javascript:void(0)" data-skin="skin-megna-dark"
                                   class="megna-dark-theme">12</a></li>
                        </ul>

                    </div>
                </div>
            </div>

            <#-- 配色 结束 -->

        </div>

        <#-- Container fluid 结束-->

    </div>
    <#--    page-wrapper 结束 -->

    <#-- footer 开始 -->
    <#include "/elite/common/footer.ftl">
    <#-- footer 结束 -->

</div>

<#if ("正式环境" == PROFILE)>
    <div style="display: none;">
        <script type="text/javascript" src="https://s13.cnzz.com/z_stat.php?id=1274001409&web_id=1274001409"></script>
        <br>
    </div>
</#if>

<#-- /* script开始 */ -->

<script>
    var $CONTEXT_PATH = '${CONTEXT_PATH}';

    var $CURRENT_LANGUAGE = <@i18nType language></@i18nType>;

</script>

<#-- /* 表单验证国际化提示信息*/ -->
<#include "/elite/common/js.ftl">
<#-- /* 表单验证国际化提示信息 页面继承 */ -->
<@BLOCK name="PAGE_MESSAGE"></@BLOCK>

<#-- jquery 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jquery/jquery-3.2.1.min.js"></script>
<#-- jquery 结束  -->
<#--  Bootstrap 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/popper/popper.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap/js/bootstrap.min.js"></script>
<#-- Bootstrap 结束  -->
<#-- slimscrollbar scrollbar JavaScript -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/js/perfect-scrollbar.jquery.min.js"></script>
<#-- Wave Effects -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/js/waves.js"></script>
<#-- Menu sidebar -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/js/sidebarmenu.js"></script>
<#-- stickey kit -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/stickykit/sticky-kit.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/sparkline/jquery.sparkline.min.js"></script>


<#-- /* 自定义script开始 */ -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/js/custom.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/sweetalert2/sweetalert2.all.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/select2/js/select2.full.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/select2/js/i18n/zh-CN.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>

<#-- /* 页面级别插件开始 */ -->
<#--<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/jquery-lazyload/jquery.lazyload.js"></script>-->
<@BLOCK name="PAGE_PLUGIN" ></@BLOCK>
<#-- /* /.页面级别插件结束 */ -->

<#-- /* 页面级别script开始 */ -->
<@BLOCK name="PAGE_SCRIPT" ></@BLOCK>
<#-- /* /.页面级别script结束 */ -->

<@BLOCK name="CUSTOM_SCRIPT" ></@BLOCK>
<#-- /* 自定义script结束 */ -->

<#-- /* /.script 结束 */ -->

</body>
<#-- /* /.body结束 */ -->
</html>
