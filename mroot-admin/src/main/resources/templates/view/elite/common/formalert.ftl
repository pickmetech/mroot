<#-- 页面提示信息模板函数 -->
<div class="alert alert-warning alert-dismissible fade hide" role="alert" id="js_form-msg">
    <strong id="js_alert-text">${I18N("message.form.alert.content")}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span>
    </button>
</div>

<#include "/elite/common/indexalert.ftl">
