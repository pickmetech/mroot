<@OVERRIDE name="PAGE_SCRIPT_STYLE">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jstree/themes/default/style.min.css">
</@OVERRIDE>
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_role-authorization-form"
                      action="${authorizationSave}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                        <input name="id" type="hidden" value="${role.id}">

                        <input id="js_rules" name="rules" type="hidden" value="">

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3"></label>
                            <div class="col-md-9">
                                <div id="js_rule-tree">

                                    <ul>
                                        <#list treeRules as topRules>
                                            <li id="${topRules.id}" data-jstree='{ "opened" : true }'>
                                                ${topRules.title}
                                                <#list topRules.childrenList as childRules>
                                                    <ul>
                                                        <li id="${childRules.id}" data-jstree='{ "opened" : true }'>
                                                            ${childRules.title}
                                                            <#list childRules.childrenList as leftRules>
                                                                <ul>
                                                                    <li id="${leftRules.id}"
                                                                        data-jstree='{ "opened" : true }'>
                                                                        ${leftRules.title}
                                                                        <#list leftRules.childrenList as buttonRules>
                                                                            <ul>
                                                                                <li id="${buttonRules.id}">
                                                                                    ${buttonRules.title}</li>
                                                                            </ul>
                                                                        </#list>
                                                                    </li>
                                                                </ul>
                                                            </#list>
                                                        </li>
                                                    </ul>
                                                </#list>
                                            </li>
                                        </#list>
                                    </ul>

                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/role/rolejs.ftl">
</@OVERRIDE>
<#-- /* 页面级别插件开始 */ -->
<@OVERRIDE name="PAGE_PLUGIN">
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jstree/jstree.min.js"></script>
</@OVERRIDE>
<#-- /* 页面级别插件结束 */ -->

<#--/* 页面级别script开始 */-->
<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
    <#include "/elite/scriptplugin/jqueryvalidate.ftl">
    <#include "/elite/scriptplugin/formvalidation.ftl">
</@OVERRIDE>
<#--/* 页面级别script结束 */-->
<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        var ruleTree = function () {
            $('#js_rule-tree').jstree({
                "core": {
                    "themes": {
                        "responsive": false
                    }
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder font-warning"
                    },
                    "file": {
                        "icon": "fa fa-file font-warning"
                    }
                },
                "plugins": ["types", "checkbox"],
                "checkbox": {
                    "cascade": "undetermined",
                    "three_state": false
                }
            });

            /**
             * 加载完成后 选中节点
             */
            $('#js_rule-tree').on('ready.jstree', function (e, data) {
                var ruleIds = [${ruleString}];
                $('#js_rule-tree').jstree('check_node', ruleIds, true);
            });

            /**
             * 选中节点
             */
            $('#js_rule-tree').on('select_node.jstree', function (e, data) {
            });
        };


        jQuery(document).ready(function () {
            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            ruleTree();

            EliteFormValidation.formValidationRoleAuthorization();

        });
    </script>
</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
