<@OVERRIDE name="PAGE_SCRIPT_STYLE">
<#-- Footable CSS -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/footable/css/footable.bootstrap.min.css">
</@OVERRIDE>
<@OVERRIDE name="CUSTOM_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/footable-page.css">
</@OVERRIDE>

<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/indexalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">${I18N("message.table.head.title")}</h4>

                <div class="m-t-20 m-b-20">


                    <div class="dt-buttons m-l-10">


                    </div>

                    <#-- 搜索工具栏开始 -->

                    <div class="input-group col-lg-3 col-xlg-3">
                        <label class="sr-only">${I18N('message.table.categoryDescription.text')}</label>
                        <div class="input-group" id="js_search-form">
                            <input type="text" class="form-control"
                                   name="categoryDescription" value="${categoryDescription}" autocomplete="off"
                                   placeholder="${I18N('message.table.categoryDescription.text')}">
                            <div class="input-group-append">
                                <button id="js_search" data-url="${index}" type="button"
                                        class="btn btn-success waves-effect waves-light"><span
                                            class="fas fa-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <#-- 搜索工具栏结束 -->

                </div>

                <div class="table-responsive">
                    <table id="js_table"
                           class="table m-t-30 table-hover no-wrap contact-list footable footable-paging footable-paging-center">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-checkbox m-b-0">
                                    <input type="checkbox" class="custom-control-input js_table-check"
                                           data-set="#js_table .js_table-checkboxes"
                                           autocomplete="off">
                                    <span class="custom-control-label p-l-3">#</span>
                                </label>
                            </th>
                            <#assign th=[
                            "message.table.id",
                            "message.system.mailLog.list.table.categoryDescription",
                            "message.system.mailLog.list.table.patternDescription",
                            "message.system.mailLog.list.table.fromMail",
                            "message.system.mailLog.list.table.toMail",
                            "message.system.mailLog.list.table.times",
                            "message.system.mailLog.list.table.sendResult",
                            "message.table.state",
                            "message.table.gmtCreate",
                            "message.table.gmtCreateIp",
                            "message.table.gmtModified",
                            "message.table.operate.title"
                            ]/>
                            <@tableTh th></@tableTh>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <#if page.records??  && (0 < page.records?size)>
                            <#list page.records as item>
                                <tr>
                                    <td class="align-middle">
                                        <label class="custom-control custom-checkbox m-b-0">
                                            <input type="checkbox" class="custom-control-input js_table-checkboxes"
                                                   name="id[]"
                                                   value="${item.id}" autocomplete="off">
                                            <span class="custom-control-label p-l-3">#</span>
                                        </label>
                                    </td>
                                    <td>${item.id}</td>
                                    <td><@defaultStr item.categoryDescription></@defaultStr></td>
                                    <td><@defaultStr item.patternDescription></@defaultStr></td>
                                    <td><@defaultStr item.fromMail></@defaultStr></td>
                                    <td><@defaultStr item.toMail></@defaultStr></td>
                                    <td><@defaultStr item.times></@defaultStr></td>
                                    <td><@defaultStr item.sendResult></@defaultStr></td>
                                    <td><@status item.status></@status></td>
                                    <td><@dateFormat item.gmtCreate></@dateFormat></td>
                                    <td><@defaultStr item.ip></@defaultStr></td>
                                    <td><@dateFormat item.gmtModified></@dateFormat></td>
                                    <td><@tableOperate item.id></@tableOperate></td>
                                    <td></td>
                                </tr>
                            </#list>
                        <#else>
                            <tr>
                                <td class="text-center" colspan="14"><span>${I18N("message.table.empty.content")}</span>
                                </td>
                            </tr>
                        </#if>
                        </tbody>
                        <tfoot>


                        <tr class="footable-paging">
                            <td colspan="14">
                                <#-- 分页开始 -->
                                <div id="paginate" class="m-t-30 footable-pagination-wrapper"></div>
                                <#-- 分页结束 -->
                            </td>
                        </tr>

                        </tfoot>
                    </table>
                </div>

            </div>
        </div>


    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            // 分页
            EliteTable.pagination({
                url: '${index}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}',
                params: function () {
                    return {
                        <#if title??>title: '${title}'</#if>
                    };
                }
            });

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
