<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_user-form" action="${update}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                        <input name="id" type="hidden" value="${user.id}">


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.id")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${user.id}</p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.username")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${user.username}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.nickName")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${user.nickName}</p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.email")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr user.email></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.phone")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr user.phone></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.realName")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr user.realName></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label class="control-label text-right col-md-3">${I18N("message.form.category")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <#list userTypeMap?keys as key>
                                    <fieldset>
                                        <div class="custom-control custom-radio">
                                            <input id="js_category-${key}" type="radio"
                                                   value="${key}" name="category"
                                                   <#if !(user.category)??>checked</#if>
                                                    <#if (user.category == key)>checked</#if>
                                                   class="custom-control-input">
                                            <label class="custom-control-label"
                                                   for="js_category-${key}">${userTypeMap[key]}</label>
                                        </div>
                                    </fieldset>
                                </#list>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.state")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.state.normal.btn")}"
                                       data-off-text="${I18N("message.form.state.disable.btn")}"
                                       <#if (stateNormal == user.state)>checked</#if>
                                       name="stateStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/user/userjs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');


            EliteFormValidation.formValidationUser();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
