<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.eventId")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${loggingEvent.eventId}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.levelString")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.levelString></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.callerFilename")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.callerFilename></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.loggerName")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.loggerName></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.threadName")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.threadName></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.callerClass")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.callerClass></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.callerMethod")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.callerMethod></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.arg0")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.arg0></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.arg1")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.arg1></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.arg2")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.arg2></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.arg3")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.arg3></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.formattedMessage")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.formattedMessage></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.referenceFlag")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.referenceFlag></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.callerLine")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr loggingEvent.callerLine></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.loggingEvent.form.timestmp")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@number2DateFormat loggingEvent.timestmp></@number2DateFormat></p>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@viewFormOperate></@viewFormOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
