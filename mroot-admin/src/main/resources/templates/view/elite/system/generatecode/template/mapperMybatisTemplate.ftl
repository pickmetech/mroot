<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${daoPackageName}.${classPrefix}.${className}DAO">

    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${doPackageName}.${classPrefix}.${className}DO">
    <#list generateModels as var>
        <#if dataId == var.name>
        <#--<id column="${var.name}" property="${var.camelCase}" jdbcType="${var.dataType}"/>-->
          <id column="${var.name}" property="${var.camelCaseName}" jdbcType="${var.dataType}"/>
        <#else>
        <#--<result column="${var.name}" property="${var.camelCase}" jdbcType="${var.dataType}"/>-->
           <result column="${var.name}" property="${var.camelCaseName}" jdbcType="${var.dataType}"/>
        </#if>
    </#list>
    </resultMap>

    <!-- 查询全部列 -->
    <sql id="Base_Column_List">
<#list generateModels as var>
<#if !var_has_next >
    <#if "category" == var.camelCaseName>
        ${var.name}
    <#elseif "sole" == var.camelCaseName>
        ${var.name}
    <#elseif "title" == var.camelCaseName>
        ${var.name}
    <#elseif "state" == var.camelCaseName>
        ${var.name}
    <#elseif "content" == var.camelCaseName>
        ${var.name}
    <#else>
        ${var.name}
    </#if>
<#else>
    <#if "category" == var.camelCaseName>
        ${var.name},
    <#elseif "sole" == var.camelCaseName>
        ${var.name},
    <#elseif "title" == var.camelCaseName>
        ${var.name},
    <#elseif "state" == var.camelCaseName>
        ${var.name},
    <#elseif "content" == var.camelCaseName>
        ${var.name},
    <#else>
        ${var.name},
    </#if>
</#if>
</#list>
    </sql>


    <!-- 根据 id 批量删除 -->
    <delete id="deleteBatchById" parameterType="java.util.List">
        <if test="null != idArray">
            DELETE
            FROM ${tableName}
            WHERE id IN
            <foreach collection="idArray" item="item" open="(" close=")" separator=",">
            ${r"#{"}item${r"}"}
            </foreach>
        </if>
    </delete>

    <!-- 根据 id 批量更新状态 -->
    <update id="updateBatchStatusById" parameterType="java.util.Map">
        <if test="null != idArray">
            UPDATE ${tableName}
            SET state = ${r"#{"}state${r"}"},
            gmt_modified = ${r"#{"}gmtModified${r"}"}
            WHERE id IN
            <foreach collection="idArray" item="item" open="(" close=")" separator=",">
            ${r"#{"}item${r"}"}
            </foreach>
        </if>
    </update>


</mapper>
