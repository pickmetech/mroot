<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>
        <#if headTitle??>
            ${headTitle}_${I18N("message.metaTitle")}
        <#else>
            ${I18N('message.metaTitle')}
        </#if>
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
    <meta name="robots" content="noarchive,nofollow">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">


    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['ELITE']}/favicon.ico">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/style.css">
    <link href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/error-pages.css" rel="stylesheet">

    <!--[if lte IE 8]>
    <script>window.location = '${CONTEXT_PATH}/index/error/oops';</script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/html5shiv/html5shiv.js"></script>
    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/respond/respond.js"></script>
    <![endif]-->

</head>

<body class="horizontal-nav skin-default fixed-layout">

<section class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h1>${I18N('message.exception500')}</h1>
            <h3 class="text-uppercase"></h3>
            <p class="text-muted m-t-30 m-b-30">${I18N('message.exception500.content')}</p>
            <a href="${CONTEXT_PATH}/main"
               class="btn btn-info btn-rounded waves-effect waves-light m-b-40">${I18N('message.exception.back.index')}</a>
        </div>
    </div>
</section>

<#-- jquery 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/jquery/jquery-3.2.1.min.js"></script>
<#-- jquery 结束  -->
<#--  Bootstrap 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/popper/popper.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/bootstrap/js/bootstrap.min.js"></script>
<#-- Bootstrap 结束  -->
<#-- Wave Effects -->
<script src="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/js/waves.js"></script>
</body>
<#-- /* /.body结束 */ -->
</html>
