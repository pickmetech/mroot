<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

<#-- 数据统计开始 -->

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row m-t-20">
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white"><@numberFormatHasZero userCount></@numberFormatHasZero></h1>
                                <h6 class="text-white">${I18N("message.main.widget.user")}</h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card">
                            <div class="box bg-primary text-center">
                                <h1 class="font-light text-white"><@numberFormatHasZero articleCount></@numberFormatHasZero></h1>
                                <h6 class="text-white">${I18N("message.main.widget.article")}</h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card">
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><@numberFormatHasZero requestLogCount></@numberFormatHasZero></h1>
                                <h6 class="text-white">${I18N("message.main.widget.requestLog")}</h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card">
                            <div class="box bg-dark text-center">
                                <h1 class="font-light text-white"><@numberFormatHasZero logCount></@numberFormatHasZero></h1>
                                <h6 class="text-white">${I18N("message.main.widget.log")}</h6>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

<#-- 数据统计结束 -->

<#-- 表格列表开始 -->

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">${I18N("message.main.widget.osInfo")}</h5>
                <ul class="feeds">
                    <#list osInfoMap?keys as key>
                        <li>
                            <div class="bg-info"><i class="far fa-bell"></i></div>${key}<span
                                    class="text-muted">${osInfoMap[key]}</span></li>
                    </#list>
                </ul>
            </div>
        </div>
    </div>
<#-- 表格列表结束 -->
</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');
        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
