<#-- /* 页面级别插件开始 */ -->
<@OVERRIDE name="PAGE_PLUGIN">
<#--    <#include "/elite/scriptplugin/footable.ftl">-->
</@OVERRIDE>
<#-- /* 页面级别插件结束 */ -->

<#--/* 页面级别script开始 */-->
<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/table.ftl">
    <#include "/elite/scriptplugin/tool.ftl">
    <#include "/elite/scriptplugin/pagination.ftl">
</@OVERRIDE>
<#--/* 页面级别script结束 */-->
