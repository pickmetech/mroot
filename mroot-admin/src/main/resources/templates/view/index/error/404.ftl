<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>${headTitle}_${I18N("message.metaTitle")}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
    <meta name="robots" content="noarchive,nofollow">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 全局样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/vendors.bundle.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/style.bundle.css">
    <#-- /* /.全局样式结束 */ -->

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/favicon.ico">

</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1"
         style="background-image: url(${GLOBAL_RESOURCE_MAP['APP']}/media/img/error/bg1.jpg);">
        <div class="m-error_container">
					<span class="m-error_number">
						<h1>
                        ${I18N('message.exception404')}
                        </h1>
					</span>
            <p class="m-error_desc">
                ${I18N('message.exception404.content')}
                <br><br>
                <span style="margin-left: 30px;">
                <a class="m-link m--font-bold" href="/">
                ${I18N('message.exception.back.index')}
                </a>
            </span>
            </p>
        </div>
    </div>
</div>


<#-- /* javascript 开始 */ -->

<#-- /* /.javascript 结束 */ -->
</body>
<#-- /* /.body结束 */ -->
</html>
