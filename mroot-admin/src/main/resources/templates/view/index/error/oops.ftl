<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>Oops!抱歉,你的浏览器版本过低,我们无法提供支持</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/favicon.ico"/>
    <style>
        html, body, div, p, span, h3, ol, li {
            padding: 0;
            margin: 0;
        }

        ol, li {
            list-style-type: none;
            list-style-image: none;
        }

        html, body, .main {
            height: 100%;
        }

        .main {
            height: 100%;
            font-family: "微软雅黑", "宋体", serif;
            background: #e8e8e8;
        }

        .con {
            margin: 0 auto;
            width: 565px;
        }

        .result {
            padding: 110px 0 80px 0;
            background: #008ead;
        }

        .result h3 {
            border-bottom: #fff 1px solid;
            padding-bottom: 10px;
            width: 100%;
            margin-bottom: 10px;
            color: #fff;
            font-size: 22px;
            font-weight: bold;
        }

        .result div {
            color: #e8e8e8;
        }

        .result p {
            font-size: 18px;
        }

        .result ol {
            padding-left: 25px;
        }

        .result ol li {
            padding-top: 20px;
            list-style-type: decimal;
            font-size: 16px;
        }

        .explain {
            padding: 20px 0;
            background: #e2e2e2;
            color: #666;
            font-size: 16px;
        }

        .explain p {
            margin-top: 10px;
        }
    </style>

    <script src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/plugins/jquery/jquery.min.js"></script>
    <script>
        (function () {
            var ie = !-[1,];
            if (!ie) {
                location.href = "${CONTEXT_PATH}/";
            }
            if (navigator.userAgent.indexOf("MSIE 11.") > 0) {
                location.href = "${CONTEXT_PATH}/";
            }
        })();
    </script>
</head>
<body>
<div class="main">
    <div class="result">
        <div style="text-align: center;padding: 10px 0 20px 0;">
            <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/oops/ie-old.png" alt="ie-old"
                 style="width:70px;height:70px;">
        </div>
        <div class="con">
            <h3>Oops! 您使用的浏览器版本过于陈旧，我们无法支持！</h3>
            <h4>建议您使用以下浏览器（点击图标下载）</h4>
        </div>

        <div style="text-align: center;margin-top:50px;margin-bottom: 30px;padding-left: 20px;">
            <a href="https://www.firefox.com.cn/" target="_blank"
               style="margin:10px;width:70px;height:70px;">
                <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/oops/firefox.png" title="火狐浏览器"
                     alt="Firefox"
                     style="width:70px;height:70px;"> <span>Firefox</span>
            </a>
            <a href="https://www.google.cn/chrome/" target="_blank"
               style="margin:10px;width:70px;height:70px;">
                <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/oops/Chrome.png" title="谷歌浏览器"
                     alt="chrome"
                     style="width:70px;height:70px;"> <span>Chrome</span>
            </a>
            <a href="https://www.apple.com/cn/safari/" target="_blank" style="margin:10px;width:70px;height:70px;">
                <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/oops/Safari.png" title="Safari"
                     alt="Safari"
                     style="width:70px;height:70px;"> <span>Safari</span>
            </a>
            <a href="https://support.microsoft.com/zh-cn/hub/4230784/internet-explorer-help" target="_blank"
               style="margin:10px;width:70px;height:70px;">
                <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/oops/ie9.png" title="ie" alt="ie"
                     style="width:70px;height:70px;">
                <span>更高版本的IE</span>
            </a>
        </div>

        <div class="con">
            <div>
                <p>亲爱的朋友们：</p>
                <ol>
                    <li>我们网站采集用比较先进的技术，相对而言更加简洁、美观、安全</li>
                    <li>微软也鼓励升级，会有更多更炫的特效，给你带来不一般的视觉体验</li>
                    <li>我们不支持IE9及以下浏览器，支持所有非IE内核的浏览器</li>
                    <li>国内浏览器请切换成极速模式或者高速模式</li>
                </ol>
            </div>
        </div>

    </div>

    <div class="explain">
        <div class="con">
            <h3 style="text-align: left;">以下浏览器我们完美支持：</h3>

            <p>1. 360浏览器&nbsp;&nbsp;<a href="https://browser.360.cn/ee/" target="_blank">去官网免费下载</a></p>

            <p>2. 搜狗高速浏览器&nbsp;&nbsp;<a href="https://ie.sogou.com/" target="_blank">去官网免费下载</a></p>

            <p>
                3. UC浏览器&nbsp;&nbsp;<a href="https://www.uc.cn/" target="_blank">去官网免费下载</a></p>

            <p>4. 遨游云浏览器&nbsp;&nbsp;<a href="http://www.maxthon.cn/" target="_blank">去官网免费下载</a>
            </p>

            <p>5. 猎豹浏览器&nbsp;&nbsp;<a href="https://www.liebao.cn/" target="_blank">去官网免费下载</a></p>

            <p>6. 百度浏览器&nbsp;&nbsp;<a href="https://liulanqi.baidu.com/" target="_blank">去官网免费下载</a></p>

            <p>
                7. QQ浏览器&nbsp;&nbsp;<a href="https://browser.qq.com/" target="_blank">去官网免费下载</a>
            </p>
        </div>
    </div>
</div>
</body>
</html>
