<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>
        <#if headTitle??>
            ${headTitle}
        <#else>
            ${I18N('message.metaTitle')}
        </#if>
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/plugins/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/style/style.css">

    <script src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/plugins/jquery/jquery.min.js"></script>

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/favicon.ico">
    <link rel="apple-touch-icon" href="${GLOBAL_RESOURCE_MAP['RESOURCE']}/apple-touch-icon.png">

    <!--[if lte IE 9]>
    <script>window.location = '${CONTEXT_PATH}/index/error/oops';</script>
    <![endif]-->


</head>
<#-- /* /.body 开始 */ -->
<body>
<#-- /* /.wrapper 开始 */ -->
<div class="wrapper">

    <#-- /* /.section 开始 */ -->
    <section class="section section--header">
        <#-- /* /.header 开始 */ -->
        <header>
            <div class="content">
                <div class="header__handler">
                    <div class="sidebar">
                            <span class="hamburger">
                                	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129"
                                         width="40px" height="40px">
										<g>
											<path d="m91.4,33.5h-53.8c-2.3,0-4.1,1.8-4.1,4.1 0,2.3 1.8,4.1 4.1,4.1h53.9c2.3,0 4.1-1.8 4.1-4.1-0.1-2.3-1.9-4.1-4.2-4.1z"
                                                  fill="#FFFFFF"/>
												<path d="m91.4,87.4h-53.8c-2.3,0-4.1,1.8-4.1,4.1 0,2.3 1.8,4.1 4.1,4.1h53.9c2.3,0 4.1-1.8 4.1-4.1-0.1-2.3-1.9-4.1-4.2-4.1z"
                                                      fill="#FFFFFF"/>
													<path d="m91.4,60.4h-53.8c-2.3,0-4.1,1.8-4.1,4.1 0,2.3 1.8,4.1 4.1,4.1h53.9c2.3,0 4.1-1.8 4.1-4.1-0.1-2.3-1.9-4.1-4.2-4.1z"
                                                          fill="#FFFFFF"/></g>
												</svg>
                            </span>
                        <ul class="sidebar__menu">
                            <li class="sidebar__menu__item active">
                                <a href="${CONTEXT_PATH}/">更新记录</a>
                            </li>
                        </ul>
                    </div>
                    <a href="${CONTEXT_PATH}/" class="metronic__logo">
                        <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/img/logo-2.png" alt="MRoot">
                    </a>
                </div>
                <div class="header__references">
                    <a href="https://gitee.com/eryang/mroot"
                       class="header__references_purchase" target="_blank">码云地址</a>
                </div>
            </div>
        </header>
        <#-- /* /.header 结束 */ -->
        <#-- /* /.content 开始 */ -->
        <div class="content">

            <#-- /* /.intro 开始 */ -->
            <div class="intro">
                <img src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/img/logo-1.png" alt="MRoot">
                <h2 class="intro__title">${I18N('message.system.name')}</h2>
                <a href="${CONTEXT_PATH}/login" class="intro__video" data-lity>
                    <span>进入后台</span>
                    <i class="glyph-icon flaticon-visible"></i>
                </a>
            </div>
            <#-- /* /.intro 结束 */ -->

            <#-- /* /.main__menu_wrap 开始 */ -->
            <div class="main__menu_wrap">

                <ul class="main__menu">
                    <li class="main__menu__item active">
                        <a href="${CONTEXT_PATH}/">更新记录</a>
                    </li>
                </ul>

            </div>
            <#-- /* /.main__menu_wrap 结束 */ -->

        </div>
        <#-- /* /.content 结束 */ -->
    </section>
    <#-- /* /.section 结束 */ -->

    <#-- /* /.section 开始 */ -->



    <@BLOCK name="MAIN_CONTENT"></@BLOCK>


    <#-- /* /.section 结束 */ -->

    <#-- /* /.section 开始 */ -->
    <section class="section section--bg">

        <div class="content">
            <blockquote class="blockquote">数据库：多数据源支持，Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能<br/>
                持久层：MyBatis持久化，使用MyBatis-Plus优化，减少sql开发量<br/>
                MVC：基于Spring Mvc注解，Rest风格Controller，Exception统一管理<br/>
                任务调度：Quartz, 可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能<br/>
                国际化：基于Spring的国际化信息<br/>
                安全框架：Shiro进行权限控制，灵活的权限控制，可控制权限到按钮级别<br/>
                前端：使用Bootstrap，优美的页面，丰富的插件<br/>
                缓存、线程、安全防护、日志、CDN、邮箱、代码生成、友好的代码结构及注释<br/>
                ... ...<br/>
            </blockquote>
        </div>
    </section>
    <#-- /* /.section 结束 */ -->

    <#-- /* /.section 开始 */ -->
    <section class="section section--bg">

        <div class="content">
            <h2 class="title text-center">MRoot小小木快速开发平台</h2>
        </div>
    </section>
    <#-- /* /.section 结束 */ -->

</div>
<#-- /* /.wrapper 结束 */ -->

<#-- 底部开始 -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center pb-4">
                <p>2015 - ${.now?string("yyyy")}<span
                            class="text-color-light p-lg-1">&copy;${I18N("message.system.develop")}</span>皖ICP备18003198号-1
                </p>
            </div>
        </div>
    </div>
</footer>
<#-- 底部结束 -->

<#if ("正式环境" == PROFILE)>
    <div style="display: none;">
        <script type="text/javascript" src="https://s13.cnzz.com/z_stat.php?id=1274001409&web_id=1274001409"></script>
        <br>
    </div>
</#if>

<script src="${GLOBAL_RESOURCE_MAP['RESOURCE']}/index/plugins/bootstrap/js/bootstrap.min.js"></script>
<@BLOCK name="CUSTOM_SCRIPT"></@BLOCK>
</body>
<#-- /* /.body 结束 */ -->
</html>
