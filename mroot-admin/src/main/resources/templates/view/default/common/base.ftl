<#include "/default/common/macro.ftl">
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>
        <#if headTitle??>
            ${headTitle}_${I18N("message.metaTitle")}
        <#else>
            ${I18N('message.metaTitle')}
        </#if>
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
    <meta name="robots" content="noarchive,nofollow">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 全局样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['VENDORS']}/base/vendors.bundle.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['BASE']}/style.bundle.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['APP']}/css/custom.css">
    <#-- /* /.全局样式结束 */ -->

    <#-- /* 页面插件样式开始 */ -->
    <@BLOCK name="PAGE_SCRIPT_STYLE" ></@BLOCK>
    <#-- /* /.页面插件样式结束 */ -->

    <#-- /* 页面样式开始 */ -->
    <@BLOCK name="PAGE_STYLE" ></@BLOCK>
    <#-- /* /.页面样式结束 */ -->

    <#-- /* 自定义样式开始 */ -->
    <@BLOCK name="CUSTOM_STYLE" ></@BLOCK>
    <#-- /* .自定义样式结束 */ -->

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/favicon.ico">
    <link rel="apple-touch-icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/apple-touch-icon.png">

    <!--[if lte IE 9]>
    <script>window.location = '${CONTEXT_PATH}/index/error/oops';</script>
    <![endif]-->

</head>

<body class="m-page--fluid m-page--loading-enabled m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">

<#-- 页面开始 -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <#include "/default/common/header.ftl">


    <#-- Body 开始 -->
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <#-- 二级头部开始 -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            ${breadcrumbTitle}
                        </h3>
                        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                            <li class="m-nav__item m-nav__item--home">
                                <a href="${CONTEXT_PATH}/main" class="m-nav__link m-nav__link--icon">
                                    <i class="m-nav__link-icon la la-home"></i>
                                </a>
                            </li>
                            <#if breadcrumbUrl??>
                                <li class="m-nav__separator">
                                    -
                                </li>
                                <li class="m-nav__item">
                                    <a href="javascript:" class="m-nav__link">
											<span class="m-nav__link-text">
                                                ${breadcrumbUrl.title}
                                            </span>
                                    </a>
                                </li>
                            </#if>
                            <#if breadcrumbMenu??>
                                <li class="m-nav__separator">
                                    -
                                </li>
                                <li class="m-nav__item">
                                    <a href="javascript:" class="m-nav__link">
											<span class="m-nav__link-text">
                                                ${breadcrumbMenu.title}
                                            </span>
                                    </a>
                                </li>
                            </#if>
                            <#if breadcrumbChile??>
                                <li class="m-nav__separator">
                                    -
                                </li>
                                <li class="m-nav__item">
                                    <a href="${CONTEXT_PATH}${breadcrumbChile.url}" class="m-nav__link">
											<span class="m-nav__link-text">
                                                ${breadcrumbChile.title}
                                            </span>
                                    </a>
                                </li>
                            </#if>
                        </ul>
                    </div>
                </div>
            </div>
            <#-- 二级头部结束 -->

            <#-- 页面主要内容开始 -->
            <div class="m-content">
                <@BLOCK name="MAIN_CONTENT"></@BLOCK>
            </div>
            <#-- 页面主要内容结束 -->
        </div>
    </div>
    <#-- Body 结束 -->


    <#include "/default/common/footer.ftl">

    <#-- 返回头部开始 -->
    <div id="m_scroll_top" class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top"
         data-scroll-offset="500"
         data-scroll-speed="300">
        <i class="la la-arrow-up"></i>
    </div>
    <#-- 返回头部结束 -->

</div>
<#-- 页面结束 -->

<#if ("正式环境" == PROFILE)>
    <div style="display: none;">
        <script type="text/javascript" src="https://s13.cnzz.com/z_stat.php?id=1274001409&web_id=1274001409"></script>
        <br>
        <a href="http://webscan.360.cn/index/checkwebsite/url/mroot.yuneryu.com">
            <img border="0"
                 src="http://img.webscan.360.cn/status/pai/hash/a5a3f71e98c92ed1a86f327acc301191/?size=74x27"/></a>
    </div>
</#if>

<#-- /* script开始 */ -->

<#-- /* 表单验证国际化提示信息*/ -->
<#include "/default/common/js.ftl">
<#-- /* 表单验证国际化提示信息 页面继承 */ -->
<@BLOCK name="PAGE_MESSAGE"></@BLOCK>

<!-- 全局 Scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/base/vendors.bundle.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['BASE']}/scripts.bundle.js"></script>
<!-- 全局 Scripts 结束 -->

<#-- /* 页面级别插件开始 */ -->
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/jquery-lazyload/jquery.lazyload.js"></script>
<@BLOCK name="PAGE_PLUGIN" ></@BLOCK>
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/js/app.js"></script>
<#-- /* /.页面级别插件结束 */ -->
<script>
    var $CONTEXT_PATH = '${CONTEXT_PATH}';
</script>

<#-- /* 页面级别script开始 */ -->
<@BLOCK name="PAGE_SCRIPT" ></@BLOCK>
<#-- /* /.页面级别script结束 */ -->

<#-- /* 自定义script开始 */ -->
<@BLOCK name="CUSTOM_SCRIPT" ></@BLOCK>
<#-- /* 自定义script结束 */ -->


<#-- /* /.script 结束 */ -->

</body>
<#-- /* /.body结束 */ -->
</html>
