<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title><#if headTitle??>
            ${headTitle}_${I18N("message.metaTitle")}
        <#else>
            ${I18N('message.exception.title')}_${I18N('message.metaTitle')}
        </#if></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
    <meta name="robots" content="noarchive,nofollow">

    <meta name="keywords" content="${I18N('message.system.name')}">
    <meta name="description" content="${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 全局样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['VENDORS']}/base/vendors.bundle.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['BASE']}/style.bundle.css">
    <#-- /* /.全局样式结束 */ -->

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/favicon.ico">

</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1"
         style="background-image: url(${GLOBAL_RESOURCE_MAP['APP']}/media/img/error/bg1.jpg);">
        <div class="m-error_container">
					<span class="m-error_number">
						<h1>
                        ${I18N('message.exception500')}
                        </h1>
					</span>
            <p class="m-error_desc">
                ${exceptionMessage}
                <br><br>
                <span style="margin-left: 30px;">
                <a class="m-link m--font-bold" href="${CONTEXT_PATH}/main">
                ${I18N('message.exception.back.index')}
                </a>&nbsp;&nbsp;<a class="m-link m--font-bold" href="${refererUrl}">
                ${I18N('message.exception.back.referer')}
                </a>
            </span>
            </p>
        </div>
    </div>
</div>


<#-- /* javascript 开始 */ -->

<!-- 全局 Scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/base/vendors.bundle.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['BASE']}/scripts.bundle.js"></script>
<!-- 全局 Scripts 结束 -->

<#-- /* /.javascript 结束 */ -->
</body>
<#-- /* /.body结束 */ -->
</html>
