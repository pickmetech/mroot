<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>${I18N("login.page.metaTitle")}-${I18N('message.system.name')}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <#-- /* 禁止网络爬虫抓取 */ -->
<meta name="robots" content="noarchive,nofollow">

    <meta name="keywords"
          content="mroot,MRoot,kotlin,Kotlin,Spring,Spring Boot,Spring Boot2,小小木,快速开发,快速开发平台,${I18N('message.system.name')}">
    <meta name="description" content="MRoot是基于Spring Boot2使用Kotlin编写的快速开发平台-${I18N('message.system.name')}">
    <meta name="author" content="${I18N('message.system.develop')}">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 全局样式开始 */ -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['VENDORS']}/base/vendors.bundle.css">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['BASE']}/style.bundle.css">
    <#--<link rel="stylesheet"-->
    <#--href="${GLOBAL_RESOURCE_MAP['APP']}/css/custom.css">-->
    <#-- /* /.全局样式结束 */ -->

    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/favicon.ico">
    <link rel="apple-touch-icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/apple-touch-icon.png">

    <!--[if lte IE 9]>
    <script>window.location = '${CONTEXT_PATH}/index/error/oops';</script>
    <![endif]-->

</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3"
         id="m_login" style="background-image: url(${GLOBAL_RESOURCE_MAP['APP']}/media/img/bg/bg-2.jpg);">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="javascript:">
                        <img src="${GLOBAL_RESOURCE_MAP['APP']}/media/img/logos/logo-1.png">
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            ${I18N("login.page.title")}
                        </h3>

                    </div>
                    <form class="m-login__form m-form" id="login_form"
                          action="${CONTEXT_PATH}/do/login" method="post" autocomplete="off">

                        <div class="form-group m-form__group">
                            <input class="form-control m-input"
                                   name="username"
                                   type="text"
                                   value="123456"
                                   maxlength="18"
                                   placeholder="${I18N('login.username.input')}"
                            >
                        </div>

                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last"
                                   name="password"
                                   type="password"
                                   value="123456"
                                   maxlength="18"
                                   placeholder="${I18N('login.password.input')}"
                            >
                        </div>


                        <input id="formToken" type="hidden" name="formToken" value="${formToken}">

                        <input id="isVerifyCode" type="hidden" name="isVerifyCode" value="${isVerifyCode}">

                        <input id="verifyCode" type="hidden" name="verifyCode" value="">

                        <div class="m-login__form-action">

                            <button id="m_login_signin_submit"
                                    class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                ${I18N('login.label')}
                            </button>
                        </div>

                    </form>
                </div>

                <div class="m-login__account">
							<span class="m-login__account-msg">
                            ${I18N("message.system.name")}
                            </span>&nbsp;&nbsp;
                    <a class="m-link m-link--light m-login__account-link" id="language_zh" data-value="zh"
                       data-url="${CONTEXT_PATH}/language/zh"
                       href="javascript:">
                        ${I18N("message.language.zh")}
                    </a>
                    <a class="m-link m-link--light m-login__account-link" id="language_en"
                       data-value="en"
                       data-url="${CONTEXT_PATH}/language/en"
                       href="javascript:">
                        ${I18N("message.language.en")}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 验证码弹窗开始 -->
<div class="modal fade" id="verify_code_modal" tabindex="-1" role="dialog" aria-labelledby="verifyCodeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="verifyCodeModalLabel">${I18N('login.verifyCode.input')}</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <img class="verifyImg" src="${CONTEXT_PATH}/verify/image"
                         title="${I18N('login.verifyCode.alert')}"
                         alt="${I18N('login.verifyCode.alert')}">
                </div>
                <div class="form-group">
                    <input class="form-control m-input" id="verify_code"
                           type="text"
                           value=""
                           maxlength="6"
                           placeholder="${I18N('login.verifyCode.input')}" autocomplete="off">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    ${I18N('message.btn.close')}
                </button>
                <button id="m_verify_code_submit"
                        class="btn btn-primary">
                    ${I18N('login.label')}
                </button>
            </div>
        </div>
    </div>
</div>
<!-- 验证码弹窗结束 -->

<#if ("正式环境" == PROFILE)>
    <div style="display: none;">
        <script type="text/javascript" src="https://s13.cnzz.com/z_stat.php?id=1274001409&web_id=1274001409"></script>
        <br>
        <a href="http://webscan.360.cn/index/checkwebsite/url/mroot.yuneryu.com">
            <img border="0"
                 src="http://img.webscan.360.cn/status/pai/hash/a5a3f71e98c92ed1a86f327acc301191/?size=74x27"/></a>
    </div>
</#if>

<#-- /* 表单验证国际化提示信息 */ -->
<#include "/default/common/js.ftl">
<#include "/default/login/loginjs.ftl" />
<#-- /* .表单验证国际化提示信息 */ -->

<#-- /* javascript 开始 */ -->

    <!-- 全局 Scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/base/vendors.bundle.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['BASE']}/scripts.bundle.js"></script>
    <!-- 全局 Scripts 结束 -->

<#-- /* 页面级别插件开始 */ -->
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/jquery-lazyload/jquery.lazyload.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/js/app.js"></script>
<#-- /* /.页面级别插件结束 */ -->

    <!-- 页面 Scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['APP']}/js/custom.additional-methods.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['APP']}/js/login.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['APP']}/js/tool.js"></script>
    <!-- 页面 Scripts 结束 -->

<#-- /* /.javascript 结束 */ -->
</body>
<#-- /* /.body结束 */ -->
</html>
