# 后台${classComment}页面
# AOP 日志
# ${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.index=${classComment}列表
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.add=添加${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.save=保存添加${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.edit=修改${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.update=更新修改${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.view=查看${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.delete=删除${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.delete.batch=批量删除${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.recycleBin=${classComment}回收站
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.recover=恢复${classComment}
message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.recover.batch=批量恢复${classComment}
# 列表表格页面
# ${classComment}列表
# list table
<#list generateModels as var>
    <#if "id" != var.camelCaseName && "category" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "sole" != var.camelCaseName && "title" != var.camelCaseName
    && "sort" != var.camelCaseName && "remark" != var.camelCaseName
    && "state" != var.camelCaseName>
        # ${var.comment}
        message.${classPrefix}.${classFirstLowerCaseName}.list.table.${var.camelCaseName}=${var.comment}
    </#if>
</#list>

# add 新增${classComment}页面
# 添加${classComment}
message.${classPrefix}.${classFirstLowerCaseName}.add.meta.title=添加${classComment}
# 添加${classComment}表单
message.${classPrefix}.${classFirstLowerCaseName}.add.title=添加${classComment}表单
# edit 修改${classComment}页面
# 修改${classComment}
message.${classPrefix}.${classFirstLowerCaseName}.edit.meta.title=修改${classComment}
# 修改${classComment}表单
message.${classPrefix}.${classFirstLowerCaseName}.edit.title=修改${classComment}表单
# 查看${classComment}表单
message.${classPrefix}.${classFirstLowerCaseName}.view.meta.title=查看${classComment}表单
# 查看${classComment}
message.${classPrefix}.${classFirstLowerCaseName}.view.title=查看${classComment}

<#list generateModels as var>
    <#if "id" != var.camelCaseName && "category" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "sole" != var.camelCaseName && "title" != var.camelCaseName
    && "sort" != var.camelCaseName && "remark" != var.camelCaseName
    && "state" != var.camelCaseName>
        # ${var.comment}
        message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}=${var.comment}
        # 请输入${var.comment}
        message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}.text=请输入${var.comment}
        # 请输入${var.comment}
        message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}.text.help=${var.comment}
    </#if>
</#list>

# hibernate validator 验证提示信息
<#list generateModels as var>
    <#if "id" != var.camelCaseName && "category" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "sole" != var.camelCaseName && "title" != var.camelCaseName
    && "sort" != var.camelCaseName && "remark" != var.camelCaseName
    && "state" != var.camelCaseName>
        validation.${classPrefix}.${classFirstLowerCaseName}.${var.camelCaseName}.pattern=${var.comment}不能为空，只能输入
    </#if>
</#list>

# jquery validator 验证提示信息
# ${classFirstLowerCaseName}
<#list generateModels as var>
    <#if "id" != var.camelCaseName && "category" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "sole" != var.camelCaseName && "title" != var.camelCaseName
    && "sort" != var.camelCaseName && "remark" != var.camelCaseName
    && "state" != var.camelCaseName>
        jquery.validation.${classPrefix}.${classFirstLowerCaseName}.${var.camelCaseName}.pattern=${var.comment}不能为空，只能输入
    </#if>
</#list>
