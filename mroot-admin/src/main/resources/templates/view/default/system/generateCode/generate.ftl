<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pagealert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="generate_form" action="${save}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

            <#include "/default/common/formalert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <input type="hidden" name="table" value="${table}">


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.generateCode.form.currentTable")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${table}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.generateCode.form.model")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="modelPackageName"
                           placeholder="${I18N("message.generateCode.form.model.span")}"
                           value="wang.encoding.mroot.domain.entity">
                    <span class="m-form__help">${I18N("message.generateCode.form.model.span")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.generateCode.form.mapper")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="mapperPackageName"
                           placeholder="${I18N("message.generateCode.form.mapper.span")}"
                           value="wang.encoding.mroot.dao">
                    <span class="m-form__help">${I18N("message.generateCode.form.mapper.span")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.generateCode.form.service")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="service"
                           placeholder="${I18N("message.generateCode.form.service.span")}"
                           value="wang.encoding.mroot.service">
                    <span class="m-form__help">${I18N("message.generateCode.form.service.span")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.generateCode.form.controller")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="controller"
                           placeholder="${I18N("message.generateCode.form.controller.span")}"
                           value="wang.encoding.mroot.admin.controller">
                    <span class="m-form__help">${I18N("message.generateCode.form.controller.span")}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

            <div class="m-form__group form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.generateCode.form.prefix")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
						<span class="m-switch m-switch--icon m-switch--info">
							<label>
							<input type="checkbox" name="tablePrefix">
							<span></span>
                            </label>
						</span>
                </div>
            </div>

        </div>

    <#-- 提交按钮 -->
    <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
        formvalidation.formValidationGenerate();
    });
</script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
