<@OVERRIDE name="MAIN_CONTENT">

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ${I18N("message.form.head.title")}
                    </h3>
                </div>
            </div>
        </div>

        <form class="m-form m-form--state m-form--fit m-form--label-align-right">
            <div class="m-portlet__body">

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.id")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static">${mailLog.id}</span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.categoryDescription")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.categoryDescription></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.patternDescription")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.patternDescription></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.fromMail")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.fromMail></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.toMail")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.toMail></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.content")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.content></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.times")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.times></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mailLog.form.sendResult")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.sendResult></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.state")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static">${mailLog.status}</span>
                    </div>
                </div>
                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.gmtCreate")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@dateFormat  mailLog.gmtCreate></@dateFormat></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.gmtCreateIp")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@defaultStr mailLog.ip></@defaultStr></span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.gmtModified")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <span class="m-form__control-static"><@dateFormat  mailLog.gmtModified></@dateFormat></span>
                    </div>
                </div>

            </div>

            <@viewFormOperate></@viewFormOperate>

        </form>

    </div>

</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            Tool.highlight_top_nav('${navIndex}');
        });
    </script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
