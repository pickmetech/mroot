<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pagealert.ftl">

    <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">

    <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
    <h3 class="m-portlet__head-text">
    ${I18N("message.table.head.title")}
    </h3>
    </div>
    </div>

    </div>

    <div class="m-portlet__body">

    <div class="row">
    <div class="col-xl-12">

    <div class="m-section">
    <div class="m-section__content">

    <div class="m-form m-form--label-align-right">
    <div class="row align-items-center">
    <div class="col-xl-8 order-2 order-xl-1">

    <div class="form-group m-form__group row align-items-center">

    <@shiro.hasPermission name="${model}/add">
        <div class="col-md-2">
        <div class="btn-group">
        <a class="btn btn-primary m-btn m-btn--icon" href="${add}">
        <span>
    <i class="fa fa-plus"></i>
        <span>
        ${I18N("message.table.add.btn")}
        </span>
        </span>
        </a>
        </div>
        <div class="d-md-none m--margin-bottom-10"></div>
        </div>

    </@shiro.hasPermission>

    <@shiro.hasPermission name="${model}/deletebatch or ${
    model}/recyclebin">
        <div class="col-md-2">

        <div class="btn-group">
        <button type="button" class="btn btn-info dropdown-toggle"
        data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false"
        id="dropdownMenuButton">
        ${I18N("message.table.more.btn")}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <@shiro.hasPermission name="${model}/deletebatch">
            <a class="dropdown-item ajax_post ajax_confirm"
            data-url="${deletebatch}"
        data-form="ids"
        href="javascript:">
        <i class="fa fa-trash-o"></i>${I18N("message.table.delete.btn")}
            </a>
        </@shiro.hasPermission>
        <@shiro.hasPermission name="${model}/recyclebin">
            <a class="dropdown-item" href="${recyclebin}">
        <i class="fa fa-ban"></i>${I18N("message.table.recycleBin.btn")}
            </a>
        </@shiro.hasPermission>
        </div>
        </div>

        <div class="d-md-none m--margin-bottom-10"></div>
        </div>


    </@shiro.hasPermission>

    </div>

    </div>
    </div>
    </div>
    <div class="m-form m-form--label-align-right m--margin-top-20">
    <div class="row align-items-center">
    <div class="col-xl-8 order-2 order-xl-1">

    <div class="form-group m-form__group row align-items-center">
    <div class="col-md-4">
    <div class="m-input-icon m-input-icon--left search-form">
<input type="text" class="form-control m-input"
       name="title" value="${title}"
placeholder="${I18N('message.table.title.text')}"
       autocomplete="off">
<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
                                                    </span>
    </div>
<div class="d-md-none m--margin-bottom-10"></div>
    </div>
    <div class="col-md-8">
<button id="search" data-url="${index}"
        class="btn btn-primary m-btn m-btn--icon" type="button">
    <span><i class="fa fa-check"></i>
    <span>${I18N("message.table.search.btn")}</span>
    </span>
    </button>
    </div>
<div class="d-md-none m--margin-bottom-10"></div>
    </div>
    </div>

    </div>
    </div>

    </div>

    </div>
    <div class="m-datatable m-datatable--default">
    <table id="sample" class="table table-bordered table-hover m-table m-table--head-bg-brand">
    <thead>
<tr>
<th style="width:1%;">
    <label class="m-checkbox m-checkbox--solid m-checkbox--info">
        <input class="group-checkable" type="checkbox" data-set="#sample .checkboxes"
               autocomplete="off">
        <span></span>
    </label>
</th>
    <#assign th=[
    "message.table.id",
    "message.table.sole",
    "message.table.title",
    "message.system.scheduleJob.list.table.beanName",
    "message.system.scheduleJob.list.table.methodName",
    "message.system.scheduleJob.list.table.params",
    "message.system.scheduleJob.list.table.cronExpression",
    "message.table.state",
    "message.table.remark",
    "message.table.gmtCreate",
    "message.table.gmtCreateIp",
    "message.table.gmtModified",
    "message.table.operate.title"
    ]/>
    <@tableTh th></@tableTh>
</tr>
    </thead>
    <tbody>
    <#if page.records??  && (0 < page.records?size)>
        <#list page.records as item>
            <tr>
            <th>

            <label class="m-checkbox m-checkbox--solid m-checkbox--info">
            <input class="checkboxes ids" type="checkbox" name="id[]"
            value="${item.id}"
        autocomplete="off">
        <span></span>
            </label>

            </th>
            <td>${item.id}</td>
            <td><@defaultStr item.sole></@defaultStr></td>
            <td>
        <span data-toggle="m-tooltip" data-placement="top"
        title="<@defaultStr item.title></@defaultStr>">
            <@subStr str=item.title length=item.title?length></@subStr>
            </span>
            </td>
            <td><@defaultStr item.beanName></@defaultStr></td>
            <td><@defaultStr item.methodName></@defaultStr></td>
            <td><@defaultStr item.params></@defaultStr></td>
            <td><@defaultStr item.cronExpression></@defaultStr></td>
            <td><@defaultStr item.status></@defaultStr></td>
            <td>
        <span data-toggle="m-tooltip" data-placement="top"
        title="<@defaultStr item.remark></@defaultStr>">
            <@subStr str=item.remark length=item.remark?length></@subStr>
            </span>
            </td>
            <td><@dateFormat item.gmtCreate></@dateFormat></td>
            <td><@defaultStr item.ip></@defaultStr></td>
            <td><@dateFormat item.gmtModified></@dateFormat></td>
            <td><@tableOperate item.id></@tableOperate></td>
            </tr>
        </#list>
    <#else>
        <tr>
        <td class="text-center"
        colspan="14">
        ${I18N("message.table.empty.content")}</td>
        </tr>
    </#if>
    </tbody>
    </table>

    <div id="paginate"
         class="m-datatable__pager m-datatable--paging-loaded clearfix"></div>
    </div>

    </div>
    </div>

    </div>

    </div>

</@OVERRIDE>
<#include "/default/scriptplugin/index.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        // 分页
        Table.pagination({
            url: '${index}',
            totalRow: '${page.total}',
            pageSize: '${page.size}',
            pageNumber: '${page.current}',
            params: function () {
                return {
                    <#if title??>title: '${title}'</#if>
                };
            }
        });

    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
