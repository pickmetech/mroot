 /*
  * // +-------------------------------------------------------------------------------------------------
  * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
  * // +-------------------------------------------------------------------------------------------------
  * // |                             独在异乡为异客         每逢佳节倍思亲
  * // +-------------------------------------------------------------------------------------------------
  * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
  * // +-------------------------------------------------------------------------------------------------
  */

 // -----------------------------------------------------------------------------------------------------
 // +----------------------------------------------------------------------------------------------------
 // |                   ErYang出品 属于小极品          共同学习    共同进步
 // +----------------------------------------------------------------------------------------------------
 // -----------------------------------------------------------------------------------------------------


 package wang.encoding.mroot.admin;

 import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
 import lombok.extern.slf4j.Slf4j;
 import org.springframework.boot.SpringApplication;
 import org.springframework.boot.autoconfigure.SpringBootApplication;
 import org.springframework.boot.builder.SpringApplicationBuilder;
 import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
 import org.springframework.cache.annotation.EnableCaching;
 import org.springframework.context.ConfigurableApplicationContext;
 import org.springframework.context.annotation.PropertySource;
 import org.springframework.scheduling.annotation.EnableAsync;
 import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
 import wang.encoding.mroot.common.component.ProfileComponent;


 /**
  * 入口
  *
  * EnableAsync(proxyTargetClass = true) 配置代理为 cglib 代理(默认使用的是 jdk 动态代理)
  *
  * @author ErYang
  */
 @SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class}, scanBasePackages = {"wang.encoding.mroot"})
 @EnableCaching
 @EnableAsync(proxyTargetClass = true)
 @PropertySource({"classpath:common.properties", "classpath:mail.properties", "classpath:ueditor.properties"})
 @EnableRedisHttpSession
 @Slf4j
 public class MrootAdminApplication extends SpringBootServletInitializer {


     @Override
     protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
         return application.sources(MrootAdminApplication.class);
     }

     // -------------------------------------------------------------------------------------------------

     public static void main(String[] args) {
         ConfigurableApplicationContext ctx = SpringApplication.run(MrootAdminApplication.class, args);
         // 得到当前模式
         if (logger.isInfoEnabled()) {
             logger.info(">>>>>>>>服务启动成功，当前模式[{},{}]<<<<<<<<", ProfileComponent.getActiveProfile(),
                     ProfileComponent.getActiveProfile2Description());
         }
     }

     // -------------------------------------------------------------------------------------------------

 }

 // -----------------------------------------------------------------------------------------------------

 // End MrootAdminApplication class

 /* End of file MrootAdminApplication.java */
 /* Location: ./src/main/java/wang/encoding/mroot/admin/MrootAdminApplication.java */

 // -----------------------------------------------------------------------------------------------------
 // +----------------------------------------------------------------------------------------------------
 // |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
 // +----------------------------------------------------------------------------------------------------
 // -----------------------------------------------------------------------------------------------------
