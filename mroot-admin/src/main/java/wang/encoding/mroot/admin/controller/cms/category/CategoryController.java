/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.cms.category;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.cms.AdminCategoryService;
import wang.encoding.mroot.vo.admin.entity.cms.category.AdminCategoryGetVO;
import wang.encoding.mroot.common.enums.CategoryTypeEnum;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;


/**
 * 后台 文章分类 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/cms/category")
public class CategoryController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/cms/category";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/cms/category";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "category";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 添加文章
     */
    private static final String ADD_ARTICLE = "/addarticle";
    private static final String ADD_ARTICLE_URL = MODULE_NAME + ADD_ARTICLE;
    private static final String ADD_ARTICLE_VIEW = VIEW_PATH + ADD_ARTICLE;
    /**
     * 文章模块
     */
    private static final String ARTICLE_MODULE_NAME = "/cms/article";
    private static final String SAVE_ARTICLE_URL_NAME = "saveArticle";
    private static final String SAVE_ARTICLE_URL = ARTICLE_MODULE_NAME + SAVE;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;


    private final AdminCategoryService adminCategoryService;

    @Autowired
    public CategoryController(AdminCategoryService adminCategoryService) {
        this.adminCategoryService = adminCategoryService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminCategoryGetVO adminCategoryGetVO = new AdminCategoryGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminCategoryGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminCategoryGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminCategoryGetVO> pageInt = new Page<>();
        IPage<AdminCategoryGetVO> pageAdmin = adminCategoryService
                .list2page(super.initPage(pageInt), adminCategoryGetVO, AdminCategoryGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 最大排序值
        int maxSort = adminCategoryService.getMax2Sort();
        modelAndView.addObject(MAX_SORT_NAME, maxSort + 1);
        // 分类类型
        modelAndView.addObject("categoryTypeMap", this.listCategoryType());
        this.getCategoriesLt32Tree();
        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param categoryGetVO AdminCategoryGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_SAVE)
    @FormToken(remove = true)
    public Object save(AdminCategoryGetVO categoryGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();

            // 创建 AdminCategoryGetVO 对象
            AdminCategoryGetVO adminCategoryGetVO = this.initAddData(categoryGetVO);

            // 发布的文章是否需要审核
            String auditStr = super.request.getParameter("auditStr");
            if (StringUtils.isNotBlank(auditStr) && auditStr.equals(configConst.getBootstrapSwitchEnabled())) {
                adminCategoryGetVO.setAudit(BooleanEnum.YES.getKey());
            } else {
                adminCategoryGetVO.setAudit(BooleanEnum.NO.getKey());
            }
            // 是否允许发布内容
            String allowStr = super.request.getParameter("allowStr");
            if (StringUtils.isNotBlank(allowStr) && allowStr.equals(configConst.getBootstrapSwitchEnabled())) {
                adminCategoryGetVO.setAllow(BooleanEnum.YES.getKey());
            } else {
                adminCategoryGetVO.setAllow(BooleanEnum.NO.getKey());
            }
            // 是否前台可见
            String displayStr = super.request.getParameter("displayStr");
            if (StringUtils.isNotBlank(displayStr) && displayStr.equals(configConst.getBootstrapSwitchEnabled())) {
                adminCategoryGetVO.setDisplay(BooleanEnum.YES.getKey());
            } else {
                adminCategoryGetVO.setDisplay(BooleanEnum.NO.getKey());
            }

            // Hibernate Validation  验证数据
            String validationResult = adminCategoryService.validationCategory(adminCategoryGetVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(adminCategoryGetVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增 文章分类
            boolean result = adminCategoryService.save(adminCategoryGetVO);
            if (result) {
                // 异步博客模块文章分类缓存
                Future<String> asyncResult = adminControllerAsyncTask
                        .clearAllCache2Pattern(CacheNameConst.BLOG_CATEGORY_CACHE);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "新增文章分类，清空博客模块缓存");
            }
            return super.initSaveJSONObject(result);

        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);


        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminCategoryGetVO categoryGetVO = adminCategoryService.getById(idValue);
        if (null == categoryGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == categoryGetVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }

        modelAndView.addObject(VIEW_MODEL_NAME, categoryGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());
        // 分类类型
        modelAndView.addObject("categoryTypeMap", this.listCategoryType());
        // 是的状态
        modelAndView.addObject("booleanYes", BooleanEnum.YES.getKey());
        this.getCategoriesLt32Tree();
        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminCategoryGetVO categoryGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 数据真实性
            AdminCategoryGetVO categoryGetVOBefore = adminCategoryService.getById(idValue);
            if (null == categoryGetVOBefore) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == categoryGetVOBefore.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 创建 AdminCategoryGetVO 对象
            AdminCategoryGetVO validationCategory = BeanMapperComponent
                    .map(categoryGetVOBefore, AdminCategoryGetVO.class);
            validationCategory.setCategory(categoryGetVOBefore.getCategory());
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationCategory.setState(StateEnum.NORMAL.getKey());
            } else {
                validationCategory.setState(StateEnum.DISABLE.getKey());
            }

            // 发布的文章是否需要审核
            String auditStr = super.request.getParameter("auditStr");
            if (StringUtils.isNotBlank(auditStr) && auditStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationCategory.setAudit(BooleanEnum.YES.getKey());
            } else {
                validationCategory.setAudit(BooleanEnum.NO.getKey());
            }
            // 是否允许发布内容
            String allowStr = super.request.getParameter("allowStr");
            if (StringUtils.isNotBlank(allowStr) && allowStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationCategory.setAllow(BooleanEnum.YES.getKey());
            } else {
                validationCategory.setAllow(BooleanEnum.NO.getKey());
            }
            // 是否前台可见
            String displayStr = super.request.getParameter("displayStr");
            if (StringUtils.isNotBlank(displayStr) && displayStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationCategory.setDisplay(BooleanEnum.YES.getKey());
            } else {
                validationCategory.setDisplay(BooleanEnum.NO.getKey());
            }

            this.initEditData(validationCategory, categoryGetVO);

            // Hibernate Validation 验证数据
            String validationResult = adminCategoryService.validationCategory(validationCategory);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationEditData(validationCategory, categoryGetVOBefore, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 修改 文章分类
            boolean result = adminCategoryService.update(validationCategory);
            if (result) {
                // 异步博客模块文章分类缓存
                Future<String> asyncResult = adminControllerAsyncTask
                        .clearAllCache2Pattern(CacheNameConst.BLOG_CATEGORY_CACHE);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "修改文章分类，清空博客模块缓存");
            }
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加文章页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_ARTICLE_URL)
    @RequestMapping(ADD_ARTICLE + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_ADD_ARTICLE)
    @FormToken(init = true)
    public ModelAndView addArticle(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_ARTICLE_VIEW, ADD_ARTICLE_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminCategoryGetVO categoryGetVO = adminCategoryService.getById(idValue);
        if (null == categoryGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, categoryGetVO);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        this.getCategories2Tree();
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(SAVE_ARTICLE_URL_NAME, SAVE_ARTICLE_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加文章页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminCategoryGetVO categoryGetVO = adminCategoryService.getById(idValue);
        if (null == categoryGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, categoryGetVO);
        AdminCategoryGetVO pCategory = adminCategoryService.getById(categoryGetVO.getPid());
        modelAndView.addObject("pCategory", pCategory);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminCategoryGetVO categoryGetVO = adminCategoryService.getById(idValue);
            if (null == categoryGetVO) {
                return super.initErrorCheckJSONObject();
            }

            // 删除 文章分类
            boolean result = adminCategoryService.remove2StatusById(categoryGetVO.getId());
            if (result) {
                // 异步博客模块文章分类缓存
                Future<String> asyncResult = adminControllerAsyncTask
                        .clearAllCache2Pattern(CacheNameConst.BLOG_CATEGORY_CACHE);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "修改文章分类，清空博客模块缓存");
            }
            return super.initDeleteJSONObject(result);
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     *
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            // 异步博客模块文章分类缓存
            Future<String> asyncResult = adminControllerAsyncTask
                    .clearAllCache2Pattern(CacheNameConst.BLOG_CATEGORY_CACHE);
            adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "修改文章分类，清空博客模块缓存");
            return super.returnDeleteJSONObject(AdminCategoryService.class, adminCategoryService);
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_CATEGORY_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);


        AdminCategoryGetVO adminCategoryGetVO = new AdminCategoryGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminCategoryGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminCategoryGetVO.setState(StateEnum.DELETE.getKey());

        Page<AdminCategoryGetVO> pageInt = new Page<>();
        IPage<AdminCategoryGetVO> pageAdmin = adminCategoryService
                .list2page(super.initPage(pageInt), adminCategoryGetVO, AdminCategoryGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 所有分类类型
     *
     * @return Map
     */
    private Map<String, String> listCategoryType() {
        Map<String, String> map = new HashMap<>(16);
        for (CategoryTypeEnum categoryTypeEnum : CategoryTypeEnum.values()) {
            map.put(String.valueOf(categoryTypeEnum.getKey()), categoryTypeEnum.getValue());
        }
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 类型小于 3 文章分类集合
     */
    private void getCategoriesLt32Tree() {
        // 权限 tree 数据
        List<AdminCategoryGetVO> list = adminCategoryService.listTypeLt3();
        if (null != list) {
            list = AdminCategoryGetVO.list(list);
            super.request.setAttribute("treeCategories", list);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 文章分类集合
     */
    private void getCategories2Tree() {
        // 权限 tree 数据
        List<AdminCategoryGetVO> list = adminCategoryService.listType();
        if (null != list) {
            list = AdminCategoryGetVO.list(list);
            super.request.setAttribute("treeCategories", list);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminCategoryGetVO AdminCategoryGetVO   文章分类
     * @return AdminCategoryGetVO
     */
    private AdminCategoryGetVO initAddData(@NotNull final AdminCategoryGetVO adminCategoryGetVO) {
        AdminCategoryGetVO addCategoryGetVO = BeanMapperComponent.map(adminCategoryGetVO, AdminCategoryGetVO.class);
        // IP
        addCategoryGetVO.setGmtCreateIp(super.getIp());
        addCategoryGetVO.setState(StateEnum.NORMAL.getKey());
        addCategoryGetVO.setGmtCreate(Date.from(Instant.now()));
        return addCategoryGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editCategoryGetVO AdminCategoryGetVO  文章分类
    # @param categoryGetVO AdminCategoryGetVO  文章分类
     */
    private void initEditData(@NotNull final AdminCategoryGetVO editCategoryGetVO,
            @NotNull final AdminCategoryGetVO categoryGetVO) {
        editCategoryGetVO.setSole(categoryGetVO.getSole());
        editCategoryGetVO.setTitle(categoryGetVO.getTitle());
        editCategoryGetVO.setSort(categoryGetVO.getSort());
        editCategoryGetVO.setRemark(categoryGetVO.getRemark());
        editCategoryGetVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param categoryGetVO AdminCategoryGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminCategoryGetVO categoryGetVO,
            @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        AdminCategoryGetVO titleExist = adminCategoryService.getByTitle(categoryGetVO.getTitle());
        if (null != titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        AdminCategoryGetVO soleExist = adminCategoryService.getBySole(categoryGetVO.getSole());
        if (null != soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param categoryGetVO AdminCategoryGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationEditData(@NotNull final AdminCategoryGetVO newCategoryGetVO,
            @NotNull final AdminCategoryGetVO categoryGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = adminCategoryService
                .propertyUnique(AdminCategoryGetVO.TITLE, newCategoryGetVO.getTitle(), categoryGetVO.getTitle());
        if (!titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        boolean soleExist = adminCategoryService
                .propertyUnique(AdminCategoryGetVO.SOLE, newCategoryGetVO.getSole(), categoryGetVO.getSole());
        if (!soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryController class

/* End of file CategoryController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/cms/category/CategoryController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
