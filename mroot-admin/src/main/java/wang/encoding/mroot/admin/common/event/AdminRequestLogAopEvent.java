/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.event;


import org.springframework.context.ApplicationEvent;
import wang.encoding.mroot.bo.admin.entity.system.requestlog.AdminRequestLogBO;

import java.io.Serializable;

/**
 * 请求日志事件
 *
 * @author ErYang
 */
public class AdminRequestLogAopEvent extends ApplicationEvent implements Serializable {


    private static final long serialVersionUID = 5263098796109502709L;

    private AdminRequestLogBO adminRequestLogBO;


    public AdminRequestLogAopEvent(Object source) {
        super(source);
    }

    // -------------------------------------------------------------------------------------------------

    public AdminRequestLogAopEvent(Object source, AdminRequestLogBO adminRequestLogBO) {
        super(source);
        this.adminRequestLogBO = adminRequestLogBO;
    }

    // -------------------------------------------------------------------------------------------------

    public AdminRequestLogBO getAdminRequestLogBO() {
        return adminRequestLogBO;
    }

    public void setAdminRequestLogBO(AdminRequestLogBO adminRequestLogBO) {
        this.adminRequestLogBO = adminRequestLogBO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRequestLogAopEvent class

/* End of file AdminRequestLogAopEvent.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/event/AdminRequestLogAopEvent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
