/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.admin.common.interceptor.BaseInterceptor;
import wang.encoding.mroot.common.constant.ShareConst;
import wang.encoding.mroot.admin.common.interceptor.FormTokenInterceptor;
import wang.encoding.mroot.common.interceptor.SqlInjectInterceptor;

/**
 * 拦截器配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
public class AdminWebMvcConfigurer implements WebMvcConfigurer {

    private final ConfigConst configConst;
    private final ShareConst shareConst;

    @Autowired
    public AdminWebMvcConfigurer(ConfigConst configConst, ShareConst shareConst) {
        this.configConst = configConst;
        this.shareConst = shareConst;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 BaseInterceptor
     *
     * @return BaseInterceptor
     */
    public BaseInterceptor baseInterceptor() {
        return new BaseInterceptor(configConst);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 TokenInterceptor
     *
     * @return TokenInterceptor
     */
    public FormTokenInterceptor formTokenInterceptor() {
        return new FormTokenInterceptor(shareConst);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 SqlInjectInterceptor
     *
     * @return SqlInjectInterceptor
     */
    public SqlInjectInterceptor sqlInjectInterceptor() {
        return new SqlInjectInterceptor();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 拦截器
     *
     * @param registry InterceptorRegistry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 默认拦截器
        registry.addInterceptor(baseInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/assets/**", "/verify/image", "/language/**", "/do/login", "/**/save**",
                        "/**/update**", "/**/delete**", "/**recover**");

        // SqlInjectInterceptor 防止 sql 注入
        registry.addInterceptor(sqlInjectInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/**/favicon.ico", "/assets/**", "/", "/verify/image", "/language/**", "/login",
                        "/main", "/**/index", "/**/view/**", "/**/recyclebin");

        // FormTokenInterceptor 表单重复提交
        registry.addInterceptor(formTokenInterceptor())
                .addPathPatterns("/login", "/do/login", "/system/user/**", "/system/role/**", "/system/rule/**",
                        "/system/config/**", "/cms/category/**", "/cms/article/**", "/system/schedulejob/**",
                        "/system/mail/**")
                .excludePathPatterns("/", "/assets/**", "/verify/image", "/language/**", "/main", "/**/index",
                        "/**/view/**", "/**/recyclebin");

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminWebMvcConfigurer class

/* End of file AdminWebMvcConfigurer.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/config/AdminWebMvcConfigurer.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
