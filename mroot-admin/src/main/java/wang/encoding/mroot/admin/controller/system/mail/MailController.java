/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.mail;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.enums.*;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.time.ClockUtils;
import wang.encoding.mroot.plugin.mail.component.MailComponent;
import wang.encoding.mroot.plugin.mail.constant.MailConst;
import wang.encoding.mroot.plugin.mail.constant.MailTemplateConst;
import wang.encoding.mroot.service.admin.system.AdminMailLogService;
import wang.encoding.mroot.vo.admin.entity.system.maillog.AdminMailLogGetVO;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import java.time.Instant;
import java.util.Date;


/**
 * 后台 电子邮箱 控制器
 *
 *@author ErYang
 */
@Slf4j
@RestController
@RequestMapping("/system/mail")
public class MailController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/mail";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/mail";
    /**
     * 测试页面
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 测试
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;

    private final AdminMailLogService adminMailLogService;

    private final MailTemplateConst mailTemplateConst;

    private final MailConst mailConst;

    @Autowired
    public MailController(MailTemplateConst mailTemplateConst, MailConst mailConst,
            AdminMailLogService adminMailLogService) {
        this.mailTemplateConst = mailTemplateConst;
        this.mailConst = mailConst;
        this.adminMailLogService = adminMailLogService;
    }


    // -------------------------------------------------------------------------------------------------

    /**
     * 测试页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_MAIL_INDEX)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + ADD_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 测试发送邮件
     *
     * @return JSONObject
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_MAIL_SEND_TEST)
    @FormToken(remove = true)
    public JSONObject save() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            String email = super.request.getParameter("email");
            if (StringUtils.isBlank(email)) {
                return super.initErrorJSONObject(failResult, "validation.system.mail.email.pattern");
            }
            if (null == MailConst.openMail || !MailConst.openMail) {
                return super.initErrorJSONObject(failResult, "message.system.mail.status.error");
            }
            AdminMailLogGetVO addMailLogGetVO = new AdminMailLogGetVO();
            addMailLogGetVO.setToMail(email);

            // 发送邮件
            AdminUserGetVO adminUserGetVO = super.getCurrentAdmin();
            String content = String.format(mailTemplateConst.getTestContent(), adminUserGetVO.getUsername());
            addMailLogGetVO.setContent(content);
            long startTime = ClockUtils.currentTimeMillis();
            this.initAdminMailLogGetVOData(addMailLogGetVO);
            boolean result = MailComponent.sendSimpleEmail(email, mailTemplateConst.getTestTitle(), content);
            addMailLogGetVO.setTimes(Math.toIntExact(ClockUtils.elapsedTime(startTime)));
            if (result) {
                addMailLogGetVO.setSendResult(MailSendResultEnum.SEND_SUCCEED.getValue());
                adminMailLogService.save(addMailLogGetVO);
                return super.initSucceedJSONObject(ResultData.ok(), "message.system.mail.send.succeed");
            } else {
                addMailLogGetVO.setSendResult(MailSendResultEnum.SEND_FAILED.getValue());
                adminMailLogService.save(addMailLogGetVO);
                return super.initErrorJSONObject(failResult, "message.system.mail.send.failed");
            }
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 构建  AdminMailLogGetVO
     *
     * @param addMailLogGetVO AdminMailLogGetVO
     */
    private void initAdminMailLogGetVOData(@NotNull final AdminMailLogGetVO addMailLogGetVO) {
        addMailLogGetVO.setCategory(MailTypeEnum.TEST.getKey());
        addMailLogGetVO.setCategoryDescription(MailTypeEnum.TEST.getValue());
        addMailLogGetVO.setPattern(MailPatternEnum.SIMPLE.getKey());
        addMailLogGetVO.setPatternDescription(MailPatternEnum.SIMPLE.getValue());
        addMailLogGetVO.setFromMail(mailConst.getFromMailAddress());
        addMailLogGetVO.setTitle(mailTemplateConst.getTestTitle());
        addMailLogGetVO.setSendResult(MailSendResultEnum.SEND_ING.getValue());
        // IP
        addMailLogGetVO.setGmtCreateIp(super.getIp());
        addMailLogGetVO.setState(StateEnum.NORMAL.getKey());
        addMailLogGetVO.setGmtCreate(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MailController class

/* End of file MailController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/mail/MailController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
