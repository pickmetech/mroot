/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.redis.util.RedisUtils;

import java.io.Serializable;

/**
 * redis 实现共享 session
 * @author ErYang
 */
@Slf4j
@Component
public class RedisSessionDAO extends EnterpriseCacheSessionDAO {

    /**
     * session 在 redis 过期时间是 30分钟 30*60
     */
    private static final int EXPIRE_TIME = 1800;
    /**
     * 缓存 前缀
     */
    private static final String PREFIX = CacheNameConst.ADMIN_SHIRO_SESSION_CACHE + CacheNameConst.REDIS_CACHE_SEPARATOR;

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建  session 保存到数据库
     * @param session Session
     * @return Serializable
     */
    @Override
    protected Serializable doCreate(Session session) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>新增Shiro Session[{}]<<<<<<<<", session.getId());
        }
        Serializable sessionId = super.doCreate(session);
        RedisUtils.setString(PREFIX + sessionId.toString(), SerializeUtils.serialize(session));
        return sessionId;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取 session
     * @param sessionId Serializable
     * @return Session
     */
    @Override
    protected Session doReadSession(Serializable sessionId) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>读取Shiro Session[{}]<<<<<<<<", sessionId);
        }
        // 先从缓存中获取 session 如果没有再去 redis 数据库中获取
        Session session = super.doReadSession(sessionId);
        if (null == session) {
            session = (Session) SerializeUtils
                    .deserialize((byte[]) RedisUtils.getString(PREFIX + sessionId.toString()));
        }
        return session;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新  session
     * @param session Session
     */
    @Override
    protected void doUpdate(Session session) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>更新Shiro Session[{}]<<<<<<<<", session.getId());
        }
        super.doUpdate(session);
        String key = PREFIX + session.getId().toString();
        RedisUtils.setString(key, SerializeUtils.serialize(session));
        // 更新 session 的过期时间
        RedisUtils.expire(key, EXPIRE_TIME);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 session
     * @param session Session
     */
    @Override
    protected void doDelete(Session session) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>删除Shiro Session[{}]<<<<<<<<", session.getId());
        }
        super.doDelete(session);
        RedisUtils.delete(PREFIX + session.getId().toString());
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RedisSessionDAO class

/* End of file RedisSessionDAO.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/RedisSessionDAO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
