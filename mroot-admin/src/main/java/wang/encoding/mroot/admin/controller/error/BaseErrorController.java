/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.error;


import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.util.HttpRequestUtils;

/**
 * 错误控制器
 *
 * @author ErYang
 */
@RestController
public class BaseErrorController extends AdminBaseController implements ErrorController {


    /**
     * 错误页面地址
     */
    private static final String ERROR_URL = "/error";
    /**
     * 404错误页面地址
     */
    private static final String ERROR404_URL = "/error/404";
    /**
     * 错误视图
     */
    private static final String ERROR_EXCEPTION_VIEW = "/error/404";
    /**
     * 无权限页面地址
     */
    private static final String ERROR403_URL = "/error/403";
    /**
     * 无权限页面视图
     */
    private static final String ERROR403_EXCEPTION_VIEW = "/error/403";


    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = ERROR404_URL)
    public ModelAndView error404() {
        ModelAndView modelAndView = new ModelAndView(super.initView(ERROR_EXCEPTION_VIEW));
        String url = super.request.getHeader("Referer");
        if (null != url) {
            modelAndView.addObject("refererUrl", url);
        }
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = ERROR_URL)
    public ModelAndView handleError404() {
        ModelAndView modelAndView = new ModelAndView(super.initView(ERROR_EXCEPTION_VIEW));
        String url = super.request.getHeader("Referer");
        if (null != url) {
            modelAndView.addObject("refererUrl", url);
        }
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 无权限错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = ERROR403_URL)
    public Object handleError403() {
        if (HttpRequestUtils.isAjaxRequest(super.request)) {
            ResultData result = ResultData.error();
            result.set(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage("message.exception403.content"));
            return result.toFastJson();
        } else {
            ModelAndView modelAndView = new ModelAndView(super.initView(ERROR403_EXCEPTION_VIEW));
            String url = super.request.getHeader("Referer");
            if (null != url) {
                modelAndView.addObject("refererUrl", url);
            }
            return modelAndView;
        }
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public String getErrorPath() {
        return super.getCurrentThemePath() + ERROR_EXCEPTION_VIEW;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseErrorController class

/* End of file BaseErrorController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/error/BaseErrorController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
