/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.kaptcha.Kaptcha;
import com.baomidou.kaptcha.exception.KaptchaIncorrectException;
import com.baomidou.kaptcha.exception.KaptchaNotFoundException;
import com.baomidou.kaptcha.exception.KaptchaTimeoutException;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.admin.common.constant.DynamicData;
import wang.encoding.mroot.admin.common.constant.ResourceConst;
import wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask;
import wang.encoding.mroot.admin.common.task.AdminControllerAsyncTaskResultUtils;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.constant.DatabaseConst;
import wang.encoding.mroot.common.controller.BaseController;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.id.IdUtils;
import wang.encoding.mroot.common.util.number.NumberUtils;
import wang.encoding.mroot.common.util.reflect.ReflectionUtils;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;
import wang.encoding.mroot.vo.admin.enums.RuleTypeEnum;


import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.List;

/**
 * 后台基类控制器
 *
 * @author ErYang
 */
public class AdminBaseController extends BaseController {


    /**
     * 错误页面地址
     */
    private static final String ERROR404_URL = "/error/404";
    /**
     * html br 标签
     */
    private static final String HTML_BR = "<br>";
    /**
     * stateStr 状态参数名称
     */
    private static final String STATE_STR_PARAMETER_NAME = "stateStr";
    /**
     * ID[] 集合参数名称
     */
    private static final String ID_ARRAY_PARAMETER_NAME = "id[]";
    /**
     * 页面 分页 名称
     */
    protected static final String VIEW_PAGE_NAME = "page";
    /**
     *  ID 标识名称
     */
    protected static final String ID_NAME = "id";
    /**
     * 错误提示语 名称标识
     */
    private static final String MESSAGE_ERROR_NAME = "message.info.error";
    /**
     * 标识存在 名称标识
     */
    protected static final String MESSAGE_SOLE_EXIST_NAME = "message.sole.exist";
    /**
     * 标题存在 名称标识
     */
    protected static final String MESSAGE_TITLE_EXIST_NAME = "message.title.exist";
    /**
     * url存在 名称标识
     */
    protected static final String MESSAGE_URL_EXIST_NAME = "message.url.exist";
    /**
     * 添加成功提示语 名称标识
     */
    private static final String MESSAGE_ADD_INFO_SUCCEED_NAME = "message.add.info.succeed";
    /**
     * 添加失败提示语 名称标识
     */
    private static final String MESSAGE_ADD_INFO_FAIL_NAME = "message.add.info.fail";
    /**
     * 编辑成功提示语 名称标识
     */
    private static final String MESSAGE_EDIT_INFO_SUCCEED_NAME = "message.edit.info.succeed";
    /**
     * 编辑失败提示语 名称标识
     */
    private static final String MESSAGE_EDIT_INFO_FAIL_NAME = "message.edit.info.fail";
    /**
     * 删除成功提示语 名称标识
     */
    private static final String MESSAGE_DELETE_INFO_SUCCEED_NAME = "message.delete.info.succeed";
    /**
     * 删除失败提示语 名称标识
     */
    private static final String MESSAGE_DELETE_INFO_FAIL_NAME = "message.delete.info.fail";
    /**
     * 清空成功提示语 名称标识
     */
    private static final String MESSAGE_TRUNCATE_INFO_SUCCEED_NAME = "message.truncate.info.succeed";
    /**
     * 清空失败提示语 名称标识
     */
    private static final String MESSAGE_TRUNCATE_INFO_FAIL_NAME = "message.truncate.info.fail";
    /**
     * 默认视图目录
     */
    private static final String DEFAULT_VIEW = "/default";
    /**
     * elite 视图目录
     */
    private static final String ELITE_VIEW = "/elite";
    /**
     * 重定向标识
     */
    private static final String REDIRECT_NAME = "redirect:";
    /**
     * 上个请求变量名称
     */
    private static final String REFERER_URL_NAME = "refererUrl";
    /**
     * 面包屑菜单变量
     *
     */
    private static final String BREAD_CRUMB_NAME = "breadcrumb";
    private static final String BREAD_CRUMB_URL_NAME = "breadcrumbUrl";
    private static final String BREAD_CRUMB_MENU_NAME = "breadcrumbMenu";
    private static final String BREAD_CRUMB_CHILE_NAME = "breadcrumbChile";
    private static final String BREAD_CRUMB_TITLE_NAME = "breadcrumbTitle";
    private static final String HEAD_TITLE_NAME = "headTitle";
    /**
     * 首页
     */
    protected static final String INDEX_NAME = "index";
    /**
     * 添加
     */
    protected static final String ADD_NAME = "add";
    protected static final String SAVE_NAME = "save";
    /**
     * 添加子类
     */
    protected static final String ADD_CHILDREN_NAME = "addChildren";
    /**
     * 编辑
     */
    protected static final String EDIT_NAME = "edit";
    protected static final String UPDATE_NAME = "update";
    /**
     * 查看
     */
    protected static final String VIEW_NAME = "view";
    /**
     * 批量删除
     */
    protected static final String DELETE_NAME = "delete";
    /**
     * 批量删除
     */
    protected static final String DELETE_BATCH_NAME = "deleteBatch";
    /**
     * 回收站
     */
    protected static final String RECYCLE_BIN_NAME = "recycleBin";
    /**
     * nav 地址
     */
    protected static final String NAV_INDEX_URL_NAME = "navIndex";
    /**
     * 授权
     */
    protected static final String AUTHORIZATION_NAME = "authorization";
    /**
     * 保存授权
     */
    protected static final String AUTHORIZATION_SAVE_NAME = "authorizationSave";
    /**
     * 文章封面图片的上传地址
     */
    protected static final String ARTICLE_COVER_UPLOAD_PATH_URL = "/img/article/cover";
    /**
     * model 名称
     */
    protected static final String MODULE = "model";
    /**
     * 首页
     */
    private static final String INDEX_URL = "/index";
    /**
     * 保存
     */
    private static final String SAVE_URL_NAME = "save";
    /**
     * 更新
     */
    private static final String UPDATE_URL_NAME = "update";
    /**
     * 删除
     */
    private static final String DELETE_URL_NAME = "delete";
    /**
     * 新增
     */
    private static final String ADD_URL_NAME = "add";
    /**
     * 编辑
     */
    private static final String EDIT_URL_NAME = "edit";
    /**
     * 查看
     */
    private static final String VIEW_URL_NAME = "view";

    /**
     *  最大排序 标识名称
     */
    protected static final String MAX_SORT_NAME = "maxSort";

    @Autowired
    protected ConfigConst configConst;

    @Value(value = "${server.servlet.contextPath}")
    protected String contextPath;
    @Value(value = "${upload.resources-path}")
    protected String uploadResourcesPath;
    @Autowired
    private AdminRuleService adminRuleService;
    @Autowired
    protected AdminControllerAsyncTask adminControllerAsyncTask;
    @Autowired
    protected DatabaseConst databaseConst;
    @Autowired
    protected ApplicationContext applicationContext;
    @Autowired
    protected AdminControllerAsyncTaskResultUtils adminControllerAsyncTaskResultUtils;
    @Autowired
    protected ResourceConst resourceConst;
    @Autowired
    protected Kaptcha kaptcha;

    /**
     * 得到当前管理员
     *
     * @return AdminUserGetVO
     */
    protected AdminUserGetVO getCurrentAdmin() {
        return (AdminUserGetVO) ShiroSessionUtils.getAttribute(configConst.getAdminSessionName());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到当前模板路径
     * @return String
     */
    protected String getCurrentThemePath() {
        // 默认主题
        if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.DEFAULT_THEME)) {
            return DEFAULT_VIEW;
        } else if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.ELITE_THEME)) {
            return ELITE_VIEW;
        } else {
            return ELITE_VIEW;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建视图
     *
     * @param view String 视图地址
     * @return String 视图地址
     */
    protected String initView(@NotNull final String view) {
        return this.getCurrentThemePath() + view;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建 url 地址
     *
     * @param url String 地址
     * @return String  地址
     */
    protected String initUrl(@NotNull final String url) {
        return contextPath + "/" + url;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建错误重定向地址
     *
     * @return ModelAndView 重定向地址
     */
    protected ModelAndView initErrorRedirectUrl() {
        return new ModelAndView(REDIRECT_NAME + ERROR404_URL);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 检验图片验证码
     * @param clientCode String 客户端验证码
     * @return boolean
     */
    protected boolean checkKaptcha(@NotNull final String clientCode) {
        try {
            return kaptcha.validate(clientCode);
        } catch (Exception e) {
            if (e instanceof KaptchaIncorrectException) {
                //return "验证码不正确";
                return false;
            } else if (e instanceof KaptchaNotFoundException) {
                //return "验证码未找到";
                return false;
            } else if (e instanceof KaptchaTimeoutException) {
                //return "验证码过期";
                return false;
            } else {
                //return "验证码渲染失败";
                return false;
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据模块创建模块重定向地址
     * 不包括 contextPath
     *
     * @param url String url
     * @return String 重定向地址
     */
    protected String initRedirectUrl(@NotNull final String url) {
        return REDIRECT_NAME + url;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 设置分页
     *
     * @param page Page
     * @return Page
     */
    protected <T> Page<T> initPage(final Page<T> page) {
        long index = 1L;
        long size;
        // 得到数据库配置的分页条数
        AdminConfigGetVO configVO = DynamicData.getConfigBySole(databaseConst.getAdminPageSizeName());
        if (null == configVO || StringUtils.isBlank(configVO.getContent()) || 0 >= NumberUtils
                .toLong(configVO.getContent())) {
            size = NumberUtils.toLong(configConst.getAdminPageSize());
        } else {
            size = NumberUtils.toLong(configVO.getContent());
        }
        String pageNumber = super.request.getParameter("p");
        if (pageNumber != null) {
            if (StringUtils.isNotBlank(pageNumber)) {
                index = NumberUtils.toLong(pageNumber);
            }
        }
        page.setSize(size);
        page.setCurrent(index);
        return page;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据请求设置上个请求地址
     * @param module String
     */
    protected void initRefererUrl(final String module) {
        String url = super.request.getHeader("Referer");
        String currentUrl = super.request.getRequestURI();
        if (StringUtils.isNotBlank(url)) {
            // 如果不是登录 保存 更新 删除 地址 就设置
            String urlOld = url;
            url = url.toLowerCase();
            if (StringUtils.isNotBlank(currentUrl)) {
                if (!url.contains(configConst.getAdminLoginUrlName()) && !url.contains(SAVE_URL_NAME) && !url
                        .contains(UPDATE_URL_NAME) && !url.contains(DELETE_URL_NAME) && !url.contains(ADD_URL_NAME)
                        && !url.contains(EDIT_URL_NAME) && !url.contains(VIEW_URL_NAME)) {
                    super.request.setAttribute(REFERER_URL_NAME, urlOld);
                } else {
                    super.request.setAttribute(REFERER_URL_NAME, contextPath + module + INDEX_URL);
                }
            } else {
                super.request.setAttribute(REFERER_URL_NAME, contextPath + INDEX_URL);
            }
        } else {
            super.request.setAttribute(REFERER_URL_NAME, contextPath + INDEX_URL);
        }
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建失败 Hibernate 验证信息 JSONObject 带 token
     *
     * @param failResult       ResultData
     * @param validationResult       String
     *
     * @return JSONObject
     */
    protected JSONObject initErrorHibernateValidationJSONObject(@NotNull final ResultData failResult,
            @NotNull final String validationResult) {
        failResult.set(GlobalMessage.MESSAGE, this.initResult2HibernateValidation(validationResult));
        failResult.set(configConst.getValidationMessageName(), configConst.getValidationMessageName());
        // 生成 token 并放入 session 中
        this.initJSONObjectTokenValue(failResult);
        return failResult.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建失败验证信息 JSONObject 带 token
     *
     * @param failResult       ResultData
     *
     * @return JSONObject
     */
    protected JSONObject initErrorValidationJSONObject(@NotNull final ResultData failResult) {
        // 生成 token 并放入 session 中
        this.initJSONObjectTokenValue(failResult);
        return failResult.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建保存信息 JSONObject
     *
     * @param result boolean
     *
     * @return JSONObject
     */
    protected JSONObject initSaveJSONObject(boolean result) {
        return this.initJSONObject(result, MESSAGE_ADD_INFO_SUCCEED_NAME, MESSAGE_ADD_INFO_FAIL_NAME, true);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 获取 ID 值 判断是不是数值 空的话就不是数值
     *
     * @param id       String
     * @return BigInteger
     */
    protected BigInteger getId(@NotNull final String id) {
        if (NumberUtils.isNumber(id)) {
            return new BigInteger(id);
        }
        return null;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建 查询信息 ModelAndView
     *
     * @param modelAndView       ModelAndView
     * @param redirectAttributes       RedirectAttributes
     * @param url       String
     * @return ModelAndView 跳转到页面
     */
    protected ModelAndView initValidationResult2Get(@NotNull final ModelAndView modelAndView,
            @NotNull final RedirectAttributes redirectAttributes, @NotNull final String url) {
        this.initResult2Validation(redirectAttributes, LocaleMessageSourceComponent.getMessage(MESSAGE_ERROR_NAME));
        modelAndView.setViewName(this.initRedirectUrl(url));
        return modelAndView;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建 查询信息 ModelAndView
     *
     * @param modelAndView       ModelAndView
     * @param redirectAttributes       RedirectAttributes
     * @param url       String
     * @param message       String
     * @return ModelAndView 跳转到页面
     */
    protected ModelAndView initValidationResult2Get(@NotNull final ModelAndView modelAndView,
            @NotNull final RedirectAttributes redirectAttributes, @NotNull final String message,
            @NotNull final String url) {
        this.initResult2Validation(redirectAttributes, LocaleMessageSourceComponent.getMessage(message));
        modelAndView.setViewName(this.initRedirectUrl(url));
        return modelAndView;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 添加失败 Flash 提示信息
     * 使用 addFlashAttribute 参数不会出现在url地址栏中
     *
     * 页面通过 ${failMessage} 获取
     *
     * @param messages messages 信息
     */
    protected void addFailMessage(@NotNull final RedirectAttributes redirectAttributes,
            @NotNull final String... messages) {
        StringBuilder sb = new StringBuilder();
        for (String message : messages) {
            sb.append(message);
            if (1 < messages.length) {
                sb.append(HTML_BR);
            }
        }
        redirectAttributes.addFlashAttribute(configConst.getFailMessageName(), sb.toString());
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     *  设置  ModelAndView
     * @param view String 视图
     * @param url String 地址
     * @return ModelAndView
     */
    protected ModelAndView initModelAndView(@NotNull final String view, @NotNull final String url) {
        ModelAndView modelAndView = new ModelAndView(this.initView(view));
        this.initViewTitle(url);
        return modelAndView;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 返回 删除提示
     *
     * @param clazz Class
     * @param object Object
     * @return JSONObject
     */
    protected JSONObject returnDeleteJSONObject(@NotNull final Class<?> clazz, @NotNull final Object object) {
        BigInteger[] idArray = this.getIdArray();
        // 验证数据
        if (null == idArray || ArrayUtils.isEmpty(idArray)) {
            return this.initErrorCheckJSONObject();
        }
        Class<?> idClass = idArray.getClass();
        Method method = ReflectionUtils.getMethod(clazz, "removeBatch2UpdateStatus", idClass);
        boolean flag = ReflectionUtils.invokeMethod(object, method, (Object) idArray);
        if (flag) {
            return this.initDeleteJSONObject(true);
        } else {
            return this.initDeleteJSONObject(false);
        }
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 获取 ID 值 判断是不是数值 空的话就不是数值
     *
     * @return id BigInteger
     */
    protected BigInteger getId() {
        String id = super.request.getParameter("id");
        return this.getId(id);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建失败检查信息 JSONObject
     *
     * @return JSONObject
     */
    protected JSONObject initErrorCheckJSONObject() {
        ResultData resultData = ResultData
                .fail(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage(MESSAGE_ERROR_NAME));
        return resultData.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建删除信息 JSONObject
     *
     * @param result boolean
     *
     * @return JSONObject
     */
    protected JSONObject initDeleteJSONObject(boolean result) {
        return this.initJSONObject(result, MESSAGE_DELETE_INFO_SUCCEED_NAME, MESSAGE_DELETE_INFO_FAIL_NAME, false);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建清空信息 JSONObject
     *
     * @return JSONObject
     */
    protected JSONObject initTruncateJSONObject() {
        return this.initJSONObject(true, MESSAGE_TRUNCATE_INFO_SUCCEED_NAME, MESSAGE_TRUNCATE_INFO_FAIL_NAME, false);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建 返回错误信息 JSONObject
     *
     * @return JSONObject
     */
    protected JSONObject initReturnErrorJSONObject() {
        ResultData resultData = ResultData
                .fail(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage(MESSAGE_ERROR_NAME));
        return resultData.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建失败检查信息 JSONObject 带  token
     *
     * @param failResult       ResultData
     *
     * @return JSONObject
     */
    protected JSONObject initErrorCheckJSONObject(@NotNull final ResultData failResult) {
        return this.initErrorJSONObject(failResult, MESSAGE_ERROR_NAME);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建失败提示信息 JSONObject 带 token
     *
     * @param failResult       ResultData
     * @param message       String i18n 名称
     *
     * @return JSONObject
     */
    protected JSONObject initErrorJSONObject(@NotNull final ResultData failResult, String message) {
        failResult.set(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage(message));
        // 生成 token 并放入 session 中
        this.initJSONObjectTokenValue(failResult);
        return failResult.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建成功提示信息 JSONObject 带 token
     *
     * @param succeedResult       ResultData
     * @param message       String i18n 名称
     *
     * @return JSONObject
     */
    protected JSONObject initSucceedJSONObject(@NotNull final ResultData succeedResult, String message) {
        succeedResult.set(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage(message));
        // 生成 token 并放入 session 中
        this.initJSONObjectTokenValue(succeedResult);
        return succeedResult.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建更新信息 JSONObject
     *
     * @param result boolean
     *
     * @return JSONObject
     */
    protected JSONObject initUpdateJSONObject(boolean result) {
        return this.initJSONObject(result, MESSAGE_EDIT_INFO_SUCCEED_NAME, MESSAGE_EDIT_INFO_FAIL_NAME, true);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 获取状态值 bootstrap-switch 插件的启用状态标识
     *
     * @return stateStr String
     */
    protected String getStatusStr() {
        return super.request.getParameter(STATE_STR_PARAMETER_NAME);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 得到 id[] 集合
     *
     * @return BigInteger[]
     */
    protected BigInteger[] getIdArray() {
        String[] idArray = super.request.getParameterValues(ID_ARRAY_PARAMETER_NAME);
        if (null == idArray || ArrayUtils.isEmpty(idArray)) {
            return null;
        }
        // 转为 BigInteger数组
        return wang.encoding.mroot.common.util.collection.ArrayUtils.string2BigInteger(idArray);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 添加失败 Flash 集合提示信息
     *
     * 使用 addFlashAttribute 参数不会出现在url地址栏中
     *
     *页面通过 ${failListMessage} 获取
     *
     * @param linkedList LinkedList<String> 信息 不是 i18n信息
     */
    private void addFailMessage(@NotNull final RedirectAttributes redirectAttributes,
            @NotNull final List<String> linkedList) {
        redirectAttributes.addFlashAttribute(configConst.getFailMessageListName(), linkedList);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建 Validation 验证信息
     *
     * @param redirectAttributes       RedirectAttributes
     * @param result       String 验证信息 i18n 名称
     */
    private void initResult2Validation(@NotNull final RedirectAttributes redirectAttributes,
            @NotNull final String result) {
        String[] msgArray = this.convertStrToArray(result);
        if (ArrayUtils.isNotEmpty(msgArray)) {
            List<String> list = wang.encoding.mroot.common.util.collection.ArrayUtils.asList(msgArray);
            this.addFailMessage(redirectAttributes, list);
        } else {
            this.addFailMessage(redirectAttributes, LocaleMessageSourceComponent.getMessage(result));
        }
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建信息 JSONObject 带 token
     *
     * @param result boolean
     * @param succeedMessage String 成功提示语 i18n 名称
     * @param failMessage String 失败提示语 i18n 名称
     * @param isToken boolean 是否生成 token
     *
     * @return JSONObject
     */
    protected JSONObject initJSONObject(boolean result, @NotNull final String succeedMessage,
            @NotNull final String failMessage, boolean isToken) {
        String message;
        ResultData resultData;
        // 成功
        if (result) {
            message = succeedMessage;
            resultData = ResultData.ok(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage(message));
        } else {
            // 失败
            message = failMessage;
            resultData = ResultData.fail(GlobalMessage.MESSAGE, LocaleMessageSourceComponent.getMessage(message));
        }
        // 生成 token 并放入 session 中
        if (isToken) {
            this.initJSONObjectTokenValue(resultData);
        }
        return resultData.toFastJson();
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 添加 Hibernate Validation 提示信息
     *
     * @param  result String 信息
     *
     * @return String
     */
    private String initResult2HibernateValidation(@NotNull final String result) {
        String[] msgArray = this.convertStrToArray(result);
        String message;
        if (ArrayUtils.isNotEmpty(msgArray)) {
            StringBuilder sb = new StringBuilder();
            for (String msg : msgArray) {
                sb.append(LocaleMessageSourceComponent.getMessage(msg));
                if (1 < msgArray.length) {
                    sb.append(HTML_BR);
                }
            }
            message = LocaleMessageSourceComponent.getMessage(sb.toString());
        } else {
            message = LocaleMessageSourceComponent.getMessage(result);
        }
        return message;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 分割验证信息字符串
     *
     * @param source String 字符串
     * @return String[]  分割后的数组
     */
    private String[] convertStrToArray(@NotNull final String source) {
        return source.split(HibernateValidationUtils.CONNECT_CHAR);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 url 地址  设置页面标题
     *
     * @param url String 地址
     */
    private void initViewTitle(@NotNull final String url) {
        AdminRuleGetVO ruleBO = adminRuleService.getByUrl(url);
        if (null != ruleBO) {
            if (RuleTypeEnum.BUTTON.getKey() == ruleBO.getCategory()) {

                AdminRuleGetVO chileRuleBO = adminRuleService.getById(ruleBO.getPid());
                super.request.setAttribute(BREAD_CRUMB_CHILE_NAME, chileRuleBO);

                AdminRuleGetVO menuRuleBO = adminRuleService.getById(chileRuleBO.getPid());
                super.request.setAttribute(BREAD_CRUMB_MENU_NAME, menuRuleBO);

                AdminRuleGetVO urlRuleDO = adminRuleService.getById(menuRuleBO.getPid());
                super.request.setAttribute(BREAD_CRUMB_URL_NAME, urlRuleDO);

                super.request.setAttribute(BREAD_CRUMB_NAME, chileRuleBO);
            } else {
                if (RuleTypeEnum.CHILD_MENU.getKey() == ruleBO.getCategory()) {
                    AdminRuleGetVO menuRuleBO = adminRuleService.getById(ruleBO.getPid());
                    super.request.setAttribute(BREAD_CRUMB_MENU_NAME, menuRuleBO);

                    AdminRuleGetVO urlRuleBO = adminRuleService.getById(menuRuleBO.getPid());
                    super.request.setAttribute(BREAD_CRUMB_URL_NAME, urlRuleBO);
                }
                if (RuleTypeEnum.MENU.getKey() == ruleBO.getCategory()) {
                    AdminRuleGetVO menuRuleBO = adminRuleService.getById(ruleBO.getPid());
                    super.request.setAttribute(BREAD_CRUMB_MENU_NAME, menuRuleBO);

                    AdminRuleGetVO urlRuleBO = adminRuleService.getById(menuRuleBO.getPid());
                    super.request.setAttribute(BREAD_CRUMB_URL_NAME, urlRuleBO);
                }
                if (RuleTypeEnum.URL.getKey() == ruleBO.getCategory()) {
                    request.setAttribute(BREAD_CRUMB_MENU_NAME, ruleBO);
                }
            }
            super.request.setAttribute(BREAD_CRUMB_TITLE_NAME, ruleBO.getTitle());
            super.request.setAttribute(HEAD_TITLE_NAME, ruleBO.getTitle());
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 生成 token 并放入 session 中
     *
     * @param result ResultData
     */
    protected void initJSONObjectTokenValue(@NotNull final ResultData result) {
        // 生成 token 并放入 session 中
        String tokenValue = this.initTokenValue();
        result.set(GlobalMessage.FORM_TOKEN, tokenValue);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 生成 token 并放入 session 中
     *
     */
    private String initTokenValue() {
        // 新的 token
        String tokenValue = IdUtils.fastUUID().toString();
        ShiroSessionUtils.setAttribute(GlobalMessage.FORM_TOKEN, tokenValue);
        return tokenValue;
    }

    // -----------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseAdminController class

/* End of file BaseAdminController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/controller/BaseAdminController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
