/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.filter.DelegatingFilterProxy;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.service.admin.system.AdminUserService;


import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * shiro 配置
 *
 * @author ErYang
 */
@Slf4j
@Configuration
@EnableCaching
public class ShiroConfiguration {


    /**
     * 过滤器
     */
    private static final Map<String, Filter> FILTERS = new LinkedHashMap<>();

    @Value("${server.servlet.contextPath}")
    private String contextPath;

    private final AdminUserService adminUserService;
    private final AdminRoleService adminRoleService;
    private final AdminRuleService adminRuleService;
    private final ConfigConst configConst;

    private final RedisSessionDAO redisSessionDAO;

    @Autowired
    @Lazy
    public ShiroConfiguration(AdminUserService adminUserService, AdminRoleService adminRoleService,
            AdminRuleService adminRuleService, ConfigConst configConst, RedisSessionDAO redisSessionDAO) {
        this.adminUserService = adminUserService;
        this.adminRoleService = adminRoleService;
        this.adminRuleService = adminRuleService;
        this.configConst = configConst;
        this.redisSessionDAO = redisSessionDAO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 密码凭证
     *
     * @return CredentialsMatcher
     */
    @Bean(name = "credentialsMatcher")
    public CredentialsMatcher credentialsMatcher() {
        return new MyCredentialsMatcher();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 自定义权限匹配器
     * @return UrlPermissionResolver
     */
    @Bean(name = "urlPermissionResolver")
    public UrlPermissionResolver urlPermissionResolver() {
        return new UrlPermissionResolver();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 配置自定义的权限登录器
     */
    @Bean(name = "shiroDbRealm")
    public ShiroDbRealm shiroDbRealm() {
        ShiroDbRealm shiroDbRealm = new ShiroDbRealm();
        shiroDbRealm.setCredentialsMatcher(this.credentialsMatcher());
        shiroDbRealm.setPermissionResolver(urlPermissionResolver());
        return shiroDbRealm;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * session管理器
     *
     * @return SessionManager
     */
    @Bean(name = "sessionManager")
    public SessionManager sessionManager() {
        DefaultWebSessionManager sessionManager = new ShiroSessionManager();
        // 单位 毫秒  30分钟
        sessionManager.setGlobalSessionTimeout(30 * 60 * 1000);
        sessionManager.setDeleteInvalidSessions(true);
        sessionManager.setSessionValidationSchedulerEnabled(true);
        sessionManager.setDeleteInvalidSessions(true);
        // 取消 url 后面的 JSESSIONID
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        // 使用 redis session
        sessionManager.setSessionDAO(redisSessionDAO);
        return sessionManager;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 配置核心安全事务管理器
     *
     * @return SecurityManager
     */
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRealm(this.shiroDbRealm());
        manager.setSessionManager(this.sessionManager());
        return manager;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 开启 shiro aop 注解支持
     * 使用代理方式 所以需要开启代码支持
     * Controller才能使用@RequiresPermissions
     *
     * @return AuthorizationAttributeSourceAdvisor
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(this.securityManager());
        return authorizationAttributeSourceAdvisor;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 开启 shiro aop 注解支持
     * 使用代理方式 所以需要开启代码支持
     * Controller才能使用@RequiresPermissions
     *
     * @return DefaultAdvisorAutoProxyCreator
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 支持 web Filter 排序的使用
     *
     * @return FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean<DelegatingFilterProxy> filterRegistration = new FilterRegistrationBean<>();
        DelegatingFilterProxy proxy = new DelegatingFilterProxy();
        // 该值缺省为 false 表示生命周期由 SpringApplicationContext 管理
        // 设置为 true 则表示由 ServletContainer 管理
        proxy.setTargetFilterLifecycle(false);
        proxy.setTargetBeanName("shiroFilter");
        filterRegistration.setFilter(proxy);
        filterRegistration.setEnabled(true);
        // 可以自己灵活的定义很多 避免一些根本不需要被 shiro 处理的请求被包含进来
        filterRegistration.addUrlPatterns("/*");
        //        filterRegistration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE,
        //                DispatcherType.ERROR);
        return filterRegistration;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ShiroFilterFactoryBean 处理拦截资源文件问题
     * 注意 单独一个 ShiroFilterFactoryBean 配置是或报错的
     * 因为在初始化 ShiroFilterFactoryBean 的时候需要注入 SecurityManager
     * Filter Chain 定义说明
     * 1、一个URL可以配置多个 Filter 使用逗号分隔
     * 2、当设置多个过滤器时 全部验证通过 才视为通过
     * 3、部分过滤器可指定参数 如 perms roles
     */
    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean shiroFilter() {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(this.securityManager());
        // 登录地址
        shiroFilterFactoryBean.setLoginUrl(contextPath + "/login");

        // 拦截器
        FILTERS.put("myShiroFilter",
                new MyShiroFilter(configConst, adminUserService, adminRuleService, adminRoleService));

        shiroFilterFactoryBean.setFilters(FILTERS);
        return shiroFilterFactoryBean;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ShiroConfiguration class

/* End of file ShiroConfiguration.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/ShiroConfiguration.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
