/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.bo.admin.entity.system.rule.AdminRuleBO;
import wang.encoding.mroot.bo.admin.entity.system.user.AdminUserBO;
import wang.encoding.mroot.common.component.DigestManageComponent;
import wang.encoding.mroot.common.util.ObjectUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.service.admin.system.AdminUserService;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

/**
 * 为当前登录的 Subject 授予角色和权限
 *
 * @author ErYang
 */
@Slf4j
public class ShiroDbRealm extends AbstractAuthorizingRealmWithRpn {

    @Autowired
    @Lazy
    private AdminUserService adminUserService;
    @Autowired
    @Lazy
    private AdminRoleService adminRoleService;
    @Autowired
    @Lazy
    private AdminRuleService adminRuleService;
    @Autowired
    private ConfigConst configConst;

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证当前登录的 Subject 登录时调用
     *
     * @param authenticationToken AuthenticationToken
     * @return AuthenticationInfo
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
            throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>验证当前Subject时获取到usernamePasswordToken为:{}<<<<<<<<",
                    ReflectionToStringBuilder.toString(usernamePasswordToken, ToStringStyle.MULTI_LINE_STYLE));
        }

        String username = usernamePasswordToken.getUsername();
        String password = DigestManageComponent.getSha512(String.valueOf(usernamePasswordToken.getPassword()));

        AdminUserBO adminUserBO = adminUserService.getByUsernameAndPassword(username, password);

        if (null != adminUserBO) {
            // 登录信息放入 session 中
            AdminUserGetVO userVO = adminUserService.getById(adminUserBO.getId());
            ShiroSessionUtils.setAttribute(configConst.getAdminSessionName(), userVO);
            ShiroSessionUtils.setAttribute(configConst.getAdminSessionIdName(), ShiroSessionUtils.getSessionId());
            return new SimpleAuthenticationInfo(adminUserBO.getUsername(), adminUserBO.getPassword(),
                    adminUserBO.getUsername());
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 为当前登录的 Subject 授予角色和权限
     * 1、subject.hasRole(“admin”) 或 subject.isPermitted(“admin”)：
     *  自己去调用这个是否有什么角色或者是否有什么权限的时候；
     * 2、@RequiresRoles("admin") ：在方法上加注解的时候；
     * 3、[@shiro.hasPermission name = "admin"][/@shiro.hasPermission]：
     * 在页面上加shiro标签的时候，即进这个页面的时候扫描到有这个标签的时候
     *
     * @param principalCollection principalCollection
     * @return AuthorizationInfo
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 非正常退出，即没有显式调用 SecurityUtils.getSubject().logout()
        // (可能是关闭浏览器，或超时)，但此时缓存依旧存在(principals)，所以会自己跑到授权方法里
        if (!SecurityUtils.getSubject().isAuthenticated()) {
            doClearCache(principalCollection);
            SecurityUtils.getSubject().logout();
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>Shiro授权错误<<<<<");
            }
            throw new AuthorizationException();
        }
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        String url = WebUtils.getRequestUri(request);
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>为当前登录的Subject授予角色和权限,URL[{}]<<<<<<<<", url);
        }
        SimpleAuthorizationInfo simpleAuthorInfo = new SimpleAuthorizationInfo();

        // 得到登录的用户
        AdminUserGetVO onlineUserVO = (AdminUserGetVO) ShiroSessionUtils
                .getAttribute(configConst.getAdminSessionName());

        if (null != onlineUserVO) {
            // 得到用户的权限菜单

            List shiroStringRoleList = (List) ShiroSessionUtils.getAttribute("shiroStringRoles");
            List<String> shiroStringRoles = new Gson()
                    .fromJson(new Gson().toJson(shiroStringRoleList), new TypeToken<List<String>>() {
                    }.getType());
            List shiroStringRuleList = (List) ShiroSessionUtils.getAttribute("shiroStringRules");
            List<String> shiroStringRules = new Gson()
                    .fromJson(new Gson().toJson(shiroStringRuleList), new TypeToken<List<String>>() {
                    }.getType());

            if (null == shiroStringRoles) {
                // 角色
                List<String> stringRoles = ListUtils.newArrayList();
                // 权限
                List<String> stringRules = ListUtils.newArrayList();
                // 所属角色
                AdminRoleBO rolesBO = adminRoleService.getByUserId(onlineUserVO.getId());
                if (null != rolesBO) {
                    TreeSet<AdminRuleBO> rulesBO;
                    // 添加角色
                    stringRoles.add(rolesBO.getSole());
                    // 所属角色拥有的权限
                    // 添加权限
                    rulesBO = adminRuleService.listByRoleId(rolesBO.getId());
                    if (null != rulesBO) {
                        for (AdminRuleBO adminRuleBO : rulesBO) {
                            if (!ObjectUtils.equals(BigInteger.ZERO, adminRuleBO.getPid())) {
                                stringRules.add(adminRuleBO.getUrl());
                            }
                        }

                        // 返回权限
                        simpleAuthorInfo.addRoles(stringRoles);
                        simpleAuthorInfo.addStringPermissions(stringRules);

                        // 用户权限菜单存放在 session 中
                        ShiroSessionUtils.setAttribute("shiroStringRoles", stringRoles);
                        ShiroSessionUtils.setAttribute("shiroStringRules", stringRules);
                    }
                }
            } else {
                // 返回权限
                simpleAuthorInfo.addRoles(shiroStringRoles);
                simpleAuthorInfo.addStringPermissions(shiroStringRules);
            }
        }
        return simpleAuthorInfo;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ShiroDbRealm class

/* End of file ShiroDbRealm.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/ShiroDbRealm.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
