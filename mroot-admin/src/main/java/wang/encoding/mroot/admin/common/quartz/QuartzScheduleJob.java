/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.quartz;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.admin.common.component.AdminThreadPoolTaskExecutorComponent;
import wang.encoding.mroot.admin.common.enums.ScheduleJobOperationStatusEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.common.util.net.NetUtils;
import wang.encoding.mroot.common.util.time.ClockUtils;
import wang.encoding.mroot.service.admin.system.AdminScheduleJobLogService;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;
import wang.encoding.mroot.vo.admin.entity.system.schedulejoblog.AdminScheduleJobLogGetVO;


import java.time.Instant;
import java.util.Date;


/**
 * 定时任务 Bean
 *
 * @author ErYang
 */
@Slf4j
@Component
public class QuartzScheduleJob extends QuartzJobBean {

    /**
     * 线程池
     */
    private final AdminThreadPoolTaskExecutorComponent taskExecutor;
    private final AdminScheduleJobLogService adminScheduleJobLogService;

    @Autowired
    public QuartzScheduleJob(AdminThreadPoolTaskExecutorComponent taskExecutor,
            AdminScheduleJobLogService adminScheduleJobLogService) {
        this.taskExecutor = taskExecutor;
        this.adminScheduleJobLogService = adminScheduleJobLogService;
    }


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        // 得到定时任务
        AdminScheduleJobGetVO adminScheduleJobGetVO = (AdminScheduleJobGetVO) jobExecutionContext.getMergedJobDataMap()
                .get("scheduleJob");

        // 数据库保存执行记录
        AdminScheduleJobLogGetVO adminScheduleJobLogGetVO = new AdminScheduleJobLogGetVO();
        adminScheduleJobLogGetVO.setScheduleJobId(adminScheduleJobGetVO.getId());
        adminScheduleJobLogGetVO.setTitle(adminScheduleJobGetVO.getTitle());
        adminScheduleJobLogGetVO.setBeanName(adminScheduleJobGetVO.getBeanName());
        adminScheduleJobLogGetVO.setMethodName(adminScheduleJobGetVO.getMethodName());
        adminScheduleJobLogGetVO.setParams(adminScheduleJobGetVO.getParams());
        adminScheduleJobLogGetVO.setCronExpression(adminScheduleJobGetVO.getCronExpression());
        // 任务开始时间
        long startTime = ClockUtils.currentTimeMillis();
        // 执行任务
        try {
            QuartzScheduleJobRunnable quartzScheduleJobRunnable = new QuartzScheduleJobRunnable(
                    adminScheduleJobLogGetVO.getBeanName(), adminScheduleJobLogGetVO.getMethodName(),
                    adminScheduleJobLogGetVO.getParams());
            taskExecutor.adminThreadPoolTaskExecutor().execute(quartzScheduleJobRunnable);
            // 任务执行总时长
            adminScheduleJobLogGetVO.setTimes(Math.toIntExact(ClockUtils.elapsedTime(startTime)));
            adminScheduleJobLogGetVO.setExecutionResult(ScheduleJobOperationStatusEnum.SUCCEED.getValue());
            // 定时任务状态 任务状态 1：成功 2：失败 详情请见：ScheduleJobOperationStatusEnum
            adminScheduleJobLogGetVO.setState(ScheduleJobOperationStatusEnum.SUCCEED.getKey());
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>执行定时任务失败[{}]：{}<<<<<<<<", adminScheduleJobGetVO, e.getMessage());
            }
            // 任务执行总时长
            adminScheduleJobLogGetVO.setTimes(Math.toIntExact(ClockUtils.elapsedTime(startTime)));
            adminScheduleJobLogGetVO.setExecutionResult(ScheduleJobOperationStatusEnum.FAILED.getValue());
            // 定时任务状态 任务状态 1：成功 2：失败 详情请见：ScheduleJobStatusEnum
            adminScheduleJobLogGetVO.setState(ScheduleJobOperationStatusEnum.FAILED.getKey());
            adminScheduleJobLogGetVO.setErrorMessage(e.getMessage());
        } finally {
            adminScheduleJobLogGetVO.setGmtCreateIp(IpUtils.ipv4StringToInt(NetUtils.getLocalHost()));
            adminScheduleJobLogGetVO.setState(StateEnum.NORMAL.getKey());
            adminScheduleJobLogGetVO.setGmtCreate(Date.from(Instant.now()));
            adminScheduleJobLogGetVO.setRemark(adminScheduleJobGetVO.getRemark());
            // 保存数据到数据库
            adminScheduleJobLogService.save(adminScheduleJobLogGetVO);
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QuartzScheduleJob class

/* End of file QuartzScheduleJob.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/quartz/QuartzScheduleJob.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
