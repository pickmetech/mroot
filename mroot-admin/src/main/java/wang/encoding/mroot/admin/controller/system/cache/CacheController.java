/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.cache;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.exception.ControllerException;

import java.util.concurrent.Future;


/**
 * 后台 缓存 控制器
 *
 *@author ErYang
 */
@Slf4j
@RestController
@RequestMapping("/system/cache")
public class CacheController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/cache";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/cache";
    /**
     * 清除页面
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存清除
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;


    // -------------------------------------------------------------------------------------------------

    /**
     * 清除页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_CACHE_INDEX)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + ADD_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 保存清除缓存
     *
     * @return JSONObject
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_CACHE_SAVE)
    @FormToken(remove = true)
    public JSONObject save() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            String name = super.request.getParameter("name");
            if (StringUtils.isBlank(name)) {
                return super.initErrorJSONObject(failResult, "validation.system.cache.name.pattern");
            }
            String allStr = "all";
            // 异步清空缓存
            if (allStr.equalsIgnoreCase(name)) {
                Future<String> asyncResult = adminControllerAsyncTask.clearAllCache();
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "清空所有缓存");
            } else {
                Future<String> asyncResult = adminControllerAsyncTask.clearAllCache2Pattern(name);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "清空指定缓存");
            }
            return super.initSucceedJSONObject(ResultData.ok(), "message.system.cache.save.succeed");
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CacheController class

/* End of file CacheController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/cache/CacheController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
