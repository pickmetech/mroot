/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.cms.article;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.DynamicData;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.QiNiuUtils;
import wang.encoding.mroot.common.util.io.FileUtils;
import wang.encoding.mroot.common.util.io.ImageUtils;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.common.util.time.DateFormatUtils;
import wang.encoding.mroot.service.admin.cms.AdminArticleContentService;
import wang.encoding.mroot.service.admin.cms.AdminArticleService;
import wang.encoding.mroot.service.admin.cms.AdminCategoryService;
import wang.encoding.mroot.vo.admin.entity.cms.article.AdminArticleGetVO;
import wang.encoding.mroot.vo.admin.entity.cms.articlecontent.AdminArticleContentGetVO;
import wang.encoding.mroot.vo.admin.entity.cms.category.AdminCategoryGetVO;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;


/**
 * 后台 文章 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/cms/article")
public class ArticleController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/cms/article";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/cms/article";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "article";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;

    private static final String UPLOAD = "/uploadcover";
    private static final String UPLOAD_URL = MODULE_NAME + UPLOAD;

    private final AdminArticleService adminArticleService;
    private final AdminCategoryService adminCategoryService;
    private final AdminArticleContentService adminArticleContentService;

    @Autowired
    public ArticleController(AdminArticleService adminArticleService, AdminCategoryService adminCategoryService,
            AdminArticleContentService adminArticleContentService) {
        this.adminArticleService = adminArticleService;
        this.adminCategoryService = adminCategoryService;
        this.adminArticleContentService = adminArticleContentService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminArticleGetVO adminArticleGetVO = new AdminArticleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminArticleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminArticleGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminArticleGetVO> pageInt = new Page<>();
        IPage<AdminArticleGetVO> pageAdmin = adminArticleService
                .list2page(super.initPage(pageInt), adminArticleGetVO, AdminArticleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        this.getCategories2Tree();

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param articleGetVO AdminArticleGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_SAVE)
    @FormToken(remove = true)
    public Object save(AdminArticleGetVO articleGetVO, AdminArticleContentGetVO articleContentVO)
            throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();

            // 创建 AdminArticleGetVO 对象
            AdminArticleGetVO adminArticleGetVO = this.initAddData(articleGetVO);
            // 创建 AdminArticleContentVO 对象
            AdminArticleContentGetVO adminArticleContentVO = this.initAddData2Content(articleContentVO);

            // 是否前台可见
            String displayStr = super.request.getParameter("displayStr");
            if (StringUtils.isNotBlank(displayStr) && displayStr.equals(configConst.getBootstrapSwitchEnabled())) {
                adminArticleGetVO.setDisplay(BooleanEnum.YES.getKey());
            } else {
                adminArticleGetVO.setDisplay(BooleanEnum.NO.getKey());
            }

            // 是否转载
            String reprintStr = super.request.getParameter("reprintStr");
            if (StringUtils.isNotBlank(reprintStr) && reprintStr.equals(configConst.getBootstrapSwitchEnabled())) {
                adminArticleGetVO.setReprint(BooleanEnum.YES.getKey());
            } else {
                adminArticleGetVO.setReprint(BooleanEnum.NO.getKey());
            }

            // 是否打赏
            String rewardStr = super.request.getParameter("rewardStr");
            if (StringUtils.isNotBlank(rewardStr) && rewardStr.equals(configConst.getBootstrapSwitchEnabled())) {
                adminArticleGetVO.setReward(BooleanEnum.YES.getKey());
            } else {
                adminArticleGetVO.setReward(BooleanEnum.NO.getKey());
            }

            // Hibernate Validation  验证数据
            String validationResult = adminArticleService.validationArticle(adminArticleGetVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // Hibernate Validation  验证数据
            String validationResult2 = adminArticleContentService.validationArticleContent(adminArticleContentVO);
            if (StringUtils.isNotBlank(validationResult2)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult2);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(adminArticleGetVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增 文章
            boolean result = adminArticleService.save(adminArticleGetVO);
            if (result) {
                // 异步新增文章内容
                AdminArticleGetVO newAdminArticleGetVO = adminArticleService.getByTitle(adminArticleGetVO.getTitle());
                Future<String> asyncResult = adminControllerAsyncTask
                        .addArticleContent(newAdminArticleGetVO, adminArticleContentVO);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "新增文章内容");
            }
            return super.initSaveJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 上传图片封面
     * @param file MultipartFile
     * @return JSONObject
     */
    @RequiresPermissions(UPLOAD_URL)
    @RequestMapping(UPLOAD)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_UPLOADCOVER)
    public JSONObject uploadCover(@RequestParam("file") MultipartFile file) throws ControllerException, IOException {
        ResultData failResult = ResultData.fail();
        if (null != file.getOriginalFilename() && StringUtils.isNotBlank(file.getOriginalFilename())) {
            // 2MB
            long size2mb = 1024 * 1024 * 2;
            // 图片类型
            String type = FileUtils.getFileExtension(file.getOriginalFilename());
            if (!ImageUtils.isImage(type)) {
                return super.initErrorJSONObject(failResult, "message.upload.image.file.error");
            }
            // 文件的大小
            if (file.getSize() > size2mb) {
                return super.initErrorJSONObject(failResult, "message.upload.file.over.size");
            }

            // 重命名
            String name = DateFormatUtils.formatDate(DateFormatUtils.PATTERN_YYYYMMDDHHMMSSSSS, new Date()) + (
                    Math.random() * 9000 + 1000);
            name = name.replace(".", "");
            String date = DateFormatUtils.formatDate(DateFormatUtils.PATTERN_YYYYMMDD, new Date());

            ResultData resultData;
            // 得到数据库配置的上传配置
            AdminConfigGetVO config = DynamicData.getConfigBySole(databaseConst.getQiniuUploadName());
            if (null != config && StringUtils.isNotBlank(config.getContent()) && null != QiNiuUtils.uploadLocal
                    && !QiNiuUtils.uploadLocal) {
                // 上传到七牛
                // key
                String key = "/assets" + ARTICLE_COVER_UPLOAD_PATH_URL + "/" + date + "/" + name + "." + type;
                String response = QiNiuUtils.uploadFile(file.getInputStream(), QiNiuUtils.BUCKET, key);
                // 七牛返回地址
                String url = QiNiuUtils.getResponseKeyUrl(response);
                resultData = ResultData.ok(GlobalMessage.MESSAGE, url);
            } else {
                // 上传到本地

                // 保存图片的路径
                String savePath = uploadResourcesPath + ARTICLE_COVER_UPLOAD_PATH_URL + "/" + date;
                FileUtils.makefileDirExists(savePath);
                // 名称
                String key = savePath + "/" + name + "." + type;

                // 上传图片
                BufferedInputStream bufferedInputStream = new BufferedInputStream(file.getInputStream(), 8192);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(key), 8192);
                IOUtils.copy(bufferedInputStream, bufferedOutputStream);
                bufferedOutputStream.flush();
                bufferedOutputStream.close();

                String imgUrl = ARTICLE_COVER_UPLOAD_PATH_URL + "/" + date + "/" + name + "." + type;
                String url = contextPath + "/assets" + imgUrl;
                resultData = ResultData.ok(GlobalMessage.MESSAGE, url);
            }
            return resultData.toFastJson();
        } else {
            return super.initReturnErrorJSONObject();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);


        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminArticleGetVO articleGetVO = adminArticleService.getById(idValue);
        if (null == articleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == articleGetVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }

        modelAndView.addObject(VIEW_MODEL_NAME, articleGetVO);
        // 文章内容
        AdminArticleContentGetVO adminArticleContentGetVO = adminArticleContentService.getById(idValue);
        modelAndView.addObject("articleContent", adminArticleContentGetVO);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());
        // 是的状态
        modelAndView.addObject("booleanYes", BooleanEnum.YES.getKey());
        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        this.getCategories2Tree();
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminArticleGetVO articleGetVO, AdminArticleContentGetVO articleContentVO)
            throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 数据真实性
            AdminArticleGetVO articleGetVOBefore = adminArticleService.getById(idValue);
            if (null == articleGetVOBefore) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == articleGetVOBefore.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }
            AdminArticleContentGetVO articleContentGetVOBefore = adminArticleContentService.getById(idValue);
            // 创建 AdminArticleGetVO 对象
            AdminArticleGetVO validationArticle = BeanMapperComponent.map(articleGetVOBefore, AdminArticleGetVO.class);
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationArticle.setState(StateEnum.NORMAL.getKey());
            } else {
                validationArticle.setState(StateEnum.DISABLE.getKey());
            }
            // 是否前台可见
            String displayStr = super.request.getParameter("displayStr");
            if (StringUtils.isNotBlank(displayStr) && displayStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationArticle.setDisplay(BooleanEnum.YES.getKey());
            } else {
                validationArticle.setDisplay(BooleanEnum.NO.getKey());
            }
            // 是否转载
            String reprintStr = super.request.getParameter("reprintStr");
            if (StringUtils.isNotBlank(reprintStr) && reprintStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationArticle.setReprint(BooleanEnum.YES.getKey());
            } else {
                validationArticle.setReprint(BooleanEnum.NO.getKey());
            }
            // 是否打赏
            String rewardStr = super.request.getParameter("rewardStr");
            if (StringUtils.isNotBlank(rewardStr) && rewardStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationArticle.setReward(BooleanEnum.YES.getKey());
            } else {
                validationArticle.setReward(BooleanEnum.NO.getKey());
            }
            this.initEditData(validationArticle, articleGetVO);
            // Hibernate Validation 验证数据
            String validationResult = adminArticleService.validationArticle(validationArticle);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }
            // 创建 AdminArticleContentGetVO 对象
            AdminArticleContentGetVO validationArticleContent = BeanMapperComponent
                    .map(articleContentGetVOBefore, AdminArticleContentGetVO.class);
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationArticleContent.setState(StateEnum.NORMAL.getKey());
            } else {
                validationArticleContent.setState(StateEnum.DISABLE.getKey());
            }
            this.initEditData2Content(validationArticleContent, articleContentVO);
            // Hibernate Validation 验证数据
            String validationResult2 = adminArticleContentService.validationArticleContent(validationArticleContent);
            if (StringUtils.isNotBlank(validationResult2)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }
            // 验证数据唯一性
            boolean flag = this.validationEditData(validationArticle, articleGetVOBefore, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }
            // 修改 文章
            boolean result = adminArticleService.update(validationArticle);
            if (result) {
                // 异步修改文章内容
                AdminArticleGetVO newAdminArticleGetVO = adminArticleService.getById(idValue);
                Future<String> asyncResult = adminControllerAsyncTask
                        .updateArticleContent(newAdminArticleGetVO, validationArticleContent);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "更新文章内容");
            }
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminArticleGetVO articleGetVO = adminArticleService.getById(idValue);
        if (null == articleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, articleGetVO);
        // 文章内容
        AdminArticleContentGetVO adminArticleContentGetVO = adminArticleContentService.getById(idValue);
        modelAndView.addObject("articleContent", adminArticleContentGetVO);
        // 文章分类
        AdminCategoryGetVO adminCategoryGetVO = adminCategoryService.getById(articleGetVO.getCategoryId());
        modelAndView.addObject("category", adminCategoryGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminArticleGetVO articleGetVO = adminArticleService.getById(idValue);
            if (null == articleGetVO) {
                return super.initErrorCheckJSONObject();
            }

            // 删除 文章
            boolean result = adminArticleService.remove2StatusById(articleGetVO.getId());
            if (result) {
                // 异步删除文章内容
                AdminArticleGetVO newAdminArticleGetVO = adminArticleService.getById(idValue);
                Future<String> asyncResult = adminControllerAsyncTask.removeArticleContent(newAdminArticleGetVO);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "删除文章内容");
            }
            return super.initDeleteJSONObject(result);
        }

        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     *
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger[] idArray = super.getIdArray();
            // 验证数据
            if (null == idArray || org.apache.commons.lang3.ArrayUtils.isEmpty(idArray)) {
                return this.initErrorCheckJSONObject();
            }
            boolean flag = adminArticleService.removeBatch2UpdateStatus(idArray);
            if (flag) {
                // 异步批量删除文章内容
                Future<String> asyncResult = adminControllerAsyncTask.removeBatchArticleContent(idArray);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "批量文章内容");
                return this.initDeleteJSONObject(true);
            } else {
                return this.initDeleteJSONObject(false);
            }
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_ARTICLE_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);


        AdminArticleGetVO adminArticleGetVO = new AdminArticleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminArticleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminArticleGetVO.setState(StateEnum.DELETE.getKey());

        Page<AdminArticleGetVO> pageInt = new Page<>();
        IPage<AdminArticleGetVO> pageAdmin = adminArticleService
                .list2page(super.initPage(pageInt), adminArticleGetVO, AdminArticleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 文章分类集合
     */
    private void getCategories2Tree() {
        // 权限 tree 数据
        List<AdminCategoryGetVO> list = adminCategoryService.listType();
        if (null != list) {
            list = AdminCategoryGetVO.list(list);
            super.request.setAttribute("treeCategories", list);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminArticleGetVO AdminArticleGetVO   文章
     * @return AdminArticleGetVO
     */
    private AdminArticleGetVO initAddData(@NotNull final AdminArticleGetVO adminArticleGetVO) {
        AdminArticleGetVO addArticleGetVO = BeanMapperComponent.map(adminArticleGetVO, AdminArticleGetVO.class);
        addArticleGetVO.setPageView(1);
        // IP
        addArticleGetVO.setGmtCreateIp(super.getIp());
        addArticleGetVO.setState(StateEnum.NORMAL.getKey());
        addArticleGetVO.setGmtCreate(Date.from(Instant.now()));
        return addArticleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminArticleContentVO AdminArticleContentGetVO
     *
     * @return AdminArticleContentVO
     */
    private AdminArticleContentGetVO initAddData2Content(
            @NotNull final AdminArticleContentGetVO adminArticleContentVO) {
        adminArticleContentVO.setContentHtml(adminArticleContentVO.getContent());
        // IP
        adminArticleContentVO.setGmtCreateIp(super.getIp());
        adminArticleContentVO.setState(StateEnum.NORMAL.getKey());
        adminArticleContentVO.setGmtCreate(Date.from(Instant.now()));
        return adminArticleContentVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editArticleGetVO AdminArticleGetVO  文章
     * @param articleGetVO AdminArticleGetVO  文章
     */
    private void initEditData(@NotNull final AdminArticleGetVO editArticleGetVO,
            @NotNull final AdminArticleGetVO articleGetVO) {
        editArticleGetVO.setCover(articleGetVO.getCover());
        editArticleGetVO.setTitle(articleGetVO.getTitle());
        editArticleGetVO.setDescription(articleGetVO.getDescription());
        editArticleGetVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editArticleContentGetVO AdminArticleContentGetVO  文章内容
     * @param articleContentGetVO AdminArticleContentGetVO  文章内容
     */
    private void initEditData2Content(@NotNull final AdminArticleContentGetVO editArticleContentGetVO,
            @NotNull final AdminArticleContentGetVO articleContentGetVO) {
        editArticleContentGetVO.setContent(articleContentGetVO.getContent());
        editArticleContentGetVO.setContentHtml(articleContentGetVO.getContent());
        editArticleContentGetVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param articleGetVO AdminArticleGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminArticleGetVO articleGetVO,
            @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        AdminArticleGetVO titleExist = adminArticleService.getByTitle(articleGetVO.getTitle());
        if (null != titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param articleGetVO AdminArticleGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationEditData(@NotNull final AdminArticleGetVO newArticleGetVO,
            @NotNull final AdminArticleGetVO articleGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = adminArticleService
                .propertyUnique(AdminArticleGetVO.TITLE, newArticleGetVO.getTitle(), articleGetVO.getTitle());
        if (!titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        return true;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleController class

/* End of file ArticleController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/cms/article/ArticleController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
