/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 资源配置文件
 *
 * @author ErYang
 */
@Component
@PropertySource("classpath:resource.properties")
@ConfigurationProperties(prefix = "resource")
@EnableCaching
@Getter
@Setter
public class ResourceConst {

    /**
     *默认主题
     */
    public static final String DEFAULT_THEME = "DEFAULT";
    /**
     *elite 主题
     */
    public static final String ELITE_THEME = "ELITE";
    /**
     * 资源信息集合名称
     */
    private String mapName;
    /**
     * 资源信息集合名称
     */
    private String resourceName;
    /**
     * 资源信息集合
     */
    private String resource;
    /**
     * 当前主题
     */
    private String currentTheme;
    /**
     * 默认主题目录名称
     */
    private String defaultThemeName;
    /**
     * 默认主题目录
     */
    private String defaultTheme;
    /**
     * 默认公共资源目录名称
     */
    private String defaultVendorsName;
    /**
     * 默认公共资源目录
     */
    private String defaultVendors;
    /**
     * 默认 app 目录名称
     */
    private String defaultAppName;
    /**
     * 默认 app 目录
     */
    private String defaultApp;
    /**
     * 默认 base 目录名称
     */
    private String defaultBaseName;
    /**
     * 默认 base 目录
     */
    private String defaultBase;
    /**
     * elite 主题名称
     */
    private String eliteThemeName;
    /**
     * elite 主题目录
     */
    private String eliteTheme;
    /**
     * elite app 名称
     */
    private String eliteAppName;
    /**
     * elite app 目录
     */
    private String eliteApp;
    /**
     * elite base 目录名称
     */
    private String eliteBaseName;
    /**
     * elite base 目录
     */
    private String eliteBase;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ResourceConst class

/* End of file ResourceConst.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/constant/ResourceConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
