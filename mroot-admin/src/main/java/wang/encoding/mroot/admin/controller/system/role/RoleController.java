/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.role;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.collection.ArrayUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.vo.admin.entity.system.role.AdminRoleGetVO;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.Future;


/**
 * 后台 角色 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/system/role")
public class RoleController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/role";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/role";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "role";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;
    /**
     * 授权
     */
    private static final String AUTHORIZATION = "/authorization";
    private static final String AUTHORIZATION_URL = MODULE_NAME + AUTHORIZATION;
    private static final String AUTHORIZATION_VIEW = VIEW_PATH + AUTHORIZATION;
    /**
     * 保存授权
     */
    private static final String AUTHORIZATION_SAVE = "/authorizationsave";
    private static final String AUTHORIZATION_SAVE_URL = MODULE_NAME + AUTHORIZATION_SAVE;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;

    private final AdminRoleService adminRoleService;
    private final AdminRuleService adminRuleService;


    @Autowired
    public RoleController(AdminRoleService adminRoleService, AdminRuleService adminRuleService) {
        this.adminRoleService = adminRoleService;
        this.adminRuleService = adminRuleService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminRoleGetVO adminRoleGetVO = new AdminRoleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminRoleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminRoleGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminRoleGetVO> pageInt = new Page<>();
        IPage<AdminRoleGetVO> pageAdmin = adminRoleService
                .list2page(super.initPage(pageInt), adminRoleGetVO, AdminRoleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.AUTHORIZATION_NAME, contextPath + AUTHORIZATION_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param roleGetVO AdminRoleGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_SAVE)
    @FormToken(remove = true)
    public Object save(AdminRoleGetVO roleGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();

            // 创建 AdminRoleGetVO 对象
            AdminRoleGetVO adminRoleGetVO = this.initAddData(roleGetVO);

            // Hibernate Validation  验证数据
            String validationResult = adminRoleService.validationRole(adminRoleGetVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(adminRoleGetVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增 角色
            boolean result = adminRoleService.save(adminRoleGetVO);
            return super.initSaveJSONObject(result);

        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);


        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminRoleGetVO roleGetVO = adminRoleService.getById(idValue);
        if (null == roleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == roleGetVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }

        modelAndView.addObject(VIEW_MODEL_NAME, roleGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());

        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminRoleGetVO adminRoleGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 数据真实性
            AdminRoleGetVO roleGetVOBefore = adminRoleService.getById(idValue);
            if (null == roleGetVOBefore) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == roleGetVOBefore.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 创建 AdminRoleGetVO 对象
            AdminRoleGetVO validationRole = BeanMapperComponent.map(roleGetVOBefore, AdminRoleGetVO.class);
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationRole.setState(StateEnum.NORMAL.getKey());
            } else {
                validationRole.setState(StateEnum.DISABLE.getKey());
            }

            this.initEditData(validationRole, adminRoleGetVO);

            // Hibernate Validation 验证数据
            String validationResult = adminRoleService.validationRole(validationRole);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationEditData(validationRole, roleGetVOBefore, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 修改 角色
            boolean result = adminRoleService.update(validationRole);
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminRoleGetVO roleGetVO = adminRoleService.getById(idValue);
        if (null == roleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, roleGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminRoleGetVO roleGetVO = adminRoleService.getById(idValue);
            if (null == roleGetVO) {
                return super.initErrorCheckJSONObject();
            }

            // 删除 角色
            boolean result = adminRoleService.remove2StatusById(roleGetVO.getId());
            if (result) {
                // 异步删除 角色-权限
                Future<String> asyncResult = adminControllerAsyncTask.removeByRoleId(roleGetVO.getId());
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "删除角色-权限");
            }
            return super.initDeleteJSONObject(result);
        }

        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     *
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger[] idArray = super.getIdArray();
            // 验证数据
            if (null == idArray || org.apache.commons.lang3.ArrayUtils.isEmpty(idArray)) {
                return this.initErrorCheckJSONObject();
            }
            boolean flag = adminRoleService.removeBatch2UpdateStatus(idArray);
            if (flag) {
                // 异步批量删除 用户-角色
                Future<String> asyncResult = adminControllerAsyncTask.removeByRoleIdArray(idArray);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "批量删除角色-权限");
                return this.initDeleteJSONObject(true);
            } else {
                return this.initDeleteJSONObject(false);
            }
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 授权页面
     *
     * @param id String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(AUTHORIZATION_URL)
    @RequestMapping(AUTHORIZATION + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_ROLE_AUTHORIZATION)
    @FormToken(init = true)
    public ModelAndView authorization(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(AUTHORIZATION_VIEW, AUTHORIZATION_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminRoleGetVO roleGetVO = adminRoleService.getById(idValue);
        if (null == roleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }


        // 权限 tree 数据
        AdminUserGetVO currentUserVO = super.getCurrentAdmin();
        if (null != currentUserVO) {
            AdminRoleBO currentRoleVO = adminRoleService.getByUserId(currentUserVO.getId());
            if (null != currentRoleVO) {
                // 如果是超级管理员 读取全部权限
                List<AdminRuleGetVO> list;
                if (Objects.equals(currentRoleVO.getId(), BigInteger.ONE)) {
                    list = adminRuleService.listPidGt0();
                } else {
                    list = adminRuleService.listByRoleIdAndPidGt0(currentRoleVO.getId());
                }

                if (ListUtils.isNotEmpty(list)) {
                    list = AdminRuleGetVO.list(list);
                    super.request.setAttribute("treeRules", list);
                }
            }
        }

        // 已拥有权限
        String ruleString = adminRuleService.listIdByRoleId2String(idValue);
        if (null != ruleString) {
            modelAndView.addObject("ruleString", ruleString);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, roleGetVO);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.AUTHORIZATION_SAVE_NAME, contextPath + AUTHORIZATION_SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 保存授权
     *
     * @return Object
     */
    @RequiresPermissions(AUTHORIZATION_SAVE_URL)
    @RequestMapping(AUTHORIZATION_SAVE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_ROLE_AUTHORIZATION_SAVE)
    @FormToken(remove = true)
    public Object authorizationSave() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            BigInteger idValue = super.getId();
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                super.initErrorCheckJSONObject(failResult);
            }
            // 数据真实性
            AdminRoleGetVO roleGetVO = adminRoleService.getById(idValue);
            if (null == roleGetVO) {
                super.initErrorCheckJSONObject(failResult);
            }
            if (null != roleGetVO) {
                if (StateEnum.DELETE.getKey() == roleGetVO.getState()) {
                    super.initErrorCheckJSONObject(failResult);
                }
            }
            // 权限数组
            String rulesStr = super.request.getParameter("rules");
            if (StringUtils.isBlank(rulesStr)) {
                return super.initErrorJSONObject(failResult, "message.system.role.authorization.fail");
            }
            String[] ruleStrArray = rulesStr.split(",");
            if (org.apache.commons.lang3.ArrayUtils.isEmpty(ruleStrArray)) {
                return super.initErrorJSONObject(failResult, "message.system.role.authorization.fail");
            }

            // 转换数组
            BigInteger[] ruleBigIntegerArray = ArrayUtils.string2BigInteger(ruleStrArray);
            if (org.apache.commons.lang3.ArrayUtils.isNotEmpty(ruleBigIntegerArray)) {
                // 排序
                Arrays.sort(ruleBigIntegerArray);
            } else {
                return super.initErrorJSONObject(failResult, "message.system.role.authorization.fail");
            }

            // 匹配集合
            boolean flag = false;
            // 已拥有权限
            String roleHasRuleString = adminRuleService.listIdByRoleId2String(idValue);
            if (StringUtils.isNotBlank(roleHasRuleString)) {
                String[] roleHasRuleStringArray = roleHasRuleString.split(",");
                BigInteger[] roleHasRuleArray = ArrayUtils.string2BigInteger(roleHasRuleStringArray);
                if (org.apache.commons.lang3.ArrayUtils.isEmpty(roleHasRuleArray)) {
                    // 排序
                    Arrays.sort(roleHasRuleArray);
                    // 比较集合
                    flag = Arrays.equals(ruleBigIntegerArray, roleHasRuleArray);
                }
            }
            // 权限没有发生变化
            if (flag) {
                ResultData resultData = ResultData.ok();
                return super.initSucceedJSONObject(resultData, "message.authorization.succeed");
            } else {
                if (StringUtils.isNotBlank(roleHasRuleString)) {
                    // 删除已有的权限
                    adminRuleService.removeBatchByRoleIdAndRuleIdArray(idValue, null);
                }
                // 授权的权限集合
                int result = adminRuleService.saveBatchByRoleIdAndRuleIdArray(idValue, ruleBigIntegerArray);
                // 授权成功
                if (0 < result) {
                    // 清空 session 中的用户权限菜单
                    ShiroSessionUtils.setAttribute(configConst.getAdminMenuName(), null);
                    ResultData resultData = ResultData.ok();
                    return super.initSucceedJSONObject(resultData, "message.authorization.succeed");
                } else {
                    return super.initErrorJSONObject(failResult, "message.authorization.fail");
                }
            }
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_ROLE_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);


        AdminRoleGetVO adminRoleGetVO = new AdminRoleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminRoleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminRoleGetVO.setState(StateEnum.DELETE.getKey());

        Page<AdminRoleGetVO> pageInt = new Page<>();
        IPage<AdminRoleGetVO> pageAdmin = adminRoleService
                .list2page(super.initPage(pageInt), adminRoleGetVO, AdminRoleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminRoleGetVO AdminRoleGetVO   角色
     * @return AdminRoleGetVO
     */
    private AdminRoleGetVO initAddData(@NotNull final AdminRoleGetVO adminRoleGetVO) {
        AdminRoleGetVO addRoleGetVO = BeanMapperComponent.map(adminRoleGetVO, AdminRoleGetVO.class);
        // IP
        addRoleGetVO.setGmtCreateIp(super.getIp());
        addRoleGetVO.setState(StateEnum.NORMAL.getKey());
        addRoleGetVO.setGmtCreate(Date.from(Instant.now()));
        return addRoleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editRoleGetVO AdminRoleGetVO  角色
     * @param adminRoleGetVO AdminRoleGetVO  角色
     */
    private void initEditData(@NotNull final AdminRoleGetVO editRoleGetVO,
            @NotNull final AdminRoleGetVO adminRoleGetVO) {
        editRoleGetVO.setSole(adminRoleGetVO.getSole());
        editRoleGetVO.setTitle(adminRoleGetVO.getTitle());
        editRoleGetVO.setRemark(adminRoleGetVO.getRemark());
        editRoleGetVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param roleGetVO AdminRoleGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminRoleGetVO roleGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        AdminRoleGetVO titleExist = adminRoleService.getByTitle(roleGetVO.getTitle());
        if (null != titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        AdminRoleGetVO soleExist = adminRoleService.getBySole(roleGetVO.getSole());
        if (null != soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param roleGetVO AdminRoleGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationEditData(@NotNull final AdminRoleGetVO newRoleGetVO,
            @NotNull final AdminRoleGetVO roleGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = adminRoleService
                .propertyUnique(AdminRoleGetVO.TITLE, newRoleGetVO.getTitle(), roleGetVO.getTitle());
        if (!titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        boolean soleExist = adminRoleService
                .propertyUnique(AdminRoleGetVO.SOLE, newRoleGetVO.getSole(), roleGetVO.getSole());
        if (!soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RoleController class

/* End of file RoleController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/role/RoleController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
