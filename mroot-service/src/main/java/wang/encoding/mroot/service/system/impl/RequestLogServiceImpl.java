/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl;


import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.service.BaseServiceImpl;
import wang.encoding.mroot.dao.system.RequestLogDAO;
import wang.encoding.mroot.domain.entity.system.RequestLogDO;
import wang.encoding.mroot.service.system.RequestLogService;

import java.util.HashMap;
import java.util.Map;

/**
 * 后台 请求日志 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class RequestLogServiceImpl extends BaseServiceImpl<RequestLogDAO, RequestLogDO> implements RequestLogService {

    /**
     * 清空记录
     *
     * @return boolean
     */
    @Override
    public boolean truncate() {
        return SqlHelper.retBool(baseMapper.truncate());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务删除
     *
     * @param size Integer
     * @return boolean
     */
    @Override
    public boolean delete2QuartzJob(@NotNull final Integer size) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("size", size);
        return SqlHelper.retBool(baseMapper.delete2QuartzJob(params));
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogServiceImpl class

/* End of file RequestLogServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/impl/RequestLogServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
