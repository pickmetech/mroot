/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system;


import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.service.BaseService;
import wang.encoding.mroot.domain.entity.system.RoleDO;

import java.math.BigInteger;
/**
 * 角色 Service
 *
 * @author ErYang
 */
public interface RoleService extends BaseService<RoleDO> {

    /**
     * 根据用户 ID 查询角色
     *
     * @param userId BigInteger 用户ID
     * @return Set<RoleDO>
     */
    RoleDO getByUserId(@NotNull final BigInteger userId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量保存 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleIdArray BigInteger[]
     * @return int
     */
    int saveBatchByUserIdAndRoleIdArray(@NotNull final BigInteger userId, @Nullable final BigInteger[] roleIdArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量删除 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleIdArray BigInteger[]
     * @return int
     */
    int removeBatchByUserIdAndRoleIdArray(@NotNull final BigInteger userId, @Nullable final BigInteger[] roleIdArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 批量删除 用户-角色 表
     *
     * @param userIdArray BigInteger[]
     *
     * @return int
     */
    int removeByUserIdArray(@NotNull final BigInteger[] userIdArray);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RoleService interface

/* End of file RoleService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/RoleService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
