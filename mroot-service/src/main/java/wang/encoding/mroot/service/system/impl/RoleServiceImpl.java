/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl;


import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.service.BaseServiceImpl;
import wang.encoding.mroot.dao.system.RoleDAO;
import wang.encoding.mroot.domain.entity.system.RoleDO;
import wang.encoding.mroot.service.system.RoleService;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * 角色 Service 实现类
 *
 * @author ErYang
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<RoleDAO, RoleDO> implements RoleService {


    /**
     * 根据用户 ID 查询角色
     *
     * @param userId BigInteger 用户ID
     * @return Set<RoleDO>
     */
    @Override
    public RoleDO getByUserId(@NotNull final BigInteger userId) {
        RoleDO roleDO = baseMapper.getByUserId(userId);
        if (null != roleDO) {
            return roleDO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量保存 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleIdArray BigInteger[]
     * @return int
     */
    @Override
    public int saveBatchByUserIdAndRoleIdArray(@NotNull final BigInteger userId,
            @Nullable final BigInteger[] roleIdArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("userId", userId);
        if (ArrayUtils.isNotEmpty(roleIdArray)) {
            params.put("roleIdArray", roleIdArray);
        }
        return baseMapper.saveBatchByUserIdAndRoleIdArray(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量删除 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleIdArray BigInteger[]
     * @return int
     */
    @Override
    public int removeBatchByUserIdAndRoleIdArray(@NotNull final BigInteger userId,
            @Nullable final BigInteger[] roleIdArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("userId", userId);
        if (ArrayUtils.isNotEmpty(roleIdArray)) {
            params.put("roleIdArray", roleIdArray);
        }
        return baseMapper.removeBatchByUserIdAndRoleIdArray(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 批量删除 用户-角色 表
     *
     * @param userIdArray BigInteger[]
     *
     * @return int
     */
    @Override
    public int removeByUserIdArray(@NotNull final BigInteger[] userIdArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("userIdArray", userIdArray);
        return baseMapper.removeByUserIdArray(params);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RoleServiceImpl class

/* End of file RoleServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/impl/RoleServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
