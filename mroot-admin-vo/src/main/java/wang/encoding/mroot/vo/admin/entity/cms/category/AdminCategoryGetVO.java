/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]    <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>     <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.cms.category;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import wang.encoding.mroot.common.annotation.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * 后台文章分类实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class AdminCategoryGetVO extends AdminCategoryVO {

    private static final long serialVersionUID = -158128221863717100L;


    /**
     * 状态
     */
    private String status;
    /**
     * 类型
     */
    private String type;
    /**
     * IP
     */
    private String ip;
    /**
     * 父级分类
     */
    private AdminCategoryGetVO parentCategory;
    /**
     * 子级权限集合
     */
    private List<AdminCategoryGetVO> childrenList;

    // -------------------------------------------------------------------------------------------------

    /**
     * list 转为 list
     *
     * @param list List
     * @return List
     */
    public static List<AdminCategoryGetVO> list(@NotNull final List list) {
        List<AdminCategoryGetVO> listAll = new Gson()
                .fromJson(new Gson().toJson(list), new TypeToken<List<AdminCategoryGetVO>>() {
                }.getType());
        List<AdminCategoryGetVO> roots = AdminCategoryGetVO.listRoot(listAll);
        List notRoots = (List) CollectionUtils.subtract(list, roots);
        for (AdminCategoryGetVO root : roots) {
            root.setChildrenList(AdminCategoryGetVO.listChildren(root, notRoots));
        }
        return roots;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 找到根节点
     *
     * @param allNodes List
     * @return list
     */
    private static List<AdminCategoryGetVO> listRoot(@NotNull final List<AdminCategoryGetVO> allNodes) {
        List<AdminCategoryGetVO> results = new ArrayList<>();
        for (AdminCategoryGetVO node : allNodes) {
            boolean isRoot = true;
            for (AdminCategoryGetVO comparedOne : allNodes) {
                if (Objects.equals(node.getPid(), comparedOne.getId())) {
                    isRoot = false;
                    break;
                }
            }
            if (isRoot) {
                results.add(node);
            }
        }
        return results;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据根节点找到子节点
     *
     * @param root     AdminRuleGetVO
     * @param allNodes List
     * @return List
     */
    private static List<AdminCategoryGetVO> listChildren(@NotNull final AdminCategoryGetVO root,
            @NotNull final List allNodes) {
        List<AdminCategoryGetVO> list = new Gson()
                .fromJson(new Gson().toJson(allNodes), new TypeToken<List<AdminCategoryGetVO>>() {
                }.getType());
        List<AdminCategoryGetVO> children = new ArrayList<>();
        for (AdminCategoryGetVO comparedOne : list) {
            if (Objects.equals(comparedOne.getPid(), root.getId())) {
                comparedOne.parentCategory = root;
                children.add(comparedOne);
            }
        }
        List notChildren = (List) CollectionUtils.subtract(allNodes, children);
        for (AdminCategoryGetVO child : children) {
            child.setChildrenList(AdminCategoryGetVO.listChildren(child, notChildren));
        }
        return children;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminCategoryGetVO class

/* End of file AdminCategoryGetVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/cms/category/AdminCategoryGetVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
