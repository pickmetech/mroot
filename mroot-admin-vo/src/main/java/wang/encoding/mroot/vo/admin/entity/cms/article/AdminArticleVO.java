/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.cms.article;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台文章实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminArticleVO implements Serializable {


    private static final long serialVersionUID = 7945481625791828316L;

    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
     */
    public static final String TABLE_NAME = "cms_article";
    /**
     * 表前缀
     */
    public static final String TABLE_SUFFIX = "cms_";
    /**
     *文章ID
     */
    public static final String ID = "id";
    /**
     *文章分类ID
     */
    public static final String CATEGORY_ID = "category_id";
    /**
     *标题
     */
    public static final String TITLE = "title";
    /**
     *描述
     */
    public static final String DESCRIPTION = "description";
    /**
     *封面
     */
    public static final String COVER = "cover";
    /**
     *浏览量
     */
    public static final String PAGE_VIEW = "page_view";
    /**
     *优先级,数字越大级别越高
     */
    public static final String PRIORITY = "priority";
    /**
     *外链
     */
    public static final String LINK_URI = "link_uri";
    /**
     *是否前台可见,1前后台都可见,2后台可见
     */
    public static final String DISPLAY = "display";
    /**
     *状态(1是正常,2是禁用,3是删除)
     */
    public static final String STATE = "state";
    /**
     *创建时间
     */
    public static final String GMT_CREATE = "gmt_create";
    /**
     *创建IP
     */
    public static final String GMT_CREATE_IP = "gmt_create_ip";
    /**
     *修改时间
     */
    public static final String GMT_MODIFIED = "gmt_modified";

    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */

    /**
     * ID
     */
    private BigInteger id;
    /**
     * 文章分类ID
     */
    private BigInteger categoryId;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String description;
    /**
     * 封面
     */
    private String cover;
    /**
     * 浏览量
     */
    private Integer pageView;
    /**
     * 优先级,数字越大级别越高
     */
    private Integer priority;
    /**
     * 外链
     */
    private String linkUri;
    /**
     * 是否前台可见,1前后台都可见,2后台可见
     */
    private Integer display;
    /**
     * 是否转载,1是,2否
     */
    private Integer reprint;
    /**
     * 是否打赏,1是,2否
     */
    private Integer reward;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminArticleVO class

/* End of file AdminArticleVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/cms/article/AdminArticleVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
