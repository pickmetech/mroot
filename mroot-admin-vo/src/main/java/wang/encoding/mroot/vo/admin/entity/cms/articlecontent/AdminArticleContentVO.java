/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.cms.articlecontent;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台文章内容实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminArticleContentVO implements Serializable {


    private static final long serialVersionUID = 7170628041260453552L;

    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
     */
    public static final String TABLE_NAME = "cms_article_content";
    /**
     * 表前缀
     */
    public static final String TABLE_SUFFIX = "cms_";
    /**
     *主键，和文章表共用ID
     */
    public static final String ID = "id";
    /**
     *文章内容
     */
    public static final String CONTENT = "content";
    /**
     *文章内容html内容
     */
    public static final String CONTENT_HTML = "content_html";
    /**
     *创建时间
     */
    public static final String GMT_CREATE = "gmt_create";
    /**
     *创建IP
     */
    public static final String GMT_CREATE_IP = "gmt_create_ip";
    /**
     *修改时间
     */
    public static final String GMT_MODIFIED = "gmt_modified";
    /**
     *状态(1是正常,2是禁用,3是删除)
     */
    public static final String STATE = "state";

    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */

    /**
     * ID
     */
    private BigInteger id;
    /**
     * 文章内容
     */
    private String content;
    /**
     * 文章内容html内容
     */
    private String contentHtml;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminArticleContentVO class

/* End of file AdminArticleContentVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/cms/articlecontent/AdminArticleContentVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
