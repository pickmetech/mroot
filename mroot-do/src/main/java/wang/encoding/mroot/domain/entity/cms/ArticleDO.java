/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.domain.entity.cms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台文章实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@TableName("cms_article")
public class ArticleDO extends Model<ArticleDO> implements Serializable {


    private static final long serialVersionUID = -7752947696736413891L;
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private BigInteger id;
    /**
     * 文章分类ID
     */
    private BigInteger categoryId;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String description;
    /**
     * 封面
     */
    private String cover;
    /**
     * 浏览量
     */
    private Integer pageView;
    /**
     * 优先级,数字越大级别越高
     */
    private Integer priority;
    /**
     * 外链
     */
    private String linkUri;
    /**
     * 是否前台可见,1前后台都可见,2后台可见
     */
    private Integer display;
    /**
     * 是否转载,1是,2否
     */
    private Integer reprint;
    /**
     * 是否打赏,1是,2否
     */
    private Integer reward;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleDO class

/* End of file ArticleDO.java */
/* Location: ./src/main/java/wang/encoding/mroot/domain/entity/cms/ArticleDO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
