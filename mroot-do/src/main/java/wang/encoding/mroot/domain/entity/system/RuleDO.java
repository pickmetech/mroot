/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.domain.entity.system;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * 角色实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@TableName("system_rule")
public class RuleDO extends Model<RuleDO> implements Serializable, Comparable<RuleDO> {

    private static final long serialVersionUID = -1245162377123762393L;


    // -------------------------------------------------------------------------------------------------

    /**
     * 权限ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private BigInteger id;
    /**
     * 父级权限
     */
    private BigInteger pid;
    /**
     * 1-url地址,2-主菜单;3-子级菜单,4-按钮
     */
    private Integer category;
    /**
     * 名称
     */
    private String title;
    /**
     * url地址
     */
    private String url;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 状态（-1：已删除，0：禁用，1：正常）
     */
    private Integer state;
    /**
     * 备注
     */
    private String remark;
    /**
     * 添加时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 添加IP
     */
    @TableField("gmt_create_ip")
    private Integer gmtCreateIp;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 父级权限
     */
    @TableField(exist = false)
    private RuleDO parentRuleDO;
    /**
     * 子级权限
     */
    @TableField(exist = false)
    private List<RuleDO> childrenList;

    @Override
    public int compareTo(RuleDO o) {
        return this.id.compareTo(o.id);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RuleDO class

/* End of file RuleDO.java */
/* Location ./src/main/java/wang/encoding/mroot/domain/entity/system/RuleDO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
