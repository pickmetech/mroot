/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>
<http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.blog.entity.cms.category;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.AesManageComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * 博客文章分类实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class BlogCategoryGetVO extends BlogCategoryVO implements Comparable<BlogCategoryGetVO> {

    private static final long serialVersionUID = -158128221863717100L;


    /**
     * 状态
     */
    private String status;
    /**
     * 类型
     */
    private String type;
    /**
     * IP
     */
    private String ip;
    /**
     * 父级分类
     */
    private BlogCategoryGetVO parentCategory;
    /**
     * 子级权限集合
     */
    private List<BlogCategoryGetVO> childrenList;
    /**
     * 加密后的 ID
     */
    private String aesId;

    // -------------------------------------------------------------------------------------------------

    /**
     * list 转为 list
     *
     * @param list List
     * @return List
     */
    public static List<BlogCategoryGetVO> list(@NotNull final List<BlogCategoryGetVO> list) {
        List<BlogCategoryGetVO> listAll = new Gson()
                .fromJson(new Gson().toJson(list), new TypeToken<List<BlogCategoryGetVO>>() {
                }.getType());
        List<BlogCategoryGetVO> roots = BlogCategoryGetVO.listRoot(listAll);
        List notRoots = (List) CollectionUtils.subtract(list, roots);
        for (BlogCategoryGetVO root : roots) {
            root.setChildrenList(BlogCategoryGetVO.listChildren(root, notRoots));
        }
        return roots;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 找到根节点
     *
     * @param allNodes List
     * @return list
     */
    private static List<BlogCategoryGetVO> listRoot(@NotNull final List<BlogCategoryGetVO> allNodes) {
        List<BlogCategoryGetVO> results = new ArrayList<>();
        for (BlogCategoryGetVO node : allNodes) {
            boolean isRoot = true;
            for (BlogCategoryGetVO comparedOne : allNodes) {
                if (Objects.equals(node.getPid(), comparedOne.getId())) {
                    isRoot = false;
                    break;
                }
            }
            if (isRoot) {
                results.add(node);
            }
        }
        return results;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据根节点找到子节点
     *
     * @param root     AdminRuleGetVO
     * @param allNodes List
     * @return List
     */
    private static List<BlogCategoryGetVO> listChildren(@NotNull final BlogCategoryGetVO root,
            @NotNull final List allNodes) {
        List<BlogCategoryGetVO> list = new Gson()
                .fromJson(new Gson().toJson(allNodes), new TypeToken<List<BlogCategoryGetVO>>() {
                }.getType());
        List<BlogCategoryGetVO> children = new ArrayList<>();
        for (BlogCategoryGetVO comparedOne : list) {
            if (Objects.equals(comparedOne.getPid(), root.getId())) {
                comparedOne.parentCategory = root;
                // ID 加密
                comparedOne.setAesId(AesManageComponent.getEncrypt(String.valueOf(comparedOne.getId())));
                children.add(comparedOne);
            }
        }
        List notChildren = (List) CollectionUtils.subtract(allNodes, children);
        for (BlogCategoryGetVO child : children) {
            child.setChildrenList(BlogCategoryGetVO.listChildren(child, notChildren));
        }
        return children;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 排序规则
     *  sort 属性升序排序
     */
    @Override
    public int compareTo(BlogCategoryGetVO o) {
        return this.getSort().compareTo(o.getSort());
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogCategoryGetVO class

/* End of file BlogCategoryGetVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/blog/entity/cms/category/BlogCategoryGetVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
