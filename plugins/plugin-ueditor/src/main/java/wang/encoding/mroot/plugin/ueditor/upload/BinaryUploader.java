/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.upload;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import wang.encoding.mroot.plugin.ueditor.define.AppInfo;
import wang.encoding.mroot.plugin.ueditor.define.BaseState;
import wang.encoding.mroot.plugin.ueditor.define.FileType;
import wang.encoding.mroot.plugin.ueditor.define.State;


/**
 * 二进制上传
 *
 * @author ErYang
 */
@Component
public class BinaryUploader {


    /**
     * 保存
     *
     * @param request HttpServletRequest
     * @param conf    Map<String, Object>
     * @return State
     */
    public State save(final HttpServletRequest request, final Map<String, Object> conf) {
        if (!ServletFileUpload.isMultipartContent(request)) {
            return new BaseState(false, AppInfo.NOT_MULTIPART_CONTENT);
        }
        MultipartFile file = ((MultipartHttpServletRequest) request).getFile("upfile");
        assert file != null;
        String suffix = FileType.getSuffixByFilename(file.getOriginalFilename());
        long maxSize = (Long) conf.get("maxSize");
        String[] allowFiles = (String[]) conf.get("allowFiles");
        if (!validType(suffix, allowFiles)) {
            return new BaseState(false, AppInfo.NOT_ALLOW_FILE_TYPE);
        }
        try {
            State storageState = StorageManager.saveFileByInputStream(file.getInputStream(), file.getOriginalFilename(), maxSize);
            if (storageState.isSuccess()) {
                storageState.putInfo("type", suffix);
                storageState.putInfo("original", file.getOriginalFilename());
            }
            return storageState;
        } catch (IOException e) {
            return new BaseState(false, AppInfo.IO_ERROR);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证类型
     *
     * @param type       String
     * @param allowTypes String[]
     * @return Boolean
     */
    private Boolean validType(final String type, final String[] allowTypes) {
        List<String> list = Arrays.asList(allowTypes);
        return list.contains(type);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BinaryUploader class

/* End of file BinaryUploader.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/upload/BinaryUploader.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

