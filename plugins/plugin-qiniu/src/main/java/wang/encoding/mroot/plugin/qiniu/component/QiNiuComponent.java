/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.qiniu.component;


import com.qiniu.common.Zone;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.QiNiuUtils;
import wang.encoding.mroot.plugin.qiniu.constant.QiNiuConst;

import javax.annotation.PostConstruct;

/**
 * 七牛配置
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class QiNiuComponent {


    private final QiNiuConst qiNiuConst;


    @Autowired
    public QiNiuComponent(QiNiuConst qiNiuConst) {
        this.qiNiuConst = qiNiuConst;
    }


    /**
     * 注入配置
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[QiNiuComponent]<<<<<<<<");
        }
        QiNiuComponent qiNiuComponent = this;

        QiNiuUtils.ACCESS_KEY = qiNiuComponent.qiNiuConst.getAccessKey();
        QiNiuUtils.SECRET_KEY = qiNiuComponent.qiNiuConst.getSecretKey();
        QiNiuUtils.ZONE = QiNiuComponent.getQiniuZone(qiNiuComponent.qiNiuConst.getZone());
        QiNiuUtils.BUCKET = qiNiuComponent.qiNiuConst.getBucket();
        QiNiuUtils.BASE_URL = qiNiuComponent.qiNiuConst.getBaseUrl();
        QiNiuUtils.configuration = QiNiuComponent
                .getConfiguration(QiNiuComponent.getQiniuZone(qiNiuComponent.qiNiuConst.getZone()));
        QiNiuUtils.uploadManager = QiNiuComponent
                .getUploadManager(QiNiuComponent.getQiniuZone(qiNiuComponent.qiNiuConst.getZone()));
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置七牛工具类的配置" + "[ACCESS_KEY={},SECRET_KEY={}" + ",ZONE={},BUCKET={},"
                            + "BASE_URL={}]>>>>>>>>", qiNiuComponent.qiNiuConst.getAccessKey(),
                    qiNiuComponent.qiNiuConst.getSecretKey(), qiNiuComponent.qiNiuConst.getZone(),
                    qiNiuComponent.qiNiuConst.getBucket(), qiNiuComponent.qiNiuConst.getBaseUrl());
            logger.debug(">>>>>>>>设置七牛工具类的配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到七牛机房地址
     *
     * @param zoneStr String
     * @return Zone
     */
    private static Zone getQiniuZone(@Nullable final String zoneStr) {
        // 华东：zone0
        // 华北：zone1
        // 华南：zone2
        // 北美：zoneNa0
        Zone zone;
        switch (zoneStr) {
            case "zone0":
                zone = Zone.zone0();
                break;
            case "zone1":
                zone = Zone.zone1();
                break;
            case "zone2":
                zone = Zone.zone2();
                break;
            case "zoneNa0":
                zone = Zone.zoneNa0();
                break;
            default:
                zone = Zone.autoZone();
                break;
        }
        return zone;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 Configuration
     *
     * @param zone Zone
     * @return Configuration
     */
    private static Configuration getConfiguration(@NotNull final Zone zone) {
        return new Configuration(zone);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 UploadManager
     *
     * @param zone Zone
     * @return UploadManager
     */
    private static UploadManager getUploadManager(@NotNull final Zone zone) {
        Configuration configuration = QiNiuComponent.getConfiguration(zone);
        return new UploadManager(configuration);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QiNiuComponent class

/* End of file QiNiuComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/qiniu/component/QiNiuComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
