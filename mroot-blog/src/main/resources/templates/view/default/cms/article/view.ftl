<#-- /* 头部标题 */ -->
<@OVERRIDE name="HEAD_TITLE">
    <title>${article.title}_${I18N("message.blog.metaTitle")}</title>
</@OVERRIDE>
<@OVERRIDE name="META_KEYWORDS">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content??>
        <meta name="keywords"
              content="${article.title},${category.title},${I18N("message.blog.meta.base")},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content}">
    <#else>
        <meta name="keywords"
              content="${article.title},${category.title},${I18N("message.blog.meta.base")},贰阳,开发者,源码,IT网站,技术博客,分享,Java,JavaWeb,Spring,Spring Boot,Spring Cloud">
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_DESCRIPTION">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content??>
        <meta name="description"
              content="${article.title},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content},${I18N("message.blog.meta.base")}">
    <#else>
        <meta name="description"
              content="${article.title},IT网站,技术博客,${I18N("message.blog.meta.base")}">
    </#if>
</@OVERRIDE>
<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="row">

        <div class="col-lg-3">
            <aside class="sidebar" id="js_left-nav">

                <ul class="nav nav-list flex-column mb-5">

                    <#list treeCategories as topCategory>

                        <#if topCategory.childrenList>

                            <li class="nav-item">
                                <a class="nav-link"
                                   href="javascript:void(0)"><span>${topCategory.title}</span></a>
                                <ul>
                                    <#list topCategory.childrenList as childCategory>
                                        <li class="nav-item">
                                            <a class="nav-link"
                                               href="${CONTEXT_PATH}/cms/category/cate/${childCategory.aesId}">
                                                ${childCategory.title}
                                            </a>
                                        </li>
                                    </#list>
                                </ul>
                            </li>

                        <#else>

                            <li class="nav-item"><a class="nav-link"
                                                    href="${CONTEXT_PATH}/cms/category/cate/${topCategory.aesId}"><span>${topCategory.title}</span></a>
                            </li>

                        </#if>
                    </#list>


                </ul>

                <div class="tabs tabs-dark mb-4 pb-2">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active"><a
                                    class="nav-link show active text-1 font-weight-bold text-uppercase"
                                    href="#popularPosts" data-toggle="tab">${I18N("message.blog.article.view.new")}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link text-1 font-weight-bold text-uppercase"
                                                href="#recentPosts"
                                                data-toggle="tab">${I18N("message.blog.article.view.hot")}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="popularPosts">
                            <ul class="simple-post-list">
                                <#list newList as item>
                                    <li>
                                        <div class="post-image">
                                            <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                                   target="_blank">
                                                    <#if item.cover??>
                                                        <img width="50" height="50" class="img-fluid"
                                                             src="${item.cover}"
                                                             alt="${item.title}">
                                                    <#else>
                                                        <img width="50" height="50" class="img-fluid"
                                                             src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/business-consulting/blog/blog-default-bg.png"
                                                             alt="${item.title}">
                                                    </#if>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="post-info">
                                            <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                               class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                               title="<@defaultStr item.title></@defaultStr>"><@subStr str=item.title length=item.title?length></@subStr></a>
                                            <div class="post-meta"><@dateFormat2  item.gmtCreate></@dateFormat2></div>
                                        </div>
                                    </li>
                                </#list>
                            </ul>
                        </div>
                        <div class="tab-pane" id="recentPosts">
                            <ul class="simple-post-list">
                                <#list hotList as item>
                                    <li>
                                        <div class="post-image">
                                            <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                                   target="_blank">
                                                    <#if item.cover??>
                                                        <img width="50" height="50" class="img-fluid"
                                                             src="${item.cover}"
                                                             alt="${item.title}">
                                                    <#else>
                                                        <img width="50" height="50" class="img-fluid"
                                                             src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/business-consulting/blog/blog-default-bg.png"
                                                             alt="${item.title}">
                                                    </#if>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="post-info">
                                            <a href="${CONTEXT_PATH}/cms/article/view/${item.aesId}"
                                               class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                               title="<@defaultStr item.title></@defaultStr>"><@subStr str=item.title length=item.title?length></@subStr></a>
                                            <div class="post-meta"><@dateFormat2  item.gmtCreate></@dateFormat2></div>
                                        </div>
                                    </li>
                                </#list>
                            </ul>
                        </div>
                    </div>
                </div>


            </aside>
        </div>

        <div class="col-lg-8">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12">

                    <article class="blog-post">

                        <div class="text-color-dark post-image ml-0">
                            <blockquote class="blockquote-primary">
                                <p><@defaultStr article.description></@defaultStr></p>
                            </blockquote>
                        </div>

                        <h2 class="text-color-dark font-weight-normal text-5 mb-2"><strong
                                    class="font-weight-extra-bold"><@defaultStr article.title></@defaultStr></strong>
                        </h2>

                        <div class="post-infos mb-4">
                                    	<span class="info posted-by">${I18N("message.blog.article.view.category")}<span
                                                    class="post-author font-weight-semibold text-color-dark"><@defaultStr category.title></@defaultStr></span>
											</span>
                            <span class="info comments ml-4">${I18N("message.blog.article.view.pageView")}<span
                                        class="comments-number text-color-primary font-weight-semibold"><@defaultStr pageView></@defaultStr></span>
											</span>
                            <span class="info like ml-4">${I18N("message.blog.article.view.gmtCreate")}<span
                                        class="like-number font-weight-semibold custom-color-dark"><@dateFormat2  article.gmtCreate></@dateFormat2></span>
											</span>
                        </div>

                        <hr class="solid">

                        <#if (booleanYes == article.reprint)>
                        <#-- 是否转载 -->
                            <p class="mb-1">本文转载自&nbsp;${article.linkUri}&nbsp;，略有删改，如有侵权，请联系删除。</p>
                        </#if>

                        <div id="article-content" class="text-color-dark mt-5">
                            <@defaultStr articleContent.content></@defaultStr>
                        </div>

                        <hr class="solid mt-5 mb-5">

                        <#-- 网站标语 -->
                        <div class="mt-2 pt-2 p-4 rounded bg-color-grey">
                            <p><strong class="name"><a href="javascript:void(0)"
                                                       class="text-4 pb-2 pt-2 d-block">编程中一个迷途程序猿的分享</a></strong>
                            </p>
                            <p class="font-weight-semibold custom-color-dark">
                                技术分享，分享提升自己，分享让世界更美好。才疏学浅，不妥之处，敬请批评指正。</p>
                        </div>


                        <div class="mt-4 pt-4 pb-2 pl-1 rounded bg-color-grey">
                            <p><span class="info font-weight-semibold text-color-dark p-1">本文链接</span><span
                                        class="info" style="word-break:break-all;word-wrap:break-word;"><a
                                            href="javascript:void(0)" id="js_page-url"></a></span><span
                                        class="info font-weight-semibold text-color-dark p-1">转载请保留</span>
                            </p>
                        </div>

                        <#if (booleanYes == article.reward)>
                        <#-- 是否赞助 -->
                            <div class="mt-2 pt-2">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 isotope-item">
                                        <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao.png"
                                                         class="img-fluid" alt="支付宝捐助" width="100" height="100">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助</span>
													</span>
												</span>
											</span>
                                        </a>
                                    </div>

                                    <div class="col-lg-3 col-md-6 isotope-item">
                                        <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin.png"
                                                         class="img-fluid" alt="微信捐助">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助</span>
													</span>
												</span>
											</span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </#if>

                    </article>

                </div>
            </div>
        </div>

    </div>
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">

    <script>
        jQuery(document).ready(function () {

            var pageUrl = window.location.href;
            $('#js_page-url').attr('href', pageUrl).text(pageUrl);

            // 顶部导航高亮
            BlogTool.highlight_top_nav('${navIndex}');
            // 左侧导航高亮
            BlogTool.highlight_left_nav('${navIndex}');

        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
