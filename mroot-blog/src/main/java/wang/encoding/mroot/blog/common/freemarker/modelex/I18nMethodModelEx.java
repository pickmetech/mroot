/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.freemarker.modelex;


import freemarker.template.TemplateMethodModelEx;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.util.collection.ListUtils;

import java.util.List;

/**
 * i18n 国际化 freemarker 方法类
 *
 * @author ErYang
 */
@Component
public class I18nMethodModelEx implements TemplateMethodModelEx {


    @Override
    public Object exec(final List list) {
        if (ListUtils.isNotEmpty(list)) {
            return LocaleMessageSourceComponent.getMessage(list.get(0).toString());
        } else {
            return GlobalMessage.EXCEPTION_INFO;
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End I18nMethodModelEx class

/* End of file I18nMethodModelEx.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/freemarker/modelex/I18nMethodModelEx.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
