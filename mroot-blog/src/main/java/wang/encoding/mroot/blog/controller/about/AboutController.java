/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.controller.about;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.controller.BlogBaseController;
import wang.encoding.mroot.common.exception.ControllerException;

/**
 * 关于本站控制器
 *
 * @author ErYang
 */
@RestController
@RequestMapping("/about")
public class AboutController extends BlogBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/about";
    /**
     * 首页
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = "/index";
    private static final String INDEX_VIEW = MODULE_NAME + INDEX;


    /**
     * 关于本站页面
     *
     * @return ModelAndView
     */
    @RequestMapping(INDEX_URL)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW);
        modelAndView.addObject(NAV_INDEX_URL_NAME, contextPath + MODULE_NAME + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// End AboutController class

/* End of file AboutController.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/controller/about/AboutController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
