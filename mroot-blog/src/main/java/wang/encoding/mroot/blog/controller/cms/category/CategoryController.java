/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.controller.cms.category;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.controller.BlogBaseController;
import wang.encoding.mroot.common.component.AesManageComponent;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.CategoryTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.common.util.number.NumberUtils;
import wang.encoding.mroot.service.blog.cms.BlogArticleService;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;
import wang.encoding.mroot.vo.blog.entity.cms.article.BlogArticleGetVO;
import wang.encoding.mroot.vo.blog.entity.cms.category.BlogCategoryGetVO;

import java.math.BigInteger;
import java.util.List;


/**
 * 博客 文章分类 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/cms/category")
public class CategoryController extends BlogBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/cms/category";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/cms/category";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "category";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 分类页面
     */
    private static final String CATE = "/cate";
    private static final String CATE_URL = CATE;
    private static final String CATE_VIEW = VIEW_PATH + CATE;

    private final BlogCategoryService categoryService;

    private final BlogArticleService articleService;

    @Autowired
    public CategoryController(BlogCategoryService categoryService, BlogArticleService articleService) {
        this.categoryService = categoryService;
        this.articleService = articleService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 分类主页页面
     *
     * @return ModelAndView
     */
    @RequestMapping(INDEX_URL)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW);

        BlogArticleGetVO articleGetVO = new BlogArticleGetVO();
        articleGetVO.setState(StateEnum.NORMAL.getKey());
        articleGetVO.setDisplay(BooleanEnum.YES.getKey());

        Page<BlogArticleGetVO> pageInt = new Page<>();
        IPage<BlogArticleGetVO> page = articleService
                .list2page(super.initPage(pageInt), articleGetVO, BlogArticleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, page);

        // 最热文章
        BlogArticleGetVO hotArticleGetVO = new BlogArticleGetVO();
        hotArticleGetVO.setState(StateEnum.NORMAL.getKey());
        hotArticleGetVO.setDisplay(BooleanEnum.YES.getKey());
        List<BlogArticleGetVO> hotList = articleService.list(hotArticleGetVO, 12, BlogArticleGetVO.PAGE_VIEW, false);
        modelAndView.addObject("hotList", hotList);

        modelAndView.addObject(NAV_INDEX_URL_NAME, contextPath + "/");
        modelAndView.addObject(INDEX_URL_NAME, contextPath + MODULE_NAME + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 分类列表
     *
     * @param id                 String
     * @return ModelAndView
     */
    @RequestMapping(CATE_URL + "/{id}")
    public ModelAndView cate(@PathVariable(ID_NAME) String id) throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(CATE_VIEW);

        // 验证数据
        if (StringUtils.isBlank(id)) {
            return initErrorRedirectUrl();
        }
        // 解密
        String idStr = AesManageComponent.decrypt(id);
        if (!NumberUtils.isNumber(idStr)) {
            return initErrorRedirectUrl();
        }
        BigInteger idValue = new BigInteger(idStr);
        // 数据真实性
        BlogCategoryGetVO categoryGetVO = categoryService.getById(idValue);
        if (null == categoryGetVO || StateEnum.NORMAL.getKey() != categoryGetVO.getState()
                || BooleanEnum.YES.getKey() != categoryGetVO.getDisplay()) {
            return initErrorRedirectUrl();
        }

        modelAndView.addObject(VIEW_MODEL_NAME, categoryGetVO);
        BlogCategoryGetVO pCategory = categoryService.getById(categoryGetVO.getPid());
        if (null != pCategory) {
            modelAndView.addObject("pCategory", pCategory);
        }

        BlogArticleGetVO articleGetVO = new BlogArticleGetVO();
        articleGetVO.setState(StateEnum.NORMAL.getKey());
        articleGetVO.setDisplay(BooleanEnum.YES.getKey());


        Page<BlogArticleGetVO> pageInt = new Page<>();

        if (CategoryTypeEnum.SECOND.getKey() == categoryGetVO.getCategory()) {
            List<BigInteger> categoryIds = categoryService.listByPId(categoryGetVO.getId());
            categoryIds.add(categoryGetVO.getId());
            IPage<BlogArticleGetVO> page = articleService
                    .list2page(super.initPage(pageInt), articleGetVO, BlogArticleGetVO.ID, false, categoryIds);
            modelAndView.addObject(VIEW_PAGE_NAME, page);
        } else {
            articleGetVO.setCategoryId(categoryGetVO.getId());
            IPage<BlogArticleGetVO> page = articleService
                    .list2page(super.initPage(pageInt), articleGetVO, BlogArticleGetVO.ID, false);
            modelAndView.addObject(VIEW_PAGE_NAME, page);
        }

        // 最热文章
        BlogArticleGetVO hotArticleGetVO = new BlogArticleGetVO();
        hotArticleGetVO.setState(StateEnum.NORMAL.getKey());
        hotArticleGetVO.setDisplay(BooleanEnum.YES.getKey());
        hotArticleGetVO.setCategoryId(categoryGetVO.getId());
        List<BlogArticleGetVO> hotList = articleService.list(hotArticleGetVO, 12, BlogArticleGetVO.PAGE_VIEW, false);
        modelAndView.addObject("hotList", hotList);

        modelAndView.addObject(NAV_INDEX_URL_NAME, contextPath + MODULE_NAME + CATE_URL + "/" + id);
        modelAndView.addObject(INDEX_URL_NAME, contextPath + MODULE_NAME + CATE_URL + "/" + id);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryController class

/* End of file CategoryController.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/controller/cms/category/CategoryController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
